/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

#include "Faust/LaDiDaFaust.h"

//==============================================================================
class FaustSound : public SynthesiserSound
{
public:
    FaustSound() {}
    
    bool appliesToNote (int) override { return true; }
    bool appliesToChannel (int) override { return true; }
};

//==============================================================================
class FaustVoice : public SynthesiserVoice
{
public:
    FaustVoice(int numVoices, LaDiDaAudioProcessor& pIn) 
        : tailOff (true), 
          onOff (false), 
          envelope (0.0),
          p (pIn)
    {
        curNumVoices = numVoices;
        laDiDaFaust.init ((int)getSampleRate() );
        laDiDaFaust.buildUserInterface (&laDiDaFaustControl);
    }
    
    bool canPlaySound (SynthesiserSound* sound) override
    {
        return dynamic_cast<FaustSound*> (sound) != nullptr;
    }
    
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* /*sound*/,
                    int /*currentPitchWheelPosition*/) override
    {
        origFreq = (float)MidiMessage::getMidiNoteInHertz (midiNoteNumber);
        audioBuffer = new float*[1];
        audioBuffer[0] = new float[1];
        
        laDiDaFaustControl.setParamValue ("/fof/smooth",0.0f);
        laDiDaFaustControl.setParamValue ("/fof/freq",origFreq);
        laDiDaFaust.compute (1, NULL, audioBuffer);
        
        delete [] audioBuffer[0];
        delete [] audioBuffer;
        audioBuffer = 0;
        
        laDiDaFaustControl.setParamValue ("/fof/smooth",0.999f);
        laDiDaFaustControl.setParamValue ("/fof/gate",velocity);
        
        // set note status to on
        onOff = true;
    }
    
    void stopNote (float /*velocity*/, bool allowTailOff) override
    {
        onOff = false; // end the note
        laDiDaFaustControl.setParamValue ("/fof/gate",0.0f);
        tailOff = allowTailOff; // store the tailOff status
    }
    
    void pitchWheelMoved (int /*newValue*/) override 
    {
    }
    
    void controllerMoved (int controllerNumber, int newValue) override 
    {
        float newValConverted;

        // dirty hard-coded controller values handling
        // works with Novation Impulse midi controller
        
        // if mod wheel, control the vowel mix value
        if (controllerNumber == 1){
            newValConverted = (2.0f/127.0f)*(float)newValue - 1.0f;
            laDiDaFaustControl.setParamValue ("/fof/vowelMix",newValConverted);
        }
        // if knob 1, control the gain value
        else if (controllerNumber == 21){
            newValConverted = (60.0f/12.0f)*(float)newValue - 30.0f;
            laDiDaFaustControl.setParamValue ("/fof/gain",newValConverted);
        }
        // if knob 2, control the vibrato depth
        else if (controllerNumber == 22){
            newValConverted = (0.05f/127.0f)*(float)newValue;
            laDiDaFaustControl.setParamValue ("/fof/vibDepth",newValConverted);
        }
        // if knob 3, control the vibrato rate
        else if (controllerNumber == 23){
            newValConverted = (10.0f/127.0f)*(float)newValue;
            laDiDaFaustControl.setParamValue ("/fof/vibRate",newValConverted);
        }
        // if knob 5, control vowel 1 selection
        else if (controllerNumber == 25){
            newValConverted  = (newValue <= 31) ? 0.0f : 
                               (newValue > 31 && newValue <= 63) ? 1.0f :
                               (newValue > 63 && newValue <= 95) ? 2.0f : 3.0f;
            laDiDaFaustControl.setParamValue ("/fof/vowel1",newValConverted);
        }
        // if knob 6, control vowel 2 selection
        else if (controllerNumber == 26){
            newValConverted  = (newValue <= 31) ? 0.0f : 
                               (newValue > 31 && newValue <= 63) ? 1.0f :
                               (newValue > 63 && newValue <= 95) ? 2.0f : 3.0f;
            laDiDaFaustControl.setParamValue ("/fof/vowel2",newValConverted);
        }
        // if knob 7, control vowel 3 selection
        else if (controllerNumber == 27){
            newValConverted  = (newValue <= 31) ? 0.0f : 
                               (newValue > 31 && newValue <= 63) ? 1.0f :
                               (newValue > 63 && newValue <= 95) ? 2.0f : 3.0f;
            laDiDaFaustControl.setParamValue ("/fof/vowel3",newValConverted);
        }
        // if slider #1, control the attack value
        else if (controllerNumber == 41){
            newValConverted = (0.95f/127.0f)*(float)newValue + 0.05f;
            laDiDaFaustControl.setParamValue ("/fof/attack",newValConverted);
        }
        // if slider #2, control the decay value
        else if (controllerNumber == 42){
            newValConverted = (0.95f/127.0f)*(float)newValue + 0.05f;
            laDiDaFaustControl.setParamValue ("/fof/decay",newValConverted);
        }
        // if slider #3, control the sustain value
        else if (controllerNumber == 43){
            newValConverted = (0.95f/127.0f)*(float)newValue + 0.05f;
            laDiDaFaustControl.setParamValue ("/fof/sustain",newValConverted);
        }
        // if slider #4, control the release value
        else if (controllerNumber == 44){
            newValConverted = (4.95f/127.0f)*(float)newValue + 0.05f;
            laDiDaFaustControl.setParamValue ("/fof/release",newValConverted);
        }
        // if slider #5, control the gender value
        else if (controllerNumber == 45){
            newValConverted  = (newValue >= 64) ? 1.0f : 0.0f;
            laDiDaFaustControl.setParamValue ("/fof/gender",newValConverted);
        }
    }

    // method to pass parameter values to Faust dsp class
    void setParameterValues (String paraName, float paraVal)
    {
        // osc values should be updated for active and inactive notes
        if (paraName == "gender")
            laDiDaFaustControl.setParamValue ("/fof/gender",paraVal);
        else if (paraName == "vowel1")
            laDiDaFaustControl.setParamValue ("/fof/vowel1",paraVal);
        else if (paraName == "vowel2")
            laDiDaFaustControl.setParamValue ("/fof/vowel2",paraVal);
        else if (paraName == "vowel3")
            laDiDaFaustControl.setParamValue ("/fof/vowel3",paraVal);
        else if (paraName == "vowelMix")
            laDiDaFaustControl.setParamValue ("/fof/vowelMix",paraVal);
        else if (paraName == "gain")
            laDiDaFaustControl.setParamValue ("/fof/gain",paraVal);
        else if (paraName == "vibDepth")
            laDiDaFaustControl.setParamValue ("/fof/vibDepth",paraVal);
        else if (paraName == "vibRate")
            laDiDaFaustControl.setParamValue ("/fof/vibRate",paraVal);

        // only update ADSR values if note isn't currently audible
        if (envelope < 0.001 || onOff == false){
            if (paraName == "attack")
                laDiDaFaustControl.setParamValue ("/fof/attack",paraVal);
            else if (paraName == "decay")
                laDiDaFaustControl.setParamValue ("/fof/decay",paraVal);
            else if (paraName == "sustain")
                laDiDaFaustControl.setParamValue ("/fof/sustain",paraVal);
            else if (paraName == "release")
                laDiDaFaustControl.setParamValue ("/fof/release",paraVal);
        }
    }
    
    void renderNextBlock (AudioBuffer<float>& outputBuffer, int startSample, int numSamples) override
    {
        processBlock (outputBuffer, startSample, numSamples);
    }
    
    void renderNextBlock (AudioBuffer<double>& outputBuffer, int startSample, int numSamples) override
    {
        processBlock (outputBuffer, startSample, numSamples);
    }

private:

    LaDiDaAudioProcessor& p;
    
    template <typename FloatType>
    void processBlock (AudioBuffer<FloatType>& outputBuffer, int startSample, int numSamples)
    {
        int origNumSamples = numSamples;
        double polyGain = 1.0/(double)curNumVoices;
        
        // only compute block if note is on
        if (envelope != 0.0 || onOff){
            // allocate memory - done here because it is the only place where we
            // have access to the current size of the buffer
            audioBuffer = new float*[1];
                audioBuffer[0] = new float[numSamples];
            
            // compute current buffer with Faust code
            laDiDaFaust.compute (numSamples, NULL, audioBuffer);
            FloatType tmpEnv = 0.0; // temp var to calc env from avg of samples
            int i = 0;
            while (--numSamples >= 0){
                // calculate tmpEnv from first output of Faust code
                tmpEnv += std::abs (static_cast<FloatType>(audioBuffer[0][i]));
                for (int channel = 0; channel < outputBuffer.getNumChannels(); ++channel){
                    FloatType curVal;
                    curVal = static_cast<FloatType> (polyGain * audioBuffer[0][i]);
                    
                    // add current value to each channel
                    outputBuffer.addSample (channel, startSample, curVal);
                }
                // increment indexes
                ++i;
                ++startSample;
            }
            envelope = tmpEnv / static_cast<FloatType>(origNumSamples);
            
            // if no tail or amplitude below threshold (-60 dB), clear note
            if (!onOff && (envelope < 0.01 || !tailOff)){
                envelope = 0.0;
                clearCurrentNote();
            }
            
            // clean up memory
            delete [] audioBuffer[0];

            delete [] audioBuffer;
            audioBuffer = 0;
        }
    }
    
    LaDiDaFaust laDiDaFaust;
    MapUI laDiDaFaustControl;
    int curNumVoices;
    double envelope;
    float** audioBuffer;
    float origFreq;
    bool onOff, tailOff;
};

//==============================================================================
LaDiDaAudioProcessor::LaDiDaAudioProcessor() : lastUIWidth(650), lastUIHeight(300),
                                               attackParam(nullptr), decayParam(nullptr),
                                               sustainParam(nullptr), releaseParam(nullptr),
                                               gainParam(nullptr), genderParam(nullptr), 
                                               vowel1Param(nullptr), vowel2Param(nullptr), 
                                               vowel3Param(nullptr), vowelMixParam(nullptr), 
                                               vibratoDepthParam(nullptr), vibratoRateParam(nullptr),
                                               vowelNumItems(4)
{
    // create parameters
    addParameter (attackParam = new AudioParameterFloat ("attack", "attack", 0.05f, 1.0f, 0.1f));
    addParameter (decayParam = new AudioParameterFloat ("decay", "decay", 0.05f, 1.0f, 0.1f));
    addParameter (sustainParam = new AudioParameterFloat ("sustain", "sustain", 0.05f, 1.0f, 0.7f));
    addParameter (releaseParam = new AudioParameterFloat ("release", "release", 0.05f, 5.0f, 1.0f));
    addParameter (gainParam = new AudioParameterFloat ("gain", "gain", -30.0f, 30.0f, 0.0f));
    addParameter (genderParam = new AudioParameterInt ("gender", "gender", 0, 1, 0));
    addParameter (vowel1Param = new AudioParameterInt ("vowel1", "vowel1", 0, 3, 1));
    addParameter (vowel2Param = new AudioParameterInt ("vowel2", "vowel2", 0, 3, 0));
    addParameter (vowel3Param = new AudioParameterInt ("vowel3", "vowel3", 0, 3, 2));
    addParameter (vowelMixParam = new AudioParameterFloat ("vowelMix", "vowelMix", -1.0f, 1.0f, 0.0f));
    addParameter (vibratoDepthParam = new AudioParameterFloat ("vibDepth", "vibDepth", 0.00f, 0.05f, 0.015f));
    addParameter (vibratoRateParam = new AudioParameterFloat ("vibRate", "vibRate", 0.0f, 10.0f, 5.1f));
    initialiseSynth();
}

LaDiDaAudioProcessor::~LaDiDaAudioProcessor()
{
    attackParam = nullptr;
    decayParam = nullptr;
    sustainParam = nullptr;
    releaseParam = nullptr;
    gainParam = nullptr;
    genderParam = nullptr;
    vowel1Param = nullptr;
    vowel2Param = nullptr;
    vowel3Param = nullptr;
    vowelMixParam = nullptr;
    vibratoDepthParam = nullptr;
    vibratoRateParam = nullptr;
}

void LaDiDaAudioProcessor::initialiseSynth()
{
    const int numVoices = 8;

    // add the voices
    for (int i = numVoices; --i >= 0;)
        synth.addVoice(new FaustVoice(numVoices,*this));
    
    synth.addSound(new FaustSound());

}

void LaDiDaAudioProcessor::setParameterValues(String paraName)
{

    for (int i = 0; i < synth.getNumVoices(); ++i){
        FaustVoice* tmpVoice = (FaustVoice*)synth.getVoice(i);
        float correctVal = 0.0f;
        // get parameter value in the correct range
        // couldn't figure out how to do in Editor
        if (paraName == "attack")
            correctVal = attackParam->get();
        else if (paraName == "decay")
            correctVal = decayParam->get();
        else if (paraName == "sustain")
            correctVal = sustainParam->get();    
        else if (paraName == "release")
            correctVal = releaseParam->get();
        else if (paraName == "gain")
            correctVal = gainParam->get();
        else if (paraName == "gender")
            correctVal = (float)genderParam->get();
        else if (paraName == "vowel1")
            correctVal = (float)vowel1Param->get();
        else if (paraName == "vowel2")
            correctVal = (float)vowel2Param->get();
        else if (paraName == "vowel3")
            correctVal = (float)vowel3Param->get();
        else if (paraName == "vowelMix")
            correctVal = vowelMixParam->get();
        else if (paraName == "vibDepth")
            correctVal = vibratoDepthParam->get();
        else if (paraName == "vibRate")
            correctVal = vibratoRateParam->get();
        
        tmpVoice->setParameterValues(paraName,correctVal);
    }

}

//==============================================================================
const String LaDiDaAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool LaDiDaAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool LaDiDaAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double LaDiDaAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int LaDiDaAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int LaDiDaAudioProcessor::getCurrentProgram()
{
    return 0;
}

void LaDiDaAudioProcessor::setCurrentProgram (int /*index*/)
{
}

const String LaDiDaAudioProcessor::getProgramName (int /*index*/)
{
    return String();
}

void LaDiDaAudioProcessor::changeProgramName (int /*index*/, const String& /*newName*/)
{
}

//==============================================================================
void LaDiDaAudioProcessor::prepareToPlay (double sampleRate, int /*samplesPerBlock*/)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    synth.setCurrentPlaybackSampleRate (sampleRate);
    keyboardState.reset();
}

void LaDiDaAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
    keyboardState.reset();
}

// commented out so can compile on Windows
/*
#ifndef JucePlugin_PreferredChannelConfigurations
bool LaDiDaAudioProcessor::setPreferredBusArrangement (bool isInput, int bus, const AudioChannelSet& preferredSet)
{
    // Reject any bus arrangements that are not compatible with your plugin

    const int numChannels = preferredSet.size();

   #if JucePlugin_IsMidiEffect
    if (numChannels != 0)
        return false;
   #elif JucePlugin_IsSynth
    if (isInput || (numChannels != 1 && numChannels != 2))
        return false;
   #else
    if (numChannels != 1 && numChannels != 2)
        return false;

    if (! AudioProcessor::setPreferredBusArrangement (! isInput, bus, preferredSet))
        return false;
   #endif

    return AudioProcessor::setPreferredBusArrangement (isInput, bus, preferredSet);
}
#endif
*/
template <typename FloatType>
void LaDiDaAudioProcessor::process (AudioBuffer<FloatType>& buffer, MidiBuffer& midiMessages)
{
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

    const int numSamples = buffer.getNumSamples();
    
    // pass any incoming midi messages to keyboard state object
    keyboardState.processNextMidiBuffer(midiMessages, 0, numSamples, true);
    
    // we need to look at our midi messages and update our AudioProcessorParameters
    // so that editor can display the changes and notify host application
    MidiBuffer::Iterator iterator (midiMessages);
    MidiMessage m;
    int sampleNumber;
    
    while (iterator.getNextEvent (m, sampleNumber)){
        if (m.isController()){
            // if mod wheel, control the vowel mix value
            if (m.getControllerNumber() == 1){
                vowelMixParam->beginChangeGesture();
                vowelMixParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                vowelMixParam->endChangeGesture();
            }
            // if knob #1, control the gain value
            else if (m.getControllerNumber() == 21){
                gainParam->beginChangeGesture();
                gainParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                gainParam->endChangeGesture();
            }
            // if knob #2, control the vibrato depth
            else if (m.getControllerNumber() == 22){
                vibratoDepthParam->beginChangeGesture();
                vibratoDepthParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                vibratoDepthParam->endChangeGesture();
            }
            // if knob #3, control the vibrato rate
            else if (m.getControllerNumber() == 23){
                vibratoRateParam->beginChangeGesture();
                vibratoRateParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                vibratoRateParam->endChangeGesture();
            }
            // if knob #5, control the vowel 1 choice
            else if (m.getControllerNumber() == 25){
                vowel1Param->beginChangeGesture();
                vowel1Param->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                vowel1Param->endChangeGesture();
            }
            // if knob #6, control the vowel 2 choice
            else if (m.getControllerNumber() == 26){
                vowel2Param->beginChangeGesture();
                vowel2Param->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                vowel2Param->endChangeGesture();
            }
            // if knob #7, control the vowel 3 choice
            else if (m.getControllerNumber() == 27){
                vowel3Param->beginChangeGesture();
                vowel3Param->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                vowel3Param->endChangeGesture();
            }
            // if slider #1, control the attack value
            else if (m.getControllerNumber() == 41){
                attackParam->beginChangeGesture();
                attackParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                attackParam->endChangeGesture();
            }
            // if slider #2, control the decay value
            else if (m.getControllerNumber() == 42){
                decayParam->beginChangeGesture();
                decayParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                decayParam->endChangeGesture();
            }
            // if slider #3, control the sustain value
            else if (m.getControllerNumber() == 43){
                sustainParam->beginChangeGesture();
                sustainParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                sustainParam->endChangeGesture();
            }
            // if slider #4, control the release value
            else if (m.getControllerNumber() == 44){
                releaseParam->beginChangeGesture();
                releaseParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                releaseParam->endChangeGesture();
            }
            // if slider #5, control the gender value
            else if (m.getControllerNumber() == 45){
                genderParam->beginChangeGesture();
                genderParam->setValueNotifyingHost ((float)m.getControllerValue()/127.0f);
                genderParam->endChangeGesture();
            }
        }

    }
    
    // send data to synth and let it generate output
    synth.renderNextBlock (buffer, midiMessages, 0, numSamples);

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
}

//==============================================================================
bool LaDiDaAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* LaDiDaAudioProcessor::createEditor()
{
    return new LaDiDaAudioProcessorEditor (*this);
}

//==============================================================================
void LaDiDaAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.

    // creat an outer XML element
    XmlElement xml ("MYPLUGINSETTINGS");
    
    // add some attributes to it
    xml.setAttribute ("uiWidth", lastUIWidth);
    xml.setAttribute ("uiHeight", lastUIHeight);
    
    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i){
        if (AudioProcessorParameterWithID* p = dynamic_cast<AudioProcessorParameterWithID*> (getParameters().getUnchecked(i)))
            xml.setAttribute (p->paramID, p->getValue());
    }
    
    copyXmlToBinary (xml, destData);
}

void LaDiDaAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.

    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState != nullptr)
    {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName ("MYPLUGINSETTINGS"))
        {
            // ok, now pull out our last window size..
            lastUIWidth  = jmax (xmlState->getIntAttribute ("uiWidth", lastUIWidth), 800);
            lastUIHeight = jmax (xmlState->getIntAttribute ("uiHeight", lastUIHeight), 400);

            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID* p = dynamic_cast<AudioProcessorParameterWithID*> (getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost ((float) xmlState->getDoubleAttribute (p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new LaDiDaAudioProcessor();
}
