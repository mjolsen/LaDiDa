# LaDiDa - MUS256a, Final Project

This application is a audio plugin that implements a polyphonic midi 
formant-wave-function (FOF) singing voice synthesizer. The application 
implements the method developed in the paper [A Hybrid Filter--Wavetable 
Oscillator Technique for Formant-Wave-Function 
Synthesis](http://quintetnet.hfmt-hamburg.de/SMC2016/wp-content/uploads/2016/09/
SMC2016_proceedings.pdf#page=380). A set of synthesis rules from the [chant-lib]
(https://ccrma.stanford.edu/~rmichon/chantLib/) Open Music implmenetation of 
FOF have been implmented. This includes micromodulations on the fundamental 
frequency and on the frequency and depth of the vibrato, automatic adjustment 
of the center frequencies of the first two formants when the fundamental pitch 
rises above the first formant and automatic adjustment of the amplitudes of the 
formants to reinforce the higher formants at higher fundamental frequencies.
Random deviations of the fundamental frequency, vibrato rate and the formant 
parameters has been implmented to make the different notes when playing with 
polyphony sound like unique voices that would comprise a choir.

The current version implments the technique 
for male and female voices. The male voice changes from bass to tenor formant 
settings and the female voice changes from contralto to soprano, both at 
predefined transition frequencies. The application also features three different 
vowel selection boxes from which four different vowels (A, E, O and U) can be 
picked. There is a rotary fader which allows the user to modulate between the 
three currently selected vowel types. There is also an ADSR amplitude envelope, 
a gain control and a vibrato with depth and rate controls.

![LaDiDa Screenshot](/LaDiDaScreenshot.png "Screen of Application Interface")

All of the parameters can be controlled using a midi controller with the 
following mappings:

| CC # | Parameter Name |
|:----:|:--------------:|
|   1  |    Vowel Mix   |
|  21  |      Gain      |
|  22  |  Vibrato Depth |
|  23  |  Vibrato Rate  |
|  25  |    Vowel #1    |
|  26  |    Vowel #2    |
|  27  |    Vowel #3    |
|  41  |     Attack     |
|  42  |      Decay     |
|  43  |     Sustain    |
|  44  |     Release    |
|  45  |     Gender     |

To build the app, use the tool relevant to your OS (Mac: Xcode, Linux: 
makefile, PC: VisualStudio) where the projects are located in the Builds 
directory. The JUCE project is currently configured to create a VST plugin but 
this can be changed to the plugin architecture of your preference by opening 
the LaDiDa.projucer project file in the [JUCE projucer]
(https://www.juce.com/get-juce) and selecting the preferred 
architecture in the Project Settings page, saving the project and then building 
with the correct tool for your OS. You will most likely need to update the paths 
of the JUCE modules in the Introjucer to be the paths where you have the JUCE
resources stored.

---

Implemented by Michael Olsen (mjolsenATccrmaDOTstanfordDOTedu) based on starter 
code by Romain Michon (rmichonATccrmaDOTstanfordDOTedu) (and the Juce example 
"audio plugin demo") for Music 256a / CS 476a (fall 2016).
