// include the fof functions (which will include "stdfaust.lib")
import("fof.lib");

//------------------------------ User Entry Controls ----------------------------//
// user entry to control gender type
// 0 = male, 1 = female
gender = checkbox("gender");

// user entry to control vowel type
// 0 = a
// 1 = e
// 2 = o 
// 3 = u
vowel1 = vslider("vowel1",0,0,vowelCnt-1,1) : max(0) : min(vowelCnt-1) : int;
vowel2 = vslider("vowel2",1,0,vowelCnt-1,1) : max(0) : min(vowelCnt-1) : int;
vowel3 = vslider("vowel3",2,0,vowelCnt-1,1) : max(0) : min(vowelCnt-1) : int;
vowelMix = vslider("vowelMix",1,-1,1,0.01);

// user entry to control overall gain
gain = vslider("gain",0.0,-30.0,30.0,0.01) : ba.db2linear : si.smooth(s);

// user entry to control fundamental frequency
freq = vslider("freq",80.0,40.00,1100.0,0.01) : si.smooth(s);

// user entries to control ADSR envelope
attack  = vslider("attack",0.1,0.05,1,0.01);
decay   = vslider("decay",0.1,0.05,1,0.01);
sustain = vslider("sustain",0.7,0.05,1,0.01);
release = vslider("release",1.0,0.05,5.0,0.01);

// user entry to control release time of parameter smoothing filter
s = nentry("smooth",0,0,0.999,0.001);

//AdsrGrp = vgroup("[1] ADSR",(attack,decay,sustain,release,s));
// user entry to control noteon/noteoff
gate = button("gate"); 

// vibrato controls
vibRate = vslider("vibRate",5.1,0.1,10.0,0.01);
vibDepth = vslider("vibDepth",0.02,0.01,0.1,0.01);


//---------------------------- End User Entry Controls --------------------------//

//----------------------------------- Constants ---------------------------------//
// number of parameters, formants, filters to cycle, voices and vowels
//paramCnt = 4;
formantCnt = 5;
filtCnt = 2;
voiceCnt = 4;
vowelCnt = 4;

mcutoff = 261.625;
fcutoff = 392.0;

// normalization constant
normG = ba.db2linear(35);

// array of formant values for vowels a and o of the bass, tenor and soprano voices
      //------bass a-------// //------bass e-------// //-----bass o-------// //-----bass u-------//
fc = (600,1040,2250,2450,2750,400,1620,2400,2800,3100,400,750,2400,2600,2900,350,600,2400,2675,2950,
      //------tenor a------// //------tenor e------// //-----tenor o------// //-----tenor u------//
      650,1080,2650,2900,3250,400,1700,2600,3200,3580,400,800,2600,2800,3000,350,600,2700,2900,3300, 
      //------alto a-------// //------alto e-------// //-----alto o-------// //-----alto u-------//
      800,1150,2800,3500,4950,400,1600,2700,3300,4950,450,800,2830,3500,4950,325,700,2530,3500,4950,
      //-----soprano a-----// //-----soprano e-----// //-----soprano o----// //-----soprano u----//
      800,1150,2900,3900,4950,350,2000,2800,3600,4950,450,800,2830,3800,4950,325,700,2700,3800,4950);
a  = (  0,  -7,  -9,  -9, -20,  0, -12,  -9, -12, -18,  0,-11, -21, -20, -40,  0,-20, -32, -28, -36,
        0,  -6,  -7,  -8, -22,  0, -14, -12, -14, -20,  0,-10, -12, -12, -26,  0,-20, -17, -14, -26,
        0,  -4, -20, -36, -60,  0, -24, -30, -35, -60,  0, -9, -16, -28, -55,  0,-12, -30, -40, -64,
        0,  -6, -32, -20, -50,  0, -20, -15, -40, -56,  0,-11, -22, -22, -50,  0,-16, -35, -40, -60);
bw = ( 60,  70, 110, 120, 130, 40,  80, 100, 120, 120, 40, 80, 100, 120, 120, 40, 80, 100, 120, 120,
       80,  90, 120, 130, 140, 70,  80, 100, 120, 120, 70, 80, 100, 130, 135, 40, 60, 100, 120, 120, 
       80,  90, 120, 130, 140, 60,  80, 120, 150, 200, 70, 80, 100, 130, 135, 50, 60, 170, 180, 200,
       80,  90, 120, 130, 140, 60, 100, 120, 150, 200, 40, 80, 100, 120, 120, 50, 60, 170, 180, 200);

// array of values used to multiply BWs to get attack Bws
// min/max values per vowel and per gender
maleMins = (1.0, 1.25, 1.0, 1.5);
maleMaxes = (10.0, 2.5, 10.0, 4.0);
femaleMins = (2.0, 3.0, 2.0, 2.0);
femaleMaxes = (15.0, 12.0, 12.0, 12.0);

//--------------------------------- End Constants -------------------------------//

//------------------------------- DSP Architecture ------------------------------//
// amplitude and vibrato envelopes
ampEnv = gate : adsr(attack,decay,sustain,release) * gain;
vibEnv = gate : adsr(attack+decay,0,sustain,release);

// calculate index number of current value to be played
curStartIndex1 = formantCnt*((1-gender)*((freq>mcutoff)*vowelCnt+vowel1) +
                                gender*(2*vowelCnt+((freq>fcutoff)*vowelCnt+vowel1)));
curStartIndex2 = formantCnt*((1-gender)*((freq>mcutoff)*vowelCnt+vowel2) +
                                gender*(2*vowelCnt+((freq>fcutoff)*vowelCnt+vowel2)));
curStartIndex3 = formantCnt*((1-gender)*((freq>mcutoff)*vowelCnt+vowel3) +
                                gender*(2*vowelCnt+((freq>fcutoff)*vowelCnt+vowel3)));

// streams of parameters
curFcs1 = par(i,formantCnt,fc : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex1+i) : 
             si.smooth(s)) : (autobendF1,autobendF2,par(i,formantCnt-2,_));
curFcs2 = par(i,formantCnt,fc : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex2+i) : 
             si.smooth(s)) : (autobendF1,autobendF2,par(i,formantCnt-2,_));
curFcs3 = par(i,formantCnt,fc : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex3+i) : 
             si.smooth(s)) : (autobendF1,autobendF2,par(i,formantCnt-2,_));
curFcs = ((((curFcs3 : par(i,formantCnt,*(vowelMix))),(curFcs2 : par(i,formantCnt,*(1-vowelMix)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix >= 0))),
          (((curFcs1 : par(i,formantCnt,*(-1*vowelMix))),(curFcs2 : par(i,formantCnt,*(1+vowelMix)))) :          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix < 0)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,+(fcOffset));
curBws1 = par(i,formantCnt,bw : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex1+i) : 
             si.smooth(s));
curBws2 = par(i,formantCnt,bw : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex2+i) : 
             si.smooth(s));
curBws3 = par(i,formantCnt,bw : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex3+i) : 
             si.smooth(s));
curBws = ((((curBws3 : par(i,formantCnt,*(vowelMix))),(curBws2 : par(i,formantCnt,*(1-vowelMix)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix >= 0))),
          (((curBws1 : par(i,formantCnt,*(-1*vowelMix))),(curBws2 : par(i,formantCnt,*(1+vowelMix)))) :          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix < 0)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,+(bwOffset));
curGs1  = par(i,formantCnt, a : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex1+i) : 
             ba.db2linear : si.smooth(s)) : (_,par(i,formantCnt-1,_*ampScalar));
curGs2  = par(i,formantCnt, a : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex1+i) : 
             ba.db2linear : si.smooth(s)) : (_,par(i,formantCnt-1,_*ampScalar));
curGs3  = par(i,formantCnt, a : ba.selectn(formantCnt*vowelCnt*voiceCnt,curStartIndex1+i) : 
             ba.db2linear : si.smooth(s)) : (_,par(i,formantCnt-1,_*ampScalar));
curGs   = ((((curGs3 : par(i,formantCnt,*(vowelMix))),(curGs2 : par(i,formantCnt,*(1-vowelMix)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix >= 0))),
          (((curGs1 : par(i,formantCnt,*(-1*vowelMix))),(curGs2 : par(i,formantCnt,*(1+vowelMix)))) :
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix < 0)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+);// : par(i,formantCnt,+(gOffset));
f0Clk = clkCycle(filtCnt,vibModFreq);

// parameters to detemine amount to multiply BWs by to get
// attack BWs
maleSkirtParam = ba.if(freq <= 82.41,0.0, ba.if(freq >= 523.25,1.0,
                       (1.0/(523.25-82.41))*(freq-82.41)));
femaleSkirtParam = ba.if(freq <= 174.61,0.0, ba.if(freq >= 1046.5,1.0,
                        (1.0/(1046.5-174.61))*(freq-174.61)));
curSkirtWidth1 = ba.if(gender == 0, 
                       ((maleMaxes : ba.selectn(4,vowel1))-(maleMins : ba.selectn(4,vowel1))) *
                        maleSkirtParam + (maleMins : ba.selectn(4,vowel1)),
                       ((femaleMaxes : ba.selectn(4,vowel1))-(femaleMins : ba.selectn(4,vowel1))) *
                        femaleSkirtParam + (femaleMins : ba.selectn(4,vowel1)));

curSkirtWidth2 = ba.if(gender == 0, 
                       ((maleMaxes : ba.selectn(4,vowel2))-(maleMins : ba.selectn(4,vowel2))) *
                        maleSkirtParam + (maleMins : ba.selectn(4,vowel2)),
                       ((femaleMaxes : ba.selectn(4,vowel2))-(femaleMins : ba.selectn(4,vowel2))) *
                        femaleSkirtParam + (femaleMins : ba.selectn(4,vowel2)));
curSkirtWidth3 = ba.if(gender == 0, 
                       ((maleMaxes : ba.selectn(4,vowel3))-(maleMins : ba.selectn(4,vowel3))) *
                        maleSkirtParam + (maleMins : ba.selectn(4,vowel3)),
                       ((femaleMaxes : ba.selectn(4,vowel3))-(femaleMins : ba.selectn(4,vowel3))) *
                        femaleSkirtParam + (femaleMins : ba.selectn(4,vowel3)));
curAs1   = curBws : par(i,formantCnt, *(curSkirtWidth1));
curAs2   = curBws : par(i,formantCnt, *(curSkirtWidth2));
curAs3   = curBws : par(i,formantCnt, *(curSkirtWidth3));
curAs = ((((curAs3 : par(i,formantCnt,*(vowelMix))),(curAs2 : par(i,formantCnt,*(1-vowelMix)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix >= 0))),
          (((curAs1 : par(i,formantCnt,*(-1*vowelMix))),(curAs2 : par(i,formantCnt,*(1+vowelMix)))) :
          ro.interleave(formantCnt,2) : par(i,formantCnt,+) : par(i,formantCnt,*(vowelMix < 0)))) : 
          ro.interleave(formantCnt,2) : par(i,formantCnt,+);

// rules to autobend formant values based on frequency of fundamental
autobendF1 =  _ <: ba.if(_<=freq,freq,_);
autobendF2 = _ <: ba.if((_>=1300)&(freq>=200),_-(freq-200)*(_-1300)/1050,
                  ba.if(_<=30+2*freq,30+2*freq,_));

ampScalar = ba.if(gender == 0,3+1.1*(400-freq)/300,0.8+1.05*(1000-freq)/1250);

// function to select current parameters for formant n
curParams(n) = (curFcs : ba.selectn(formantCnt,n)),
               (curBws : ba.selectn(formantCnt,n)),
               (curAs  : ba.selectn(formantCnt,n)), 
               (curGs  : ba.selectn(formantCnt,n));

// function to create cycling filter fof stream for formant n
formantFof(n) = f0Clk <: par(i,filtCnt,((curParams(n),_) : fofSH)) :> _;
// function of fof streams for all formants
formantFofs = par(i,formantCnt,formantFof(i)) :> _;

// orig music.lib adsr
adsr(a,d,s,r,t) = env ~ (_,_) : (!,_) // the 2 'state' signals are fed back
with {
    env (p2,y) =
        (t>0) & (p2|(y>=1)),          // p2 = decay-sustain phase
        (y + p1*u - (p2&(y>s))*v*y - p3*w*y)    // y  = envelop signal
    *((p3==0)|(y>=eps)) // cut off tails to prevent denormals
    with {
        p1 = (p2==0) & (t>0) & (y<1);         // p1 = attack phase
        p3 = (t<=0) & (y>0);                  // p3 = release phase
        // #samples in attack, decay, release, must be >0
        na = ma.SR*a+(a==0.0); nd = ma.SR*d+(d==0.0); nr = ma.SR*r+(r==0.0);
        // correct zero sustain level
        z = s+(s==0.0)*ba.db2linear(-60);
        // attack, decay and (-60dB) release rates
        u = 1/na; v = 1-pow(z, 1/nd); w = 1-1/pow(z*ba.db2linear(60), 1/nr);
        // values below this threshold are considered zero in the release phase
        eps = ba.db2linear(-120);
    };
};

// foreign c++ functions that generates random numbers using a
// uniform real number distribution seeded by different generators
// so you get up to 4 uncorrelated uniformly distributed streams of numbers
my_noise1 = ffunction(float getRandomNumber1(), "randomNumbers.h","");
my_noise2 = ffunction(float getRandomNumber2(), "randomNumbers.h","");
my_noise3 = ffunction(float getRandomNumber3(), "randomNumbers.h","");
my_noise4 = ffunction(float getRandomNumber4(), "randomNumbers.h","");
my_noise5 = ffunction(float getRandomNumber5(), "randomNumbers.h","");
my_noise(x) = (my_noise1,my_noise2,my_noise3,my_noise4) : ba.selectn(4,x);

// adopted from lfnoise0 but using my_noise instead of noise
my_lfnoise0(f,n) = my_noise(n) : ba.latch(f : os.osc);
// adopted from lfnoise but using my_lfnoise0 instead of lfnoise0
my_lfnoise(f,n) = my_lfnoise0(f,n) : seq(i,5,fi.lowpass(1,f));

// jitter composed of 3 parallel b_jitters, supposed to 
// approximate 1/f distribution
jitter3(t1,t2,t3,j1,j2,j3) = output with {
    rand1 = j1*my_lfnoise(1.0/t1,0);
    rand2 = j2*my_lfnoise(1.0/t2,1);
    rand3 = j3*my_lfnoise(1.0/t3,2);
    output = (rand1+rand2+rand3)/3;
};

// vibrato with random deviations in the amplitude and
// frequency to mimic human vibrato
voice_vibrato(vfreq,vamp,vala1,vala2,tvala1,tvala2,
              valf1,valf2,tvalf1,tvalf2) = output with {
    jit1 = vala1*my_lfnoise(1.0/tvala1,0);
    jit2 = vala2*my_lfnoise(1.0/tvala2,1);
    jit_amp = vamp*(1+0.5*(jit1+jit2)) * vibEnv;
    jit3 = valf1*my_lfnoise(1.0/tvalf1,2);
    jit4 = valf2*my_lfnoise(1.0/tvalf2,3);
    jit_freq = vfreq*(1+0.5*(jit3+jit4));
    vibGain = 1.0;
    output = jit_amp*(jit_freq : os.osc);
};

freqModPct = 0.01;

// fundamental freq with micromodulations added
freqOffset = my_noise5*10;
vibFreqOffset = my_noise4*0.5;
fcOffset = my_noise3*50;
bwOffset = my_noise2*5;
gOffset = my_noise1*0.33;
modFreq = (freq+freqOffset)*(1.0+jitter3(0.5,0.111,1.218,freqModPct,freqModPct,freqModPct));
vibModPct = 0.01;
// micromodulated fundamental freq with voice vibrato added
vibModFreq = modFreq*(1.0+voice_vibrato(vibRate+vibFreqOffset,vibDepth,vibModPct,vibModPct,
                                        5.0,5.0,vibModPct,vibModPct,1.0,1.0));

// final process block
// fof stream multipled by normalization factor and ADSR gate
process = hgroup("fof",formantFofs * normG * ampEnv);
