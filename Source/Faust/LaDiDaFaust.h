//----------------------------------------------------------
//
// Code generated with Faust 0.9.92 (http://faust.grame.fr)
//----------------------------------------------------------

/* link with : "" */
#include "randomNumbers.h"
#include <math.h>
// Music 256a / CS 476a | fall 2016
// CCRMA, Stanford University
//
// Author: Romain Michon (rmichonATccrmaDOTstanfordDOTedu)
// Description: Simple Faust architecture file to easily integrate a Faust DSP module
// in a JUCE project

// needed by any Faust arch file
/************************************************************************
 ************************************************************************
    FAUST Architecture File
	Copyright (C) 2003-2011 GRAME, Centre National de Creation Musicale
    ---------------------------------------------------------------------
    This Architecture section is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3 of
	the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program; If not, see <http://www.gnu.org/licenses/>.

 ************************************************************************
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 ************************************************************************
    FAUST Architecture File
	Copyright (C) 2003-2011 GRAME, Centre National de Creation Musicale
    ---------------------------------------------------------------------
    This Architecture section is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 3 of
	the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program; If not, see <http://www.gnu.org/licenses/>.

 ************************************************************************
 ************************************************************************/
 
#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


// allows to control a Faust DSP module in a simple manner by using parameter's path
/************************************************************************
    FAUST Architecture File
    Copyright (C) 2003-2011 GRAME, Centre National de Creation Musicale
    ---------------------------------------------------------------------
    This Architecture section is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; If not, see <http://www.gnu.org/licenses/>.

    EXCEPTION : As a special exception, you may create a larger work
    that contains this FAUST architecture section and distribute
    that work under terms of your choice, so long as this FAUST
    architecture section is not modified.


 ************************************************************************
 ************************************************************************/

#ifndef FAUST_MAPUI_H
#define FAUST_MAPUI_H

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

#include <vector>
#include <map>
#include <string>

/************************************************************************
    FAUST Architecture File
    Copyright (C) 2003-2016 GRAME, Centre National de Creation Musicale
    ---------------------------------------------------------------------
    This Architecture section is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; If not, see <http://www.gnu.org/licenses/>.

    EXCEPTION : As a special exception, you may create a larger work
    that contains this FAUST architecture section and distribute
    that work under terms of your choice, so long as this FAUST
    architecture section is not modified.


 ************************************************************************
 ************************************************************************/
 
#ifndef FAUST_UI_H
#define FAUST_UI_H

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust User Interface
 * This abstract class contains only the method that the faust compiler can
 * generate to describe a DSP interface.
 ******************************************************************************/

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

//----------------------------------------------------------------
//  Generic decorator
//----------------------------------------------------------------

class DecoratorUI : public UI
{
    protected:
    
        UI* fUI;

    public:
    
        DecoratorUI(UI* ui = 0):fUI(ui)
        {}

        virtual ~DecoratorUI() { delete fUI; }

        // -- widget's layouts
        virtual void openTabBox(const char* label)          { fUI->openTabBox(label); }
        virtual void openHorizontalBox(const char* label)   { fUI->openHorizontalBox(label); }
        virtual void openVerticalBox(const char* label)     { fUI->openVerticalBox(label); }
        virtual void closeBox()                             { fUI->closeBox(); }

        // -- active widgets
        virtual void addButton(const char* label, FAUSTFLOAT* zone)         { fUI->addButton(label, zone); }
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)    { fUI->addCheckButton(label, zone); }
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
            { fUI->addVerticalSlider(label, zone, init, min, max, step); }
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) 	
            { fUI->addHorizontalSlider(label, zone, init, min, max, step); }
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) 			
            { fUI->addNumEntry(label, zone, init, min, max, step); }

        // -- passive widgets	
        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) 
            { fUI->addHorizontalBargraph(label, zone, min, max); }
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
            { fUI->addVerticalBargraph(label, zone, min, max); }

        virtual void declare(FAUSTFLOAT* zone, const char* key, const char* val) { fUI->declare(zone, key, val); }

};

#endif
/************************************************************************
    FAUST Architecture File
    Copyright (C) 2003-2011 GRAME, Centre National de Creation Musicale
    ---------------------------------------------------------------------
    This Architecture section is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; If not, see <http://www.gnu.org/licenses/>.

    EXCEPTION : As a special exception, you may create a larger work
    that contains this FAUST architecture section and distribute
    that work under terms of your choice, so long as this FAUST
    architecture section is not modified.


 ************************************************************************
 ************************************************************************/

#ifndef FAUST_PATHBUILDER_H
#define FAUST_PATHBUILDER_H

#include <vector>
#include <string>
#include <algorithm>

/*******************************************************************************
 * PathBuilder : Faust User Interface
 * Helper class to build complete hierarchical path for UI items.
 ******************************************************************************/

class PathBuilder
{

    protected:
    
        std::vector<std::string> fControlsLevel;
       
    public:
    
        PathBuilder() {}
        virtual ~PathBuilder() {}
    
        std::string buildPath(const std::string& label) 
        {
            std::string res = "/";
            for (size_t i = 0; i < fControlsLevel.size(); i++) {
                res += fControlsLevel[i];
                res += "/";
            }
            res += label;
            replace(res.begin(), res.end(), ' ', '_');
            return res;
        }
    
};

#endif  // FAUST_PATHBUILDER_H

/*******************************************************************************
 * MapUI : Faust User Interface
 * This class creates a map of complete hierarchical path and zones for each UI items.
 ******************************************************************************/

class MapUI : public UI, public PathBuilder
{
    
    protected:
    
        // Complete path map
        std::map<std::string, FAUSTFLOAT*> fPathZoneMap;
    
        // Label zone map
        std::map<std::string, FAUSTFLOAT*> fLabelZoneMap;
    
    public:
        
        MapUI() {};
        virtual ~MapUI() {};
        
        // -- widget's layouts
        void openTabBox(const char* label)
        {
            fControlsLevel.push_back(label);
        }
        void openHorizontalBox(const char* label)
        {
            fControlsLevel.push_back(label);
        }
        void openVerticalBox(const char* label)
        {
            fControlsLevel.push_back(label);
        }
        void closeBox()
        {
            fControlsLevel.pop_back();
        }
        
        // -- active widgets
        void addButton(const char* label, FAUSTFLOAT* zone)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addCheckButton(const char* label, FAUSTFLOAT* zone)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT fmin, FAUSTFLOAT fmax, FAUSTFLOAT step)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        
        // -- passive widgets
        void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT fmin, FAUSTFLOAT fmax)
        {
            fPathZoneMap[buildPath(label)] = zone;
            fLabelZoneMap[label] = zone;
        }
        
        // -- metadata declarations
        void declare(FAUSTFLOAT* zone, const char* key, const char* val)
        {}
        
        // set/get
        void setParamValue(const std::string& path, float value)
        {
            if (fPathZoneMap.find(path) != fPathZoneMap.end()) {
                *fPathZoneMap[path] = value;
            } else if (fLabelZoneMap.find(path) != fLabelZoneMap.end()) {
                *fLabelZoneMap[path] = value;
            }
        }
        
        float getParamValue(const std::string& path)
        {
            if (fPathZoneMap.find(path) != fPathZoneMap.end()) {
                return *fPathZoneMap[path];
            } else if (fLabelZoneMap.find(path) != fLabelZoneMap.end()) {
                return *fLabelZoneMap[path];
            } else {
                return 0.;
            }
        }
    
        // map access 
        std::map<std::string, FAUSTFLOAT*>& getMap() { return fPathZoneMap; }
        
        int getParamsCount() { return fPathZoneMap.size(); }
        
        std::string getParamAdress(int index) 
        { 
            std::map<std::string, FAUSTFLOAT*>::iterator it = fPathZoneMap.begin();
            while (index-- > 0 && it++ != fPathZoneMap.end()) {}
            return (*it).first;
        }
};

#endif // FAUST_MAPUI_H

// needed by any Faust arch file
/************************************************************************
    IMPORTANT NOTE : this file contains two clearly delimited sections :
    the ARCHITECTURE section (in two parts) and the USER section. Each section
    is governed by its own copyright and license. Please check individually
    each section for license and copyright information.
*************************************************************************/

/*******************BEGIN ARCHITECTURE SECTION (part 1/2)****************/

/************************************************************************
    FAUST Architecture File
    Copyright (C) 2003-2011 GRAME, Centre National de Creation Musicale
    ---------------------------------------------------------------------
    This Architecture section is free software; you can redistribute it
    and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; If not, see <http://www.gnu.org/licenses/>.

    EXCEPTION : As a special exception, you may create a larger work
    that contains this FAUST architecture section and distribute
    that work under terms of your choice, so long as this FAUST
    architecture section is not modified.

 ************************************************************************
 ************************************************************************/
 
/******************************************************************************
*******************************************************************************

								FAUST DSP

*******************************************************************************
*******************************************************************************/

#ifndef __dsp__
#define __dsp__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the UI* parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the UI* user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /** Global init, calls the following methods :
         * - static class 'classInit' : static table initialisation
         * - 'instanceInit' : constants and instance table initialisation
         *
         * @param samplingRate - the sampling rate in Herz
         */
        virtual void init(int samplingRate) = 0;
    
        /** Init instance state
         *
         * @param samplingRate - the sampling rate in Herz
         */
        virtual void instanceInit(int samplingRate) = 0;
    
        /** Init instance constant state
         *
         * @param samplingRate - the sampling rate in Herz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
    
        /**  
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value metadata).
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with sucessive in/out audio buffers.
         *
         * @param count - the nomber of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, doucbe or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, doucbe or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation : alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the nomber of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, doucbe or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, doucbe or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { return fDSP->metadata(m); }
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
       
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif

// tags used by the Faust compiler to paste the generated c++ code
#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif  


#ifndef FAUSTCLASS 
#define FAUSTCLASS LaDiDaFaust
#endif

class LaDiDaFaust : public dsp {
  private:
	class SIG0 {
	  private:
		int fSamplingFreq;
		int 	iRec0[2];
	  public:
		int getNumInputs() { return 0; }
		int getNumOutputs() { return 1; }
		void init(int samplingFreq) {
			fSamplingFreq = samplingFreq;
			for (int i=0; i<2; i++) iRec0[i] = 0;
		}
		void fill (int count, float output[]) {
			for (int i=0; i<count; i++) {
				iRec0[0] = (iRec0[1] + 1);
				output[i] = sinf((9.58738e-05f * float((iRec0[0] + -1))));
				// post processing
				iRec0[1] = iRec0[0];
			}
		}
	};


	static float 	ftbl0[65536];
	int 	iVec0[2];
	float 	fConst0;
	float 	fConst1;
	float 	fConst2;
	float 	fConst3;
	float 	fRec12[2];
	float 	fConst4;
	float 	fRec13[2];
	float 	fRec11[2];
	float 	fConst5;
	float 	fConst6;
	float 	fConst7;
	float 	fConst8;
	float 	fConst9;
	float 	fRec10[2];
	float 	fRec9[2];
	float 	fRec8[2];
	float 	fRec7[2];
	float 	fRec6[2];
	float 	fRec19[2];
	float 	fRec18[2];
	float 	fRec17[2];
	float 	fRec16[2];
	float 	fRec15[2];
	float 	fRec14[2];
	FAUSTFLOAT 	fslider0;
	float 	fRec4[2];
	float 	fRec5[2];
	FAUSTFLOAT 	fbutton0;
	int 	iRec20[2];
	FAUSTFLOAT 	fslider1;
	FAUSTFLOAT 	fslider2;
	FAUSTFLOAT 	fslider3;
	FAUSTFLOAT 	fslider4;
	float 	fRec21[2];
	float 	fConst10;
	float 	fConst11;
	float 	fConst12;
	float 	fRec28[2];
	float 	fConst13;
	float 	fRec29[2];
	float 	fRec27[2];
	float 	fConst14;
	float 	fConst15;
	float 	fConst16;
	float 	fConst17;
	float 	fRec26[2];
	float 	fRec25[2];
	float 	fRec24[2];
	float 	fRec23[2];
	float 	fRec22[2];
	float 	fRec35[2];
	float 	fRec34[2];
	float 	fRec33[2];
	float 	fRec32[2];
	float 	fRec31[2];
	float 	fRec30[2];
	FAUSTFLOAT 	fslider5;
	FAUSTFLOAT 	fentry0;
	FAUSTFLOAT 	fslider6;
	float 	fRec36[2];
	float 	fConst18;
	float 	fConst19;
	float 	fConst20;
	float 	fRec43[2];
	float 	fConst21;
	float 	fRec44[2];
	float 	fRec42[2];
	float 	fConst22;
	float 	fConst23;
	float 	fConst24;
	float 	fConst25;
	float 	fRec41[2];
	float 	fRec40[2];
	float 	fRec39[2];
	float 	fRec38[2];
	float 	fRec37[2];
	float 	fConst26;
	float 	fConst27;
	float 	fConst28;
	float 	fRec51[2];
	float 	fConst29;
	float 	fRec52[2];
	float 	fRec50[2];
	float 	fConst30;
	float 	fConst31;
	float 	fConst32;
	float 	fConst33;
	float 	fRec49[2];
	float 	fRec48[2];
	float 	fRec47[2];
	float 	fRec46[2];
	float 	fRec45[2];
	float 	fConst34;
	float 	fConst35;
	float 	fConst36;
	float 	fRec59[2];
	float 	fConst37;
	float 	fRec60[2];
	float 	fRec58[2];
	float 	fConst38;
	float 	fConst39;
	float 	fConst40;
	float 	fConst41;
	float 	fRec57[2];
	float 	fRec56[2];
	float 	fRec55[2];
	float 	fRec54[2];
	float 	fRec53[2];
	float 	fConst42;
	float 	fRec3[2];
	int 	iVec1[2];
	int 	iRec2[2];
	int 	iVec2[3];
	FAUSTFLOAT 	fslider7;
	FAUSTFLOAT 	fcheckbox0;
	float 	fRec61[2];
	FAUSTFLOAT 	fslider8;
	FAUSTFLOAT 	fslider9;
	float 	fRec62[2];
	FAUSTFLOAT 	fslider10;
	float 	fRec63[2];
	float 	fConst43;
	float 	fRec1[2];
	float 	fRec65[2];
	float 	fRec66[2];
	float 	fRec67[2];
	float 	fRec64[2];
	float 	fRec68[2];
	float 	fRec69[3];
	int 	iVec3[3];
	float 	fRec70[2];
	float 	fRec71[2];
	float 	fRec72[2];
	float 	fRec73[3];
	float 	fRec74[2];
	float 	fRec76[2];
	float 	fRec77[2];
	float 	fRec78[2];
	float 	fRec75[2];
	float 	fRec80[2];
	float 	fRec81[2];
	float 	fRec82[2];
	float 	fRec79[2];
	float 	fRec83[2];
	float 	fRec84[3];
	float 	fRec85[2];
	float 	fRec86[2];
	float 	fRec87[2];
	float 	fRec88[3];
	float 	fRec89[2];
	float 	fRec91[2];
	float 	fRec92[2];
	float 	fRec93[2];
	float 	fRec90[2];
	float 	fRec95[2];
	float 	fRec96[2];
	float 	fRec97[2];
	float 	fRec94[2];
	float 	fRec98[2];
	float 	fRec99[3];
	float 	fRec100[2];
	float 	fRec101[2];
	float 	fRec102[2];
	float 	fRec103[3];
	float 	fRec104[2];
	float 	fRec106[2];
	float 	fRec107[2];
	float 	fRec108[2];
	float 	fRec105[2];
	float 	fRec110[2];
	float 	fRec111[2];
	float 	fRec112[2];
	float 	fRec109[2];
	float 	fRec113[2];
	float 	fRec114[3];
	float 	fRec115[2];
	float 	fRec116[2];
	float 	fRec117[2];
	float 	fRec118[3];
	float 	fRec119[2];
	float 	fRec121[2];
	float 	fRec122[2];
	float 	fRec123[2];
	float 	fRec120[2];
	float 	fRec125[2];
	float 	fRec126[2];
	float 	fRec127[2];
	float 	fRec124[2];
	float 	fRec128[2];
	float 	fRec129[3];
	float 	fRec130[2];
	float 	fRec131[2];
	float 	fRec132[2];
	float 	fRec133[3];
	float 	fRec134[2];
	FAUSTFLOAT 	fslider11;
	float 	fRec135[2];
	int 	iRec136[2];
	float 	fRec137[2];
	int fSamplingFreq;

  public:
	virtual void metadata(Meta* m) { 
		m->declare("fof.lib/name", "Faust FOF Library");
		m->declare("fof.lib/author", "Michael J. Olsen (mjolsen at ccrma.stanford.edu)");
		m->declare("fof.lib/copyright", "Michael Jorgen Olsen");
		m->declare("fof.lib/version", "1.0");
		m->declare("fof.lib/license", "STK-4.3");
		m->declare("fof.lib/reference", "http://quintetnet.hfmt-hamburg.de/SMC2016/wp-content/uploads/2016/09/SMC2016_proceedings.pdf#page=380");
		m->declare("signal.lib/name", "Faust Signal Routing Library");
		m->declare("signal.lib/version", "0.0");
		m->declare("basic.lib/name", "Faust Basic Element Library");
		m->declare("basic.lib/version", "0.0");
		m->declare("route.lib/name", "Faust Signal Routing Library");
		m->declare("route.lib/version", "0.0");
		m->declare("miscoscillator.lib/name", "Faust Oscillator Library");
		m->declare("miscoscillator.lib/version", "0.0");
		m->declare("filter.lib/name", "Faust Filter Library");
		m->declare("filter.lib/version", "2.0");
		m->declare("math.lib/name", "Faust Math Library");
		m->declare("math.lib/version", "2.0");
		m->declare("math.lib/author", "GRAME");
		m->declare("math.lib/copyright", "GRAME");
		m->declare("math.lib/license", "LGPL with exception");
	}

	virtual int getNumInputs() { return 0; }
	virtual int getNumOutputs() { return 1; }
	static void classInit(int samplingFreq) {
		SIG0 sig0;
		sig0.init(samplingFreq);
		sig0.fill(65536,ftbl0);
	}
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		fConst0 = min(1.92e+05f, max(1.0f, (float)fSamplingFreq));
		fConst1 = (6.2831855f / fConst0);
		fConst2 = cosf(fConst1);
		fConst3 = sinf(fConst1);
		fConst4 = (0 - fConst3);
		fConst5 = (3.1415927f / fConst0);
		fConst6 = (1.0f / tanf(fConst5));
		fConst7 = (fConst6 + 1);
		fConst8 = (1.0f / fConst7);
		fConst9 = (0 - ((1 - fConst6) / fConst7));
		fConst10 = (1.2566371f / fConst0);
		fConst11 = cosf(fConst10);
		fConst12 = sinf(fConst10);
		fConst13 = (0 - fConst12);
		fConst14 = (1.0f / tanf((0.62831855f / fConst0)));
		fConst15 = (fConst14 + 1);
		fConst16 = (1.0f / fConst15);
		fConst17 = (0 - ((1 - fConst14) / fConst15));
		fConst18 = (5.1586084f / fConst0);
		fConst19 = cosf(fConst18);
		fConst20 = sinf(fConst18);
		fConst21 = (0 - fConst20);
		fConst22 = (1.0f / tanf((2.5793042f / fConst0)));
		fConst23 = (fConst22 + 1);
		fConst24 = (1.0f / fConst23);
		fConst25 = (0 - ((1 - fConst22) / fConst23));
		fConst26 = (56.605274f / fConst0);
		fConst27 = cosf(fConst26);
		fConst28 = sinf(fConst26);
		fConst29 = (0 - fConst28);
		fConst30 = (1.0f / tanf((28.302637f / fConst0)));
		fConst31 = (fConst30 + 1);
		fConst32 = (1.0f / fConst31);
		fConst33 = (0 - ((1 - fConst30) / fConst31));
		fConst34 = (12.566371f / fConst0);
		fConst35 = cosf(fConst34);
		fConst36 = sinf(fConst34);
		fConst37 = (0 - fConst36);
		fConst38 = (1.0f / tanf(fConst1));
		fConst39 = (fConst38 + 1);
		fConst40 = (1.0f / fConst39);
		fConst41 = (0 - ((1 - fConst38) / fConst39));
		fConst42 = (1.0f / fConst0);
		fConst43 = (1.0f / float(fConst0));
	}
	virtual void instanceResetUserInterface() {
		fslider0 = 5.1f;
		fbutton0 = 0.0;
		fslider1 = 0.7f;
		fslider2 = 1.0f;
		fslider3 = 0.1f;
		fslider4 = 0.1f;
		fslider5 = 0.02f;
		fentry0 = 0.0f;
		fslider6 = 8e+01f;
		fslider7 = 1.0f;
		fcheckbox0 = 0.0;
		fslider8 = 1.0f;
		fslider9 = 0.0f;
		fslider10 = 2.0f;
		fslider11 = 0.0f;
	}
	virtual void instanceClear() {
		for (int i=0; i<2; i++) iVec0[i] = 0;
		for (int i=0; i<2; i++) fRec12[i] = 0;
		for (int i=0; i<2; i++) fRec13[i] = 0;
		for (int i=0; i<2; i++) fRec11[i] = 0;
		for (int i=0; i<2; i++) fRec10[i] = 0;
		for (int i=0; i<2; i++) fRec9[i] = 0;
		for (int i=0; i<2; i++) fRec8[i] = 0;
		for (int i=0; i<2; i++) fRec7[i] = 0;
		for (int i=0; i<2; i++) fRec6[i] = 0;
		for (int i=0; i<2; i++) fRec19[i] = 0;
		for (int i=0; i<2; i++) fRec18[i] = 0;
		for (int i=0; i<2; i++) fRec17[i] = 0;
		for (int i=0; i<2; i++) fRec16[i] = 0;
		for (int i=0; i<2; i++) fRec15[i] = 0;
		for (int i=0; i<2; i++) fRec14[i] = 0;
		for (int i=0; i<2; i++) fRec4[i] = 0;
		for (int i=0; i<2; i++) fRec5[i] = 0;
		for (int i=0; i<2; i++) iRec20[i] = 0;
		for (int i=0; i<2; i++) fRec21[i] = 0;
		for (int i=0; i<2; i++) fRec28[i] = 0;
		for (int i=0; i<2; i++) fRec29[i] = 0;
		for (int i=0; i<2; i++) fRec27[i] = 0;
		for (int i=0; i<2; i++) fRec26[i] = 0;
		for (int i=0; i<2; i++) fRec25[i] = 0;
		for (int i=0; i<2; i++) fRec24[i] = 0;
		for (int i=0; i<2; i++) fRec23[i] = 0;
		for (int i=0; i<2; i++) fRec22[i] = 0;
		for (int i=0; i<2; i++) fRec35[i] = 0;
		for (int i=0; i<2; i++) fRec34[i] = 0;
		for (int i=0; i<2; i++) fRec33[i] = 0;
		for (int i=0; i<2; i++) fRec32[i] = 0;
		for (int i=0; i<2; i++) fRec31[i] = 0;
		for (int i=0; i<2; i++) fRec30[i] = 0;
		for (int i=0; i<2; i++) fRec36[i] = 0;
		for (int i=0; i<2; i++) fRec43[i] = 0;
		for (int i=0; i<2; i++) fRec44[i] = 0;
		for (int i=0; i<2; i++) fRec42[i] = 0;
		for (int i=0; i<2; i++) fRec41[i] = 0;
		for (int i=0; i<2; i++) fRec40[i] = 0;
		for (int i=0; i<2; i++) fRec39[i] = 0;
		for (int i=0; i<2; i++) fRec38[i] = 0;
		for (int i=0; i<2; i++) fRec37[i] = 0;
		for (int i=0; i<2; i++) fRec51[i] = 0;
		for (int i=0; i<2; i++) fRec52[i] = 0;
		for (int i=0; i<2; i++) fRec50[i] = 0;
		for (int i=0; i<2; i++) fRec49[i] = 0;
		for (int i=0; i<2; i++) fRec48[i] = 0;
		for (int i=0; i<2; i++) fRec47[i] = 0;
		for (int i=0; i<2; i++) fRec46[i] = 0;
		for (int i=0; i<2; i++) fRec45[i] = 0;
		for (int i=0; i<2; i++) fRec59[i] = 0;
		for (int i=0; i<2; i++) fRec60[i] = 0;
		for (int i=0; i<2; i++) fRec58[i] = 0;
		for (int i=0; i<2; i++) fRec57[i] = 0;
		for (int i=0; i<2; i++) fRec56[i] = 0;
		for (int i=0; i<2; i++) fRec55[i] = 0;
		for (int i=0; i<2; i++) fRec54[i] = 0;
		for (int i=0; i<2; i++) fRec53[i] = 0;
		for (int i=0; i<2; i++) fRec3[i] = 0;
		for (int i=0; i<2; i++) iVec1[i] = 0;
		for (int i=0; i<2; i++) iRec2[i] = 0;
		for (int i=0; i<3; i++) iVec2[i] = 0;
		for (int i=0; i<2; i++) fRec61[i] = 0;
		for (int i=0; i<2; i++) fRec62[i] = 0;
		for (int i=0; i<2; i++) fRec63[i] = 0;
		for (int i=0; i<2; i++) fRec1[i] = 0;
		for (int i=0; i<2; i++) fRec65[i] = 0;
		for (int i=0; i<2; i++) fRec66[i] = 0;
		for (int i=0; i<2; i++) fRec67[i] = 0;
		for (int i=0; i<2; i++) fRec64[i] = 0;
		for (int i=0; i<2; i++) fRec68[i] = 0;
		for (int i=0; i<3; i++) fRec69[i] = 0;
		for (int i=0; i<3; i++) iVec3[i] = 0;
		for (int i=0; i<2; i++) fRec70[i] = 0;
		for (int i=0; i<2; i++) fRec71[i] = 0;
		for (int i=0; i<2; i++) fRec72[i] = 0;
		for (int i=0; i<3; i++) fRec73[i] = 0;
		for (int i=0; i<2; i++) fRec74[i] = 0;
		for (int i=0; i<2; i++) fRec76[i] = 0;
		for (int i=0; i<2; i++) fRec77[i] = 0;
		for (int i=0; i<2; i++) fRec78[i] = 0;
		for (int i=0; i<2; i++) fRec75[i] = 0;
		for (int i=0; i<2; i++) fRec80[i] = 0;
		for (int i=0; i<2; i++) fRec81[i] = 0;
		for (int i=0; i<2; i++) fRec82[i] = 0;
		for (int i=0; i<2; i++) fRec79[i] = 0;
		for (int i=0; i<2; i++) fRec83[i] = 0;
		for (int i=0; i<3; i++) fRec84[i] = 0;
		for (int i=0; i<2; i++) fRec85[i] = 0;
		for (int i=0; i<2; i++) fRec86[i] = 0;
		for (int i=0; i<2; i++) fRec87[i] = 0;
		for (int i=0; i<3; i++) fRec88[i] = 0;
		for (int i=0; i<2; i++) fRec89[i] = 0;
		for (int i=0; i<2; i++) fRec91[i] = 0;
		for (int i=0; i<2; i++) fRec92[i] = 0;
		for (int i=0; i<2; i++) fRec93[i] = 0;
		for (int i=0; i<2; i++) fRec90[i] = 0;
		for (int i=0; i<2; i++) fRec95[i] = 0;
		for (int i=0; i<2; i++) fRec96[i] = 0;
		for (int i=0; i<2; i++) fRec97[i] = 0;
		for (int i=0; i<2; i++) fRec94[i] = 0;
		for (int i=0; i<2; i++) fRec98[i] = 0;
		for (int i=0; i<3; i++) fRec99[i] = 0;
		for (int i=0; i<2; i++) fRec100[i] = 0;
		for (int i=0; i<2; i++) fRec101[i] = 0;
		for (int i=0; i<2; i++) fRec102[i] = 0;
		for (int i=0; i<3; i++) fRec103[i] = 0;
		for (int i=0; i<2; i++) fRec104[i] = 0;
		for (int i=0; i<2; i++) fRec106[i] = 0;
		for (int i=0; i<2; i++) fRec107[i] = 0;
		for (int i=0; i<2; i++) fRec108[i] = 0;
		for (int i=0; i<2; i++) fRec105[i] = 0;
		for (int i=0; i<2; i++) fRec110[i] = 0;
		for (int i=0; i<2; i++) fRec111[i] = 0;
		for (int i=0; i<2; i++) fRec112[i] = 0;
		for (int i=0; i<2; i++) fRec109[i] = 0;
		for (int i=0; i<2; i++) fRec113[i] = 0;
		for (int i=0; i<3; i++) fRec114[i] = 0;
		for (int i=0; i<2; i++) fRec115[i] = 0;
		for (int i=0; i<2; i++) fRec116[i] = 0;
		for (int i=0; i<2; i++) fRec117[i] = 0;
		for (int i=0; i<3; i++) fRec118[i] = 0;
		for (int i=0; i<2; i++) fRec119[i] = 0;
		for (int i=0; i<2; i++) fRec121[i] = 0;
		for (int i=0; i<2; i++) fRec122[i] = 0;
		for (int i=0; i<2; i++) fRec123[i] = 0;
		for (int i=0; i<2; i++) fRec120[i] = 0;
		for (int i=0; i<2; i++) fRec125[i] = 0;
		for (int i=0; i<2; i++) fRec126[i] = 0;
		for (int i=0; i<2; i++) fRec127[i] = 0;
		for (int i=0; i<2; i++) fRec124[i] = 0;
		for (int i=0; i<2; i++) fRec128[i] = 0;
		for (int i=0; i<3; i++) fRec129[i] = 0;
		for (int i=0; i<2; i++) fRec130[i] = 0;
		for (int i=0; i<2; i++) fRec131[i] = 0;
		for (int i=0; i<2; i++) fRec132[i] = 0;
		for (int i=0; i<3; i++) fRec133[i] = 0;
		for (int i=0; i<2; i++) fRec134[i] = 0;
		for (int i=0; i<2; i++) fRec135[i] = 0;
		for (int i=0; i<2; i++) iRec136[i] = 0;
		for (int i=0; i<2; i++) fRec137[i] = 0;
	}
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	virtual LaDiDaFaust* clone() {
		return new LaDiDaFaust();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
	}
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openHorizontalBox("fof");
		ui_interface->addVerticalSlider("attack", &fslider3, 0.1f, 0.05f, 1.0f, 0.01f);
		ui_interface->addVerticalSlider("decay", &fslider4, 0.1f, 0.05f, 1.0f, 0.01f);
		ui_interface->addVerticalSlider("freq", &fslider6, 8e+01f, 4e+01f, 1.1e+03f, 0.01f);
		ui_interface->addVerticalSlider("gain", &fslider11, 0.0f, -3e+01f, 3e+01f, 0.01f);
		ui_interface->addButton("gate", &fbutton0);
		ui_interface->addCheckButton("gender", &fcheckbox0);
		ui_interface->addVerticalSlider("release", &fslider2, 1.0f, 0.05f, 5.0f, 0.01f);
		ui_interface->addNumEntry("smooth", &fentry0, 0.0f, 0.0f, 0.999f, 0.001f);
		ui_interface->addVerticalSlider("sustain", &fslider1, 0.7f, 0.05f, 1.0f, 0.01f);
		ui_interface->addVerticalSlider("vibDepth", &fslider5, 0.02f, 0.01f, 0.1f, 0.01f);
		ui_interface->addVerticalSlider("vibRate", &fslider0, 5.1f, 0.1f, 1e+01f, 0.01f);
		ui_interface->addVerticalSlider("vowel1", &fslider9, 0.0f, 0.0f, 3.0f, 1.0f);
		ui_interface->addVerticalSlider("vowel2", &fslider7, 1.0f, 0.0f, 3.0f, 1.0f);
		ui_interface->addVerticalSlider("vowel3", &fslider10, 2.0f, 0.0f, 3.0f, 1.0f);
		ui_interface->addVerticalSlider("vowelMix", &fslider8, 1.0f, -1.0f, 1.0f, 0.01f);
		ui_interface->closeBox();
	}
	virtual void compute (int count, FAUSTFLOAT** input, FAUSTFLOAT** output) {
		float 	fSlow0 = float(fslider0);
		float 	fSlow1 = float(fbutton0);
		int 	iSlow2 = (fSlow1 > 0);
		int 	iSlow3 = (fSlow1 <= 0);
		float 	fSlow4 = float(fslider1);
		float 	fSlow5 = (fSlow4 + (0.001f * (fSlow4 == 0.0f)));
		float 	fSlow6 = float(fslider2);
		float 	fSlow7 = (1 - (1.0f / powf((1e+03f * fSlow5),(1.0f / ((fSlow6 == 0.0f) + (fConst0 * fSlow6))))));
		float 	fSlow8 = (1 - fSlow5);
		float 	fSlow9 = float(fslider3);
		float 	fSlow10 = float(fslider4);
		float 	fSlow11 = (fSlow10 + fSlow9);
		float 	fSlow12 = (1.0f / ((fConst0 * fSlow11) + (fSlow11 == 0.0f)));
		float 	fSlow13 = float(fslider5);
		float 	fSlow14 = float(fentry0);
		float 	fSlow15 = (1.0f - fSlow14);
		float 	fSlow16 = (fSlow15 * float(fslider6));
		int 	iSlow17 = int(min((float)3, max((float)0, float(fslider7))));
		float 	fSlow18 = float(fcheckbox0);
		float 	fSlow19 = (1 - fSlow18);
		float 	fSlow20 = float(fslider8);
		float 	fSlow21 = (fSlow20 + 1);
		int 	iSlow22 = int(min((float)3, max((float)0, float(fslider9))));
		float 	fSlow23 = (0 - fSlow20);
		int 	iSlow24 = (fSlow20 < 0);
		float 	fSlow25 = (1 - fSlow20);
		int 	iSlow26 = int(min((float)3, max((float)0, float(fslider10))));
		int 	iSlow27 = (fSlow20 >= 0);
		int 	iSlow28 = int((iSlow17 >= 1));
		int 	iSlow29 = int((iSlow17 >= 2));
		float 	fSlow30 = ((iSlow29)?2.0f:((iSlow28)?3.0f:2.0f));
		float 	fSlow31 = (((iSlow29)?12.0f:((iSlow28)?12.0f:15.0f)) - fSlow30);
		int 	iSlow32 = int((iSlow17 >= 3));
		float 	fSlow33 = ((iSlow29)?((iSlow32)?1.5f:1.0f):((iSlow28)?1.25f:1.0f));
		float 	fSlow34 = (((iSlow29)?((iSlow32)?4.0f:1e+01f):((iSlow28)?2.5f:1e+01f)) - fSlow33);
		int 	iSlow35 = int((fSlow18 == 0));
		int 	iSlow36 = int((iSlow22 >= 1));
		int 	iSlow37 = int((iSlow22 >= 2));
		float 	fSlow38 = ((iSlow37)?2.0f:((iSlow36)?3.0f:2.0f));
		float 	fSlow39 = (((iSlow37)?12.0f:((iSlow36)?12.0f:15.0f)) - fSlow38);
		int 	iSlow40 = int((iSlow22 >= 3));
		float 	fSlow41 = ((iSlow37)?((iSlow40)?1.5f:1.0f):((iSlow36)?1.25f:1.0f));
		float 	fSlow42 = (((iSlow37)?((iSlow40)?4.0f:1e+01f):((iSlow36)?2.5f:1e+01f)) - fSlow41);
		int 	iSlow43 = int((iSlow26 >= 1));
		int 	iSlow44 = int((iSlow26 >= 2));
		float 	fSlow45 = ((iSlow44)?2.0f:((iSlow43)?3.0f:2.0f));
		float 	fSlow46 = (((iSlow44)?12.0f:((iSlow43)?12.0f:15.0f)) - fSlow45);
		int 	iSlow47 = int((iSlow26 >= 3));
		float 	fSlow48 = ((iSlow44)?((iSlow47)?1.5f:1.0f):((iSlow43)?1.25f:1.0f));
		float 	fSlow49 = (((iSlow44)?((iSlow47)?4.0f:1e+01f):((iSlow43)?2.5f:1e+01f)) - fSlow48);
		float 	fSlow50 = (fSlow15 * powf(10,(0.05f * float(fslider11))));
		float 	fSlow51 = (1 - powf(fSlow5,(1.0f / ((fSlow10 == 0.0f) + (fConst0 * fSlow10)))));
		float 	fSlow52 = (1.0f / ((fSlow9 == 0.0f) + (fConst0 * fSlow9)));
		float 	fSlow53 = (56.23413f * (iSlow24 + iSlow27));
		FAUSTFLOAT* output0 = output[0];
		for (int i=0; i<count; i++) {
			iVec0[0] = 1;
			int iTemp0 = (1 - iVec0[1]);
			fRec12[0] = ((fConst3 * fRec13[1]) + (fConst2 * fRec12[1]));
			fRec13[0] = (((fConst2 * fRec13[1]) + (fConst4 * fRec12[1])) + iTemp0);
			int iTemp1 = ((fRec12[1] <= 0) & (fRec12[0] > 0));
			int iTemp2 = (1 - iTemp1);
			float fTemp3 = getRandomNumber4();
			fRec11[0] = ((fTemp3 * iTemp1) + (iTemp2 * fRec11[1]));
			fRec10[0] = ((fConst9 * fRec10[1]) + (fConst8 * (fRec11[1] + fRec11[0])));
			fRec9[0] = ((fConst9 * fRec9[1]) + (fConst8 * (fRec10[1] + fRec10[0])));
			fRec8[0] = ((fConst9 * fRec8[1]) + (fConst8 * (fRec9[1] + fRec9[0])));
			fRec7[0] = ((fConst9 * fRec7[1]) + (fConst8 * (fRec8[1] + fRec8[0])));
			fRec6[0] = ((fConst9 * fRec6[1]) + (fConst8 * (fRec7[1] + fRec7[0])));
			float fTemp4 = getRandomNumber3();
			fRec19[0] = ((fRec19[1] * iTemp2) + (fTemp4 * iTemp1));
			fRec18[0] = ((fConst9 * fRec18[1]) + (fConst8 * (fRec19[1] + fRec19[0])));
			fRec17[0] = ((fConst9 * fRec17[1]) + (fConst8 * (fRec18[1] + fRec18[0])));
			fRec16[0] = ((fConst9 * fRec16[1]) + (fConst8 * (fRec17[1] + fRec17[0])));
			fRec15[0] = ((fConst9 * fRec15[1]) + (fConst8 * (fRec16[1] + fRec16[0])));
			fRec14[0] = ((fConst9 * fRec14[1]) + (fConst8 * (fRec15[1] + fRec15[0])));
			float fTemp5 = ((fConst1 * ((0.5f * fTemp3) + fSlow0)) * ((0.005f * (fRec14[0] + fRec6[0])) + 1));
			float fTemp6 = cosf(fTemp5);
			float fTemp7 = sinf(fTemp5);
			fRec4[0] = ((fRec5[1] * fTemp7) + (fRec4[1] * fTemp6));
			fRec5[0] = (((fRec5[1] * fTemp6) + (fRec4[1] * (0 - fTemp7))) + iTemp0);
			iRec20[0] = (iSlow2 & (iRec20[1] | (fRec21[1] >= 1)));
			int iTemp8 = (iSlow3 & (fRec21[1] > 0));
			fRec21[0] = (((iTemp8 == 0) | (fRec21[1] >= 1e-06f)) * ((fSlow12 * (((iRec20[1] == 0) & iSlow2) & (fRec21[1] < 1))) + (fRec21[1] * (1 - ((fSlow8 * (iRec20[1] & (fRec21[1] > fSlow4))) + (fSlow7 * iTemp8))))));
			fRec28[0] = ((fConst12 * fRec29[1]) + (fConst11 * fRec28[1]));
			fRec29[0] = (((fConst11 * fRec29[1]) + (fConst13 * fRec28[1])) + iTemp0);
			int iTemp9 = ((fRec28[1] <= 0) & (fRec28[0] > 0));
			int iTemp10 = (1 - iTemp9);
			float fTemp11 = getRandomNumber2();
			fRec27[0] = ((fTemp11 * iTemp9) + (iTemp10 * fRec27[1]));
			fRec26[0] = ((fConst17 * fRec26[1]) + (fConst16 * (fRec27[1] + fRec27[0])));
			fRec25[0] = ((fConst17 * fRec25[1]) + (fConst16 * (fRec26[1] + fRec26[0])));
			fRec24[0] = ((fConst17 * fRec24[1]) + (fConst16 * (fRec25[1] + fRec25[0])));
			fRec23[0] = ((fConst17 * fRec23[1]) + (fConst16 * (fRec24[1] + fRec24[0])));
			fRec22[0] = ((fConst17 * fRec22[1]) + (fConst16 * (fRec23[1] + fRec23[0])));
			float fTemp12 = getRandomNumber1();
			fRec35[0] = ((fRec35[1] * iTemp10) + (fTemp12 * iTemp9));
			fRec34[0] = ((fConst17 * fRec34[1]) + (fConst16 * (fRec35[1] + fRec35[0])));
			fRec33[0] = ((fConst17 * fRec33[1]) + (fConst16 * (fRec34[1] + fRec34[0])));
			fRec32[0] = ((fConst17 * fRec32[1]) + (fConst16 * (fRec33[1] + fRec33[0])));
			fRec31[0] = ((fConst17 * fRec31[1]) + (fConst16 * (fRec32[1] + fRec32[0])));
			fRec30[0] = ((fConst17 * fRec30[1]) + (fConst16 * (fRec31[1] + fRec31[0])));
			fRec36[0] = (fSlow16 + (fSlow14 * fRec36[1]));
			fRec43[0] = ((fConst20 * fRec44[1]) + (fConst19 * fRec43[1]));
			fRec44[0] = (((fConst19 * fRec44[1]) + (fConst21 * fRec43[1])) + iTemp0);
			int iTemp13 = ((fRec43[1] <= 0) & (fRec43[0] > 0));
			fRec42[0] = ((fRec42[1] * (1 - iTemp13)) + (fTemp4 * iTemp13));
			fRec41[0] = ((fConst25 * fRec41[1]) + (fConst24 * (fRec42[1] + fRec42[0])));
			fRec40[0] = ((fConst25 * fRec40[1]) + (fConst24 * (fRec41[1] + fRec41[0])));
			fRec39[0] = ((fConst25 * fRec39[1]) + (fConst24 * (fRec40[1] + fRec40[0])));
			fRec38[0] = ((fConst25 * fRec38[1]) + (fConst24 * (fRec39[1] + fRec39[0])));
			fRec37[0] = ((fConst25 * fRec37[1]) + (fConst24 * (fRec38[1] + fRec38[0])));
			fRec51[0] = ((fConst28 * fRec52[1]) + (fConst27 * fRec51[1]));
			fRec52[0] = (((fConst27 * fRec52[1]) + (fConst29 * fRec51[1])) + iTemp0);
			int iTemp14 = ((fRec51[1] <= 0) & (fRec51[0] > 0));
			fRec50[0] = ((fRec50[1] * (1 - iTemp14)) + (fTemp11 * iTemp14));
			fRec49[0] = ((fConst33 * fRec49[1]) + (fConst32 * (fRec50[1] + fRec50[0])));
			fRec48[0] = ((fConst33 * fRec48[1]) + (fConst32 * (fRec49[1] + fRec49[0])));
			fRec47[0] = ((fConst33 * fRec47[1]) + (fConst32 * (fRec48[1] + fRec48[0])));
			fRec46[0] = ((fConst33 * fRec46[1]) + (fConst32 * (fRec47[1] + fRec47[0])));
			fRec45[0] = ((fConst33 * fRec45[1]) + (fConst32 * (fRec46[1] + fRec46[0])));
			fRec59[0] = ((fConst36 * fRec60[1]) + (fConst35 * fRec59[1]));
			fRec60[0] = (((fConst35 * fRec60[1]) + (fConst37 * fRec59[1])) + iTemp0);
			int iTemp15 = ((fRec59[1] <= 0) & (fRec59[0] > 0));
			fRec58[0] = ((fRec58[1] * (1 - iTemp15)) + (fTemp12 * iTemp15));
			fRec57[0] = ((fConst41 * fRec57[1]) + (fConst40 * (fRec58[1] + fRec58[0])));
			fRec56[0] = ((fConst41 * fRec56[1]) + (fConst40 * (fRec57[1] + fRec57[0])));
			fRec55[0] = ((fConst41 * fRec55[1]) + (fConst40 * (fRec56[1] + fRec56[0])));
			fRec54[0] = ((fConst41 * fRec54[1]) + (fConst40 * (fRec55[1] + fRec55[0])));
			fRec53[0] = ((fConst41 * fRec53[1]) + (fConst40 * (fRec54[1] + fRec54[0])));
			float fTemp16 = (fRec3[1] + (fConst42 * ((((0.0033333334f * ((fRec53[0] + fRec45[0]) + fRec37[0])) + 1.0f) * ((10 * getRandomNumber5()) + fRec36[0])) * ((fSlow13 * ((((0.005f * (fRec30[0] + fRec22[0])) + 1) * fRec21[0]) * fRec4[0])) + 1.0f))));
			fRec3[0] = (fTemp16 - floorf(fTemp16));
			iVec1[0] = ((fRec3[0] - fRec3[1]) < 0);
			iRec2[0] = ((iVec1[1] + (iRec2[1] * (1 - (iRec2[1] >= 2)))) + iTemp0);
			int iTemp17 = (iVec1[1] + iTemp0);
			int iTemp18 = (iTemp17 * (iRec2[0] == 2));
			iVec2[0] = iTemp18;
			int iTemp19 = (1 - (iVec2[1] > 0));
			int iTemp20 = ((4 * (fRec36[0] > 392.0f)) + 8);
			int iTemp21 = (4 * (fRec36[0] > 261.625f));
			float fTemp22 = (5 * ((fSlow19 * (iSlow17 + iTemp21)) + (fSlow18 * (iSlow17 + iTemp20))));
			float fTemp23 = (fTemp22 + 4);
			int iTemp24 = int((fTemp23 >= 1));
			int iTemp25 = int((fTemp23 >= 2));
			int iTemp26 = int((fTemp23 >= 4));
			int iTemp27 = int((fTemp23 >= 3));
			int iTemp28 = int((fTemp23 >= 6));
			int iTemp29 = int((fTemp23 >= 7));
			int iTemp30 = int((fTemp23 >= 8));
			int iTemp31 = int((fTemp23 >= 5));
			int iTemp32 = int((fTemp23 >= 11));
			int iTemp33 = int((fTemp23 >= 12));
			int iTemp34 = int((fTemp23 >= 13));
			int iTemp35 = int((fTemp23 >= 16));
			int iTemp36 = int((fTemp23 >= 17));
			int iTemp37 = int((fTemp23 >= 18));
			int iTemp38 = int((fTemp23 >= 15));
			int iTemp39 = int((fTemp23 >= 10));
			int iTemp40 = int((fTemp23 >= 21));
			int iTemp41 = int((fTemp23 >= 22));
			int iTemp42 = int((fTemp23 >= 24));
			int iTemp43 = int((fTemp23 >= 23));
			int iTemp44 = int((fTemp23 >= 26));
			int iTemp45 = int((fTemp23 >= 27));
			int iTemp46 = int((fTemp23 >= 28));
			int iTemp47 = int((fTemp23 >= 25));
			int iTemp48 = int((fTemp23 >= 31));
			int iTemp49 = int((fTemp23 >= 32));
			int iTemp50 = int((fTemp23 >= 34));
			int iTemp51 = int((fTemp23 >= 33));
			int iTemp52 = int((fTemp23 >= 36));
			int iTemp53 = int((fTemp23 >= 37));
			int iTemp54 = int((fTemp23 >= 38));
			int iTemp55 = int((fTemp23 >= 35));
			int iTemp56 = int((fTemp23 >= 30));
			int iTemp57 = int((fTemp23 >= 20));
			int iTemp58 = int((fTemp23 >= 41));
			int iTemp59 = int((fTemp23 >= 42));
			int iTemp60 = int((fTemp23 >= 44));
			int iTemp61 = int((fTemp23 >= 43));
			int iTemp62 = int((fTemp23 >= 46));
			int iTemp63 = int((fTemp23 >= 47));
			int iTemp64 = int((fTemp23 >= 49));
			int iTemp65 = int((fTemp23 >= 48));
			int iTemp66 = int((fTemp23 >= 45));
			int iTemp67 = int((fTemp23 >= 51));
			int iTemp68 = int((fTemp23 >= 52));
			int iTemp69 = int((fTemp23 >= 54));
			int iTemp70 = int((fTemp23 >= 53));
			int iTemp71 = int((fTemp23 >= 56));
			int iTemp72 = int((fTemp23 >= 57));
			int iTemp73 = int((fTemp23 >= 59));
			int iTemp74 = int((fTemp23 >= 58));
			int iTemp75 = int((fTemp23 >= 55));
			int iTemp76 = int((fTemp23 >= 50));
			int iTemp77 = int((fTemp23 >= 61));
			int iTemp78 = int((fTemp23 >= 62));
			int iTemp79 = int((fTemp23 >= 64));
			int iTemp80 = int((fTemp23 >= 63));
			int iTemp81 = int((fTemp23 >= 66));
			int iTemp82 = int((fTemp23 >= 67));
			int iTemp83 = int((fTemp23 >= 69));
			int iTemp84 = int((fTemp23 >= 68));
			int iTemp85 = int((fTemp23 >= 65));
			int iTemp86 = int((fTemp23 >= 71));
			int iTemp87 = int((fTemp23 >= 72));
			int iTemp88 = int((fTemp23 >= 73));
			int iTemp89 = int((fTemp23 >= 76));
			int iTemp90 = int((fTemp23 >= 77));
			int iTemp91 = int((fTemp23 >= 79));
			int iTemp92 = int((fTemp23 >= 78));
			int iTemp93 = int((fTemp23 >= 75));
			int iTemp94 = int((fTemp23 >= 70));
			int iTemp95 = int((fTemp23 >= 60));
			int iTemp96 = int((fTemp23 >= 40));
			fRec61[0] = ((fSlow14 * fRec61[1]) + (fSlow15 * ((iTemp96)?((iTemp95)?((iTemp94)?((iTemp93)?((iTemp92)?((iTemp91)?4950:3800):((iTemp90)?2700:((iTemp89)?700:325))):((iTemp88)?((int((fTemp23 >= 74)))?4950:3800):((iTemp87)?2830:((iTemp86)?800:450)))):((iTemp85)?((iTemp84)?((iTemp83)?4950:3600):((iTemp82)?2800:((iTemp81)?2000:350))):((iTemp80)?((iTemp79)?4950:3900):((iTemp78)?2900:((iTemp77)?1150:800))))):((iTemp76)?((iTemp75)?((iTemp74)?((iTemp73)?4950:3500):((iTemp72)?2530:((iTemp71)?700:325))):((iTemp70)?((iTemp69)?4950:3500):((iTemp68)?2830:((iTemp67)?800:450)))):((iTemp66)?((iTemp65)?((iTemp64)?4950:3300):((iTemp63)?2700:((iTemp62)?1600:400))):((iTemp61)?((iTemp60)?4950:3500):((iTemp59)?2800:((iTemp58)?1150:800)))))):((iTemp57)?((iTemp56)?((iTemp55)?((iTemp54)?((int((fTemp23 >= 39)))?3300:2900):((iTemp53)?2700:((iTemp52)?600:350))):((iTemp51)?((iTemp50)?3000:2800):((iTemp49)?2600:((iTemp48)?800:400)))):((iTemp47)?((iTemp46)?((int((fTemp23 >= 29)))?3580:3200):((iTemp45)?2600:((iTemp44)?1700:400))):((iTemp43)?((iTemp42)?3250:2900):((iTemp41)?2650:((iTemp40)?1080:650))))):((iTemp39)?((iTemp38)?((iTemp37)?((int((fTemp23 >= 19)))?2950:2675):((iTemp36)?2400:((iTemp35)?600:350))):((iTemp34)?((int((fTemp23 >= 14)))?2900:2600):((iTemp33)?2400:((iTemp32)?750:400)))):((iTemp31)?((iTemp30)?((int((fTemp23 >= 9)))?3100:2800):((iTemp29)?2400:((iTemp28)?1620:400))):((iTemp27)?((iTemp26)?2750:2450):((iTemp25)?2250:((iTemp24)?1040:600)))))))));
			float fTemp97 = (5 * ((fSlow19 * (iSlow22 + iTemp21)) + (fSlow18 * (iSlow22 + iTemp20))));
			float fTemp98 = (fTemp97 + 4);
			int iTemp99 = int((fTemp98 >= 1));
			int iTemp100 = int((fTemp98 >= 2));
			int iTemp101 = int((fTemp98 >= 4));
			int iTemp102 = int((fTemp98 >= 3));
			int iTemp103 = int((fTemp98 >= 6));
			int iTemp104 = int((fTemp98 >= 7));
			int iTemp105 = int((fTemp98 >= 9));
			int iTemp106 = int((fTemp98 >= 8));
			int iTemp107 = int((fTemp98 >= 5));
			int iTemp108 = int((fTemp98 >= 11));
			int iTemp109 = int((fTemp98 >= 12));
			int iTemp110 = int((fTemp98 >= 14));
			int iTemp111 = int((fTemp98 >= 13));
			int iTemp112 = int((fTemp98 >= 16));
			int iTemp113 = int((fTemp98 >= 17));
			int iTemp114 = int((fTemp98 >= 19));
			int iTemp115 = int((fTemp98 >= 18));
			int iTemp116 = int((fTemp98 >= 15));
			int iTemp117 = int((fTemp98 >= 10));
			int iTemp118 = int((fTemp98 >= 21));
			int iTemp119 = int((fTemp98 >= 22));
			int iTemp120 = int((fTemp98 >= 24));
			int iTemp121 = int((fTemp98 >= 23));
			int iTemp122 = int((fTemp98 >= 26));
			int iTemp123 = int((fTemp98 >= 27));
			int iTemp124 = int((fTemp98 >= 29));
			int iTemp125 = int((fTemp98 >= 28));
			int iTemp126 = int((fTemp98 >= 25));
			int iTemp127 = int((fTemp98 >= 31));
			int iTemp128 = int((fTemp98 >= 32));
			int iTemp129 = int((fTemp98 >= 34));
			int iTemp130 = int((fTemp98 >= 33));
			int iTemp131 = int((fTemp98 >= 36));
			int iTemp132 = int((fTemp98 >= 37));
			int iTemp133 = int((fTemp98 >= 39));
			int iTemp134 = int((fTemp98 >= 38));
			int iTemp135 = int((fTemp98 >= 35));
			int iTemp136 = int((fTemp98 >= 30));
			int iTemp137 = int((fTemp98 >= 20));
			int iTemp138 = int((fTemp98 >= 41));
			int iTemp139 = int((fTemp98 >= 42));
			int iTemp140 = int((fTemp98 >= 44));
			int iTemp141 = int((fTemp98 >= 43));
			int iTemp142 = int((fTemp98 >= 46));
			int iTemp143 = int((fTemp98 >= 47));
			int iTemp144 = int((fTemp98 >= 49));
			int iTemp145 = int((fTemp98 >= 48));
			int iTemp146 = int((fTemp98 >= 45));
			int iTemp147 = int((fTemp98 >= 51));
			int iTemp148 = int((fTemp98 >= 52));
			int iTemp149 = int((fTemp98 >= 54));
			int iTemp150 = int((fTemp98 >= 53));
			int iTemp151 = int((fTemp98 >= 56));
			int iTemp152 = int((fTemp98 >= 57));
			int iTemp153 = int((fTemp98 >= 59));
			int iTemp154 = int((fTemp98 >= 58));
			int iTemp155 = int((fTemp98 >= 55));
			int iTemp156 = int((fTemp98 >= 50));
			int iTemp157 = int((fTemp98 >= 61));
			int iTemp158 = int((fTemp98 >= 62));
			int iTemp159 = int((fTemp98 >= 64));
			int iTemp160 = int((fTemp98 >= 63));
			int iTemp161 = int((fTemp98 >= 66));
			int iTemp162 = int((fTemp98 >= 67));
			int iTemp163 = int((fTemp98 >= 69));
			int iTemp164 = int((fTemp98 >= 68));
			int iTemp165 = int((fTemp98 >= 65));
			int iTemp166 = int((fTemp98 >= 71));
			int iTemp167 = int((fTemp98 >= 72));
			int iTemp168 = int((fTemp98 >= 74));
			int iTemp169 = int((fTemp98 >= 73));
			int iTemp170 = int((fTemp98 >= 76));
			int iTemp171 = int((fTemp98 >= 77));
			int iTemp172 = int((fTemp98 >= 79));
			int iTemp173 = int((fTemp98 >= 78));
			int iTemp174 = int((fTemp98 >= 75));
			int iTemp175 = int((fTemp98 >= 70));
			int iTemp176 = int((fTemp98 >= 60));
			int iTemp177 = int((fTemp98 >= 40));
			fRec62[0] = ((fSlow14 * fRec62[1]) + (fSlow15 * ((iTemp177)?((iTemp176)?((iTemp175)?((iTemp174)?((iTemp173)?((iTemp172)?4950:3800):((iTemp171)?2700:((iTemp170)?700:325))):((iTemp169)?((iTemp168)?4950:3800):((iTemp167)?2830:((iTemp166)?800:450)))):((iTemp165)?((iTemp164)?((iTemp163)?4950:3600):((iTemp162)?2800:((iTemp161)?2000:350))):((iTemp160)?((iTemp159)?4950:3900):((iTemp158)?2900:((iTemp157)?1150:800))))):((iTemp156)?((iTemp155)?((iTemp154)?((iTemp153)?4950:3500):((iTemp152)?2530:((iTemp151)?700:325))):((iTemp150)?((iTemp149)?4950:3500):((iTemp148)?2830:((iTemp147)?800:450)))):((iTemp146)?((iTemp145)?((iTemp144)?4950:3300):((iTemp143)?2700:((iTemp142)?1600:400))):((iTemp141)?((iTemp140)?4950:3500):((iTemp139)?2800:((iTemp138)?1150:800)))))):((iTemp137)?((iTemp136)?((iTemp135)?((iTemp134)?((iTemp133)?3300:2900):((iTemp132)?2700:((iTemp131)?600:350))):((iTemp130)?((iTemp129)?3000:2800):((iTemp128)?2600:((iTemp127)?800:400)))):((iTemp126)?((iTemp125)?((iTemp124)?3580:3200):((iTemp123)?2600:((iTemp122)?1700:400))):((iTemp121)?((iTemp120)?3250:2900):((iTemp119)?2650:((iTemp118)?1080:650))))):((iTemp117)?((iTemp116)?((iTemp115)?((iTemp114)?2950:2675):((iTemp113)?2400:((iTemp112)?600:350))):((iTemp111)?((iTemp110)?2900:2600):((iTemp109)?2400:((iTemp108)?750:400)))):((iTemp107)?((iTemp106)?((iTemp105)?3100:2800):((iTemp104)?2400:((iTemp103)?1620:400))):((iTemp102)?((iTemp101)?2750:2450):((iTemp100)?2250:((iTemp99)?1040:600)))))))));
			float fTemp178 = (5 * ((fSlow19 * (iSlow26 + iTemp21)) + (fSlow18 * (iSlow26 + iTemp20))));
			float fTemp179 = (fTemp178 + 4);
			int iTemp180 = int((fTemp179 >= 1));
			int iTemp181 = int((fTemp179 >= 2));
			int iTemp182 = int((fTemp179 >= 4));
			int iTemp183 = int((fTemp179 >= 3));
			int iTemp184 = int((fTemp179 >= 6));
			int iTemp185 = int((fTemp179 >= 7));
			int iTemp186 = int((fTemp179 >= 8));
			int iTemp187 = int((fTemp179 >= 5));
			int iTemp188 = int((fTemp179 >= 11));
			int iTemp189 = int((fTemp179 >= 12));
			int iTemp190 = int((fTemp179 >= 13));
			int iTemp191 = int((fTemp179 >= 16));
			int iTemp192 = int((fTemp179 >= 17));
			int iTemp193 = int((fTemp179 >= 18));
			int iTemp194 = int((fTemp179 >= 15));
			int iTemp195 = int((fTemp179 >= 10));
			int iTemp196 = int((fTemp179 >= 21));
			int iTemp197 = int((fTemp179 >= 22));
			int iTemp198 = int((fTemp179 >= 24));
			int iTemp199 = int((fTemp179 >= 23));
			int iTemp200 = int((fTemp179 >= 26));
			int iTemp201 = int((fTemp179 >= 27));
			int iTemp202 = int((fTemp179 >= 28));
			int iTemp203 = int((fTemp179 >= 25));
			int iTemp204 = int((fTemp179 >= 31));
			int iTemp205 = int((fTemp179 >= 32));
			int iTemp206 = int((fTemp179 >= 34));
			int iTemp207 = int((fTemp179 >= 33));
			int iTemp208 = int((fTemp179 >= 36));
			int iTemp209 = int((fTemp179 >= 37));
			int iTemp210 = int((fTemp179 >= 38));
			int iTemp211 = int((fTemp179 >= 35));
			int iTemp212 = int((fTemp179 >= 30));
			int iTemp213 = int((fTemp179 >= 20));
			int iTemp214 = int((fTemp179 >= 41));
			int iTemp215 = int((fTemp179 >= 42));
			int iTemp216 = int((fTemp179 >= 44));
			int iTemp217 = int((fTemp179 >= 43));
			int iTemp218 = int((fTemp179 >= 46));
			int iTemp219 = int((fTemp179 >= 47));
			int iTemp220 = int((fTemp179 >= 49));
			int iTemp221 = int((fTemp179 >= 48));
			int iTemp222 = int((fTemp179 >= 45));
			int iTemp223 = int((fTemp179 >= 51));
			int iTemp224 = int((fTemp179 >= 52));
			int iTemp225 = int((fTemp179 >= 54));
			int iTemp226 = int((fTemp179 >= 53));
			int iTemp227 = int((fTemp179 >= 56));
			int iTemp228 = int((fTemp179 >= 57));
			int iTemp229 = int((fTemp179 >= 59));
			int iTemp230 = int((fTemp179 >= 58));
			int iTemp231 = int((fTemp179 >= 55));
			int iTemp232 = int((fTemp179 >= 50));
			int iTemp233 = int((fTemp179 >= 61));
			int iTemp234 = int((fTemp179 >= 62));
			int iTemp235 = int((fTemp179 >= 64));
			int iTemp236 = int((fTemp179 >= 63));
			int iTemp237 = int((fTemp179 >= 66));
			int iTemp238 = int((fTemp179 >= 67));
			int iTemp239 = int((fTemp179 >= 69));
			int iTemp240 = int((fTemp179 >= 68));
			int iTemp241 = int((fTemp179 >= 65));
			int iTemp242 = int((fTemp179 >= 71));
			int iTemp243 = int((fTemp179 >= 72));
			int iTemp244 = int((fTemp179 >= 73));
			int iTemp245 = int((fTemp179 >= 76));
			int iTemp246 = int((fTemp179 >= 77));
			int iTemp247 = int((fTemp179 >= 79));
			int iTemp248 = int((fTemp179 >= 78));
			int iTemp249 = int((fTemp179 >= 75));
			int iTemp250 = int((fTemp179 >= 70));
			int iTemp251 = int((fTemp179 >= 60));
			int iTemp252 = int((fTemp179 >= 40));
			fRec63[0] = ((fSlow14 * fRec63[1]) + (fSlow15 * ((iTemp252)?((iTemp251)?((iTemp250)?((iTemp249)?((iTemp248)?((iTemp247)?4950:3800):((iTemp246)?2700:((iTemp245)?700:325))):((iTemp244)?((int((fTemp179 >= 74)))?4950:3800):((iTemp243)?2830:((iTemp242)?800:450)))):((iTemp241)?((iTemp240)?((iTemp239)?4950:3600):((iTemp238)?2800:((iTemp237)?2000:350))):((iTemp236)?((iTemp235)?4950:3900):((iTemp234)?2900:((iTemp233)?1150:800))))):((iTemp232)?((iTemp231)?((iTemp230)?((iTemp229)?4950:3500):((iTemp228)?2530:((iTemp227)?700:325))):((iTemp226)?((iTemp225)?4950:3500):((iTemp224)?2830:((iTemp223)?800:450)))):((iTemp222)?((iTemp221)?((iTemp220)?4950:3300):((iTemp219)?2700:((iTemp218)?1600:400))):((iTemp217)?((iTemp216)?4950:3500):((iTemp215)?2800:((iTemp214)?1150:800)))))):((iTemp213)?((iTemp212)?((iTemp211)?((iTemp210)?((int((fTemp179 >= 39)))?3300:2900):((iTemp209)?2700:((iTemp208)?600:350))):((iTemp207)?((iTemp206)?3000:2800):((iTemp205)?2600:((iTemp204)?800:400)))):((iTemp203)?((iTemp202)?((int((fTemp179 >= 29)))?3580:3200):((iTemp201)?2600:((iTemp200)?1700:400))):((iTemp199)?((iTemp198)?3250:2900):((iTemp197)?2650:((iTemp196)?1080:650))))):((iTemp195)?((iTemp194)?((iTemp193)?((int((fTemp179 >= 19)))?2950:2675):((iTemp192)?2400:((iTemp191)?600:350))):((iTemp190)?((int((fTemp179 >= 14)))?2900:2600):((iTemp189)?2400:((iTemp188)?750:400)))):((iTemp187)?((iTemp186)?((int((fTemp179 >= 9)))?3100:2800):((iTemp185)?2400:((iTemp184)?1620:400))):((iTemp183)?((iTemp182)?2750:2450):((iTemp181)?2250:((iTemp180)?1040:600)))))))));
			float fTemp253 = (50 * fTemp4);
			float fTemp254 = (fConst43 * (fTemp253 + ((iSlow27 * ((fSlow20 * fRec63[0]) + (fSlow25 * fRec61[0]))) + (iSlow24 * ((fSlow23 * fRec62[0]) + (fSlow21 * fRec61[0]))))));
			float fTemp255 = (fTemp254 + (iTemp19 * fRec1[1]));
			fRec1[0] = (fTemp255 - floorf(fTemp255));
			int iTemp256 = ((iVec2[1] <= 0) & (iVec2[0] > 0));
			int iTemp257 = (1 - iTemp256);
			fRec65[0] = ((fSlow14 * fRec65[1]) + (fSlow15 * ((iTemp96)?((iTemp95)?((iTemp94)?((iTemp93)?((iTemp92)?((iTemp91)?200:180):((iTemp90)?170:((iTemp89)?60:50))):((iTemp88)?120:((iTemp87)?100:((iTemp86)?80:40)))):((iTemp85)?((iTemp84)?((iTemp83)?200:150):((iTemp82)?120:((iTemp81)?100:60))):((iTemp80)?((iTemp79)?140:130):((iTemp78)?120:((iTemp77)?90:80))))):((iTemp76)?((iTemp75)?((iTemp74)?((iTemp73)?200:180):((iTemp72)?170:((iTemp71)?60:50))):((iTemp70)?((iTemp69)?135:130):((iTemp68)?100:((iTemp67)?80:70)))):((iTemp66)?((iTemp65)?((iTemp64)?200:150):((iTemp63)?120:((iTemp62)?80:60))):((iTemp61)?((iTemp60)?140:130):((iTemp59)?120:((iTemp58)?90:80)))))):((iTemp57)?((iTemp56)?((iTemp55)?((iTemp54)?120:((iTemp53)?100:((iTemp52)?60:40))):((iTemp51)?((iTemp50)?135:130):((iTemp49)?100:((iTemp48)?80:70)))):((iTemp47)?((iTemp46)?120:((iTemp45)?100:((iTemp44)?80:70))):((iTemp43)?((iTemp42)?140:130):((iTemp41)?120:((iTemp40)?90:80))))):((iTemp39)?((iTemp38)?((iTemp37)?120:((iTemp36)?100:((iTemp35)?80:40))):((iTemp34)?120:((iTemp33)?100:((iTemp32)?80:40)))):((iTemp31)?((iTemp30)?120:((iTemp29)?100:((iTemp28)?80:40))):((iTemp27)?((iTemp26)?130:120):((iTemp25)?110:((iTemp24)?70:60)))))))));
			fRec66[0] = ((fSlow14 * fRec66[1]) + (fSlow15 * ((iTemp177)?((iTemp176)?((iTemp175)?((iTemp174)?((iTemp173)?((iTemp172)?200:180):((iTemp171)?170:((iTemp170)?60:50))):((iTemp169)?120:((iTemp167)?100:((iTemp166)?80:40)))):((iTemp165)?((iTemp164)?((iTemp163)?200:150):((iTemp162)?120:((iTemp161)?100:60))):((iTemp160)?((iTemp159)?140:130):((iTemp158)?120:((iTemp157)?90:80))))):((iTemp156)?((iTemp155)?((iTemp154)?((iTemp153)?200:180):((iTemp152)?170:((iTemp151)?60:50))):((iTemp150)?((iTemp149)?135:130):((iTemp148)?100:((iTemp147)?80:70)))):((iTemp146)?((iTemp145)?((iTemp144)?200:150):((iTemp143)?120:((iTemp142)?80:60))):((iTemp141)?((iTemp140)?140:130):((iTemp139)?120:((iTemp138)?90:80)))))):((iTemp137)?((iTemp136)?((iTemp135)?((iTemp134)?120:((iTemp132)?100:((iTemp131)?60:40))):((iTemp130)?((iTemp129)?135:130):((iTemp128)?100:((iTemp127)?80:70)))):((iTemp126)?((iTemp125)?120:((iTemp123)?100:((iTemp122)?80:70))):((iTemp121)?((iTemp120)?140:130):((iTemp119)?120:((iTemp118)?90:80))))):((iTemp117)?((iTemp116)?((iTemp115)?120:((iTemp113)?100:((iTemp112)?80:40))):((iTemp111)?120:((iTemp109)?100:((iTemp108)?80:40)))):((iTemp107)?((iTemp106)?120:((iTemp104)?100:((iTemp103)?80:40))):((iTemp102)?((iTemp101)?130:120):((iTemp100)?110:((iTemp99)?70:60)))))))));
			fRec67[0] = ((fSlow14 * fRec67[1]) + (fSlow15 * ((iTemp252)?((iTemp251)?((iTemp250)?((iTemp249)?((iTemp248)?((iTemp247)?200:180):((iTemp246)?170:((iTemp245)?60:50))):((iTemp244)?120:((iTemp243)?100:((iTemp242)?80:40)))):((iTemp241)?((iTemp240)?((iTemp239)?200:150):((iTemp238)?120:((iTemp237)?100:60))):((iTemp236)?((iTemp235)?140:130):((iTemp234)?120:((iTemp233)?90:80))))):((iTemp232)?((iTemp231)?((iTemp230)?((iTemp229)?200:180):((iTemp228)?170:((iTemp227)?60:50))):((iTemp226)?((iTemp225)?135:130):((iTemp224)?100:((iTemp223)?80:70)))):((iTemp222)?((iTemp221)?((iTemp220)?200:150):((iTemp219)?120:((iTemp218)?80:60))):((iTemp217)?((iTemp216)?140:130):((iTemp215)?120:((iTemp214)?90:80)))))):((iTemp213)?((iTemp212)?((iTemp211)?((iTemp210)?120:((iTemp209)?100:((iTemp208)?60:40))):((iTemp207)?((iTemp206)?135:130):((iTemp205)?100:((iTemp204)?80:70)))):((iTemp203)?((iTemp202)?120:((iTemp201)?100:((iTemp200)?80:70))):((iTemp199)?((iTemp198)?140:130):((iTemp197)?120:((iTemp196)?90:80))))):((iTemp195)?((iTemp194)?((iTemp193)?120:((iTemp192)?100:((iTemp191)?80:40))):((iTemp190)?120:((iTemp189)?100:((iTemp188)?80:40)))):((iTemp187)?((iTemp186)?120:((iTemp185)?100:((iTemp184)?80:40))):((iTemp183)?((iTemp182)?130:120):((iTemp181)?110:((iTemp180)?70:60)))))))));
			float fTemp258 = (5 * fTemp11);
			float fTemp259 = (fTemp258 + ((iSlow27 * ((fSlow20 * fRec67[0]) + (fSlow25 * fRec65[0]))) + (iSlow24 * ((fSlow23 * fRec66[0]) + (fSlow21 * fRec65[0])))));
			float fTemp260 = (iTemp256 * fTemp259);
			fRec64[0] = (fTemp260 + (iTemp257 * fRec64[1]));
			float fTemp261 = expf((fConst5 * (0 - fRec64[0])));
			float fTemp262 = ((int((fRec36[0] <= 174.61f)))?0.0f:((int((fRec36[0] >= 1046.5f)))?1.0f:(0.0011469336f * (fRec36[0] + -174.61f))));
			float fTemp263 = ((int((fRec36[0] <= 82.41f)))?0.0f:((int((fRec36[0] >= 523.25f)))?1.0f:(0.0022683968f * (fRec36[0] + -82.41f))));
			float fTemp264 = ((iSlow35)?(fSlow33 + (fSlow34 * fTemp263)):(fSlow30 + (fSlow31 * fTemp262)));
			float fTemp265 = ((iSlow27 * ((fSlow20 * ((iSlow35)?(fSlow48 + (fSlow49 * fTemp263)):(fSlow45 + (fSlow46 * fTemp262)))) + (fSlow25 * fTemp264))) + (iSlow24 * ((fSlow23 * ((iSlow35)?(fSlow41 + (fSlow42 * fTemp263)):(fSlow38 + (fSlow39 * fTemp262)))) + (fSlow21 * fTemp264))));
			fRec68[0] = ((iTemp257 * fRec68[1]) + (fTemp260 * fTemp265));
			float fTemp266 = expf((fConst5 * (0 - fRec68[0])));
			fRec69[0] = (iVec2[2] - ((fRec69[1] * (0 - (fTemp266 + fTemp261))) + ((fRec69[2] * fTemp266) * fTemp261)));
			int iTemp267 = (iTemp17 * (iRec2[0] == 1));
			iVec3[0] = iTemp267;
			int iTemp268 = (1 - (iVec3[1] > 0));
			float fTemp269 = (fTemp254 + (iTemp268 * fRec70[1]));
			fRec70[0] = (fTemp269 - floorf(fTemp269));
			int iTemp270 = ((iVec3[1] <= 0) & (iVec3[0] > 0));
			int iTemp271 = (1 - iTemp270);
			float fTemp272 = (iTemp270 * fTemp259);
			fRec71[0] = (fTemp272 + (iTemp271 * fRec71[1]));
			float fTemp273 = expf((fConst5 * (0 - fRec71[0])));
			fRec72[0] = ((iTemp271 * fRec72[1]) + (fTemp272 * fTemp265));
			float fTemp274 = expf((fConst5 * (0 - fRec72[0])));
			fRec73[0] = (iVec3[2] - ((fRec73[1] * (0 - (fTemp274 + fTemp273))) + ((fRec73[2] * fTemp274) * fTemp273)));
			fRec74[0] = ((fSlow14 * fRec74[1]) + (fSlow15 * powf(10,(0.05f * ((iTemp177)?((iTemp176)?((iTemp175)?((iTemp174)?((iTemp173)?((iTemp172)?-60:-40):((iTemp171)?-35:((iTemp170)?-16:0))):((iTemp169)?((iTemp168)?-50:-22):((iTemp167)?-22:((iTemp166)?-11:0)))):((iTemp165)?((iTemp164)?((iTemp163)?-56:-40):((iTemp162)?-15:((iTemp161)?-20:0))):((iTemp160)?((iTemp159)?-50:-20):((iTemp158)?-32:((iTemp157)?-6:0))))):((iTemp156)?((iTemp155)?((iTemp154)?((iTemp153)?-64:-40):((iTemp152)?-30:((iTemp151)?-12:0))):((iTemp150)?((iTemp149)?-55:-28):((iTemp148)?-16:((iTemp147)?-9:0)))):((iTemp146)?((iTemp145)?((iTemp144)?-60:-35):((iTemp143)?-30:((iTemp142)?-24:0))):((iTemp141)?((iTemp140)?-60:-36):((iTemp139)?-20:((iTemp138)?-4:0)))))):((iTemp137)?((iTemp136)?((iTemp135)?((iTemp134)?((iTemp133)?-26:-14):((iTemp132)?-17:((iTemp131)?-20:0))):((iTemp130)?((iTemp129)?-26:-12):((iTemp128)?-12:((iTemp127)?-10:0)))):((iTemp126)?((iTemp125)?((iTemp124)?-20:-14):((iTemp123)?-12:((iTemp122)?-14:0))):((iTemp121)?((iTemp120)?-22:-8):((iTemp119)?-7:((iTemp118)?-6:0))))):((iTemp117)?((iTemp116)?((iTemp115)?((iTemp114)?-36:-28):((iTemp113)?-32:((iTemp112)?-20:0))):((iTemp111)?((iTemp110)?-40:-20):((iTemp109)?-21:((iTemp108)?-11:0)))):((iTemp107)?((iTemp106)?((iTemp105)?-18:-12):((iTemp104)?-9:((iTemp103)?-12:0))):((iTemp102)?((iTemp101)?-20:-9):((iTemp100)?-9:((iTemp99)?-7:0)))))))))));
			float fTemp275 = (fTemp22 + 3);
			int iTemp276 = int((fTemp275 >= 1));
			int iTemp277 = int((fTemp275 >= 2));
			int iTemp278 = int((fTemp275 >= 4));
			int iTemp279 = int((fTemp275 >= 3));
			int iTemp280 = int((fTemp275 >= 6));
			int iTemp281 = int((fTemp275 >= 7));
			int iTemp282 = int((fTemp275 >= 8));
			int iTemp283 = int((fTemp275 >= 5));
			int iTemp284 = int((fTemp275 >= 11));
			int iTemp285 = int((fTemp275 >= 12));
			int iTemp286 = int((fTemp275 >= 13));
			int iTemp287 = int((fTemp275 >= 16));
			int iTemp288 = int((fTemp275 >= 17));
			int iTemp289 = int((fTemp275 >= 18));
			int iTemp290 = int((fTemp275 >= 15));
			int iTemp291 = int((fTemp275 >= 10));
			int iTemp292 = int((fTemp275 >= 21));
			int iTemp293 = int((fTemp275 >= 22));
			int iTemp294 = int((fTemp275 >= 24));
			int iTemp295 = int((fTemp275 >= 23));
			int iTemp296 = int((fTemp275 >= 26));
			int iTemp297 = int((fTemp275 >= 27));
			int iTemp298 = int((fTemp275 >= 28));
			int iTemp299 = int((fTemp275 >= 25));
			int iTemp300 = int((fTemp275 >= 31));
			int iTemp301 = int((fTemp275 >= 32));
			int iTemp302 = int((fTemp275 >= 34));
			int iTemp303 = int((fTemp275 >= 33));
			int iTemp304 = int((fTemp275 >= 36));
			int iTemp305 = int((fTemp275 >= 37));
			int iTemp306 = int((fTemp275 >= 38));
			int iTemp307 = int((fTemp275 >= 35));
			int iTemp308 = int((fTemp275 >= 30));
			int iTemp309 = int((fTemp275 >= 20));
			int iTemp310 = int((fTemp275 >= 41));
			int iTemp311 = int((fTemp275 >= 42));
			int iTemp312 = int((fTemp275 >= 44));
			int iTemp313 = int((fTemp275 >= 43));
			int iTemp314 = int((fTemp275 >= 46));
			int iTemp315 = int((fTemp275 >= 47));
			int iTemp316 = int((fTemp275 >= 49));
			int iTemp317 = int((fTemp275 >= 48));
			int iTemp318 = int((fTemp275 >= 45));
			int iTemp319 = int((fTemp275 >= 51));
			int iTemp320 = int((fTemp275 >= 52));
			int iTemp321 = int((fTemp275 >= 54));
			int iTemp322 = int((fTemp275 >= 53));
			int iTemp323 = int((fTemp275 >= 56));
			int iTemp324 = int((fTemp275 >= 57));
			int iTemp325 = int((fTemp275 >= 59));
			int iTemp326 = int((fTemp275 >= 58));
			int iTemp327 = int((fTemp275 >= 55));
			int iTemp328 = int((fTemp275 >= 50));
			int iTemp329 = int((fTemp275 >= 61));
			int iTemp330 = int((fTemp275 >= 62));
			int iTemp331 = int((fTemp275 >= 64));
			int iTemp332 = int((fTemp275 >= 63));
			int iTemp333 = int((fTemp275 >= 66));
			int iTemp334 = int((fTemp275 >= 67));
			int iTemp335 = int((fTemp275 >= 69));
			int iTemp336 = int((fTemp275 >= 68));
			int iTemp337 = int((fTemp275 >= 65));
			int iTemp338 = int((fTemp275 >= 71));
			int iTemp339 = int((fTemp275 >= 72));
			int iTemp340 = int((fTemp275 >= 73));
			int iTemp341 = int((fTemp275 >= 76));
			int iTemp342 = int((fTemp275 >= 77));
			int iTemp343 = int((fTemp275 >= 79));
			int iTemp344 = int((fTemp275 >= 78));
			int iTemp345 = int((fTemp275 >= 75));
			int iTemp346 = int((fTemp275 >= 70));
			int iTemp347 = int((fTemp275 >= 60));
			int iTemp348 = int((fTemp275 >= 40));
			fRec76[0] = ((fSlow14 * fRec76[1]) + (fSlow15 * ((iTemp348)?((iTemp347)?((iTemp346)?((iTemp345)?((iTemp344)?((iTemp343)?4950:3800):((iTemp342)?2700:((iTemp341)?700:325))):((iTemp340)?((int((fTemp275 >= 74)))?4950:3800):((iTemp339)?2830:((iTemp338)?800:450)))):((iTemp337)?((iTemp336)?((iTemp335)?4950:3600):((iTemp334)?2800:((iTemp333)?2000:350))):((iTemp332)?((iTemp331)?4950:3900):((iTemp330)?2900:((iTemp329)?1150:800))))):((iTemp328)?((iTemp327)?((iTemp326)?((iTemp325)?4950:3500):((iTemp324)?2530:((iTemp323)?700:325))):((iTemp322)?((iTemp321)?4950:3500):((iTemp320)?2830:((iTemp319)?800:450)))):((iTemp318)?((iTemp317)?((iTemp316)?4950:3300):((iTemp315)?2700:((iTemp314)?1600:400))):((iTemp313)?((iTemp312)?4950:3500):((iTemp311)?2800:((iTemp310)?1150:800)))))):((iTemp309)?((iTemp308)?((iTemp307)?((iTemp306)?((int((fTemp275 >= 39)))?3300:2900):((iTemp305)?2700:((iTemp304)?600:350))):((iTemp303)?((iTemp302)?3000:2800):((iTemp301)?2600:((iTemp300)?800:400)))):((iTemp299)?((iTemp298)?((int((fTemp275 >= 29)))?3580:3200):((iTemp297)?2600:((iTemp296)?1700:400))):((iTemp295)?((iTemp294)?3250:2900):((iTemp293)?2650:((iTemp292)?1080:650))))):((iTemp291)?((iTemp290)?((iTemp289)?((int((fTemp275 >= 19)))?2950:2675):((iTemp288)?2400:((iTemp287)?600:350))):((iTemp286)?((int((fTemp275 >= 14)))?2900:2600):((iTemp285)?2400:((iTemp284)?750:400)))):((iTemp283)?((iTemp282)?((int((fTemp275 >= 9)))?3100:2800):((iTemp281)?2400:((iTemp280)?1620:400))):((iTemp279)?((iTemp278)?2750:2450):((iTemp277)?2250:((iTemp276)?1040:600)))))))));
			float fTemp349 = (fTemp97 + 3);
			int iTemp350 = int((fTemp349 >= 1));
			int iTemp351 = int((fTemp349 >= 2));
			int iTemp352 = int((fTemp349 >= 4));
			int iTemp353 = int((fTemp349 >= 3));
			int iTemp354 = int((fTemp349 >= 6));
			int iTemp355 = int((fTemp349 >= 7));
			int iTemp356 = int((fTemp349 >= 9));
			int iTemp357 = int((fTemp349 >= 8));
			int iTemp358 = int((fTemp349 >= 5));
			int iTemp359 = int((fTemp349 >= 11));
			int iTemp360 = int((fTemp349 >= 12));
			int iTemp361 = int((fTemp349 >= 14));
			int iTemp362 = int((fTemp349 >= 13));
			int iTemp363 = int((fTemp349 >= 16));
			int iTemp364 = int((fTemp349 >= 17));
			int iTemp365 = int((fTemp349 >= 19));
			int iTemp366 = int((fTemp349 >= 18));
			int iTemp367 = int((fTemp349 >= 15));
			int iTemp368 = int((fTemp349 >= 10));
			int iTemp369 = int((fTemp349 >= 21));
			int iTemp370 = int((fTemp349 >= 22));
			int iTemp371 = int((fTemp349 >= 24));
			int iTemp372 = int((fTemp349 >= 23));
			int iTemp373 = int((fTemp349 >= 26));
			int iTemp374 = int((fTemp349 >= 27));
			int iTemp375 = int((fTemp349 >= 29));
			int iTemp376 = int((fTemp349 >= 28));
			int iTemp377 = int((fTemp349 >= 25));
			int iTemp378 = int((fTemp349 >= 31));
			int iTemp379 = int((fTemp349 >= 32));
			int iTemp380 = int((fTemp349 >= 34));
			int iTemp381 = int((fTemp349 >= 33));
			int iTemp382 = int((fTemp349 >= 36));
			int iTemp383 = int((fTemp349 >= 37));
			int iTemp384 = int((fTemp349 >= 39));
			int iTemp385 = int((fTemp349 >= 38));
			int iTemp386 = int((fTemp349 >= 35));
			int iTemp387 = int((fTemp349 >= 30));
			int iTemp388 = int((fTemp349 >= 20));
			int iTemp389 = int((fTemp349 >= 41));
			int iTemp390 = int((fTemp349 >= 42));
			int iTemp391 = int((fTemp349 >= 44));
			int iTemp392 = int((fTemp349 >= 43));
			int iTemp393 = int((fTemp349 >= 46));
			int iTemp394 = int((fTemp349 >= 47));
			int iTemp395 = int((fTemp349 >= 49));
			int iTemp396 = int((fTemp349 >= 48));
			int iTemp397 = int((fTemp349 >= 45));
			int iTemp398 = int((fTemp349 >= 51));
			int iTemp399 = int((fTemp349 >= 52));
			int iTemp400 = int((fTemp349 >= 54));
			int iTemp401 = int((fTemp349 >= 53));
			int iTemp402 = int((fTemp349 >= 56));
			int iTemp403 = int((fTemp349 >= 57));
			int iTemp404 = int((fTemp349 >= 59));
			int iTemp405 = int((fTemp349 >= 58));
			int iTemp406 = int((fTemp349 >= 55));
			int iTemp407 = int((fTemp349 >= 50));
			int iTemp408 = int((fTemp349 >= 61));
			int iTemp409 = int((fTemp349 >= 62));
			int iTemp410 = int((fTemp349 >= 64));
			int iTemp411 = int((fTemp349 >= 63));
			int iTemp412 = int((fTemp349 >= 66));
			int iTemp413 = int((fTemp349 >= 67));
			int iTemp414 = int((fTemp349 >= 69));
			int iTemp415 = int((fTemp349 >= 68));
			int iTemp416 = int((fTemp349 >= 65));
			int iTemp417 = int((fTemp349 >= 71));
			int iTemp418 = int((fTemp349 >= 72));
			int iTemp419 = int((fTemp349 >= 74));
			int iTemp420 = int((fTemp349 >= 73));
			int iTemp421 = int((fTemp349 >= 76));
			int iTemp422 = int((fTemp349 >= 77));
			int iTemp423 = int((fTemp349 >= 79));
			int iTemp424 = int((fTemp349 >= 78));
			int iTemp425 = int((fTemp349 >= 75));
			int iTemp426 = int((fTemp349 >= 70));
			int iTemp427 = int((fTemp349 >= 60));
			int iTemp428 = int((fTemp349 >= 40));
			fRec77[0] = ((fSlow14 * fRec77[1]) + (fSlow15 * ((iTemp428)?((iTemp427)?((iTemp426)?((iTemp425)?((iTemp424)?((iTemp423)?4950:3800):((iTemp422)?2700:((iTemp421)?700:325))):((iTemp420)?((iTemp419)?4950:3800):((iTemp418)?2830:((iTemp417)?800:450)))):((iTemp416)?((iTemp415)?((iTemp414)?4950:3600):((iTemp413)?2800:((iTemp412)?2000:350))):((iTemp411)?((iTemp410)?4950:3900):((iTemp409)?2900:((iTemp408)?1150:800))))):((iTemp407)?((iTemp406)?((iTemp405)?((iTemp404)?4950:3500):((iTemp403)?2530:((iTemp402)?700:325))):((iTemp401)?((iTemp400)?4950:3500):((iTemp399)?2830:((iTemp398)?800:450)))):((iTemp397)?((iTemp396)?((iTemp395)?4950:3300):((iTemp394)?2700:((iTemp393)?1600:400))):((iTemp392)?((iTemp391)?4950:3500):((iTemp390)?2800:((iTemp389)?1150:800)))))):((iTemp388)?((iTemp387)?((iTemp386)?((iTemp385)?((iTemp384)?3300:2900):((iTemp383)?2700:((iTemp382)?600:350))):((iTemp381)?((iTemp380)?3000:2800):((iTemp379)?2600:((iTemp378)?800:400)))):((iTemp377)?((iTemp376)?((iTemp375)?3580:3200):((iTemp374)?2600:((iTemp373)?1700:400))):((iTemp372)?((iTemp371)?3250:2900):((iTemp370)?2650:((iTemp369)?1080:650))))):((iTemp368)?((iTemp367)?((iTemp366)?((iTemp365)?2950:2675):((iTemp364)?2400:((iTemp363)?600:350))):((iTemp362)?((iTemp361)?2900:2600):((iTemp360)?2400:((iTemp359)?750:400)))):((iTemp358)?((iTemp357)?((iTemp356)?3100:2800):((iTemp355)?2400:((iTemp354)?1620:400))):((iTemp353)?((iTemp352)?2750:2450):((iTemp351)?2250:((iTemp350)?1040:600)))))))));
			float fTemp429 = (fTemp178 + 3);
			int iTemp430 = int((fTemp429 >= 1));
			int iTemp431 = int((fTemp429 >= 2));
			int iTemp432 = int((fTemp429 >= 4));
			int iTemp433 = int((fTemp429 >= 3));
			int iTemp434 = int((fTemp429 >= 6));
			int iTemp435 = int((fTemp429 >= 7));
			int iTemp436 = int((fTemp429 >= 8));
			int iTemp437 = int((fTemp429 >= 5));
			int iTemp438 = int((fTemp429 >= 11));
			int iTemp439 = int((fTemp429 >= 12));
			int iTemp440 = int((fTemp429 >= 13));
			int iTemp441 = int((fTemp429 >= 16));
			int iTemp442 = int((fTemp429 >= 17));
			int iTemp443 = int((fTemp429 >= 18));
			int iTemp444 = int((fTemp429 >= 15));
			int iTemp445 = int((fTemp429 >= 10));
			int iTemp446 = int((fTemp429 >= 21));
			int iTemp447 = int((fTemp429 >= 22));
			int iTemp448 = int((fTemp429 >= 24));
			int iTemp449 = int((fTemp429 >= 23));
			int iTemp450 = int((fTemp429 >= 26));
			int iTemp451 = int((fTemp429 >= 27));
			int iTemp452 = int((fTemp429 >= 28));
			int iTemp453 = int((fTemp429 >= 25));
			int iTemp454 = int((fTemp429 >= 31));
			int iTemp455 = int((fTemp429 >= 32));
			int iTemp456 = int((fTemp429 >= 34));
			int iTemp457 = int((fTemp429 >= 33));
			int iTemp458 = int((fTemp429 >= 36));
			int iTemp459 = int((fTemp429 >= 37));
			int iTemp460 = int((fTemp429 >= 38));
			int iTemp461 = int((fTemp429 >= 35));
			int iTemp462 = int((fTemp429 >= 30));
			int iTemp463 = int((fTemp429 >= 20));
			int iTemp464 = int((fTemp429 >= 41));
			int iTemp465 = int((fTemp429 >= 42));
			int iTemp466 = int((fTemp429 >= 44));
			int iTemp467 = int((fTemp429 >= 43));
			int iTemp468 = int((fTemp429 >= 46));
			int iTemp469 = int((fTemp429 >= 47));
			int iTemp470 = int((fTemp429 >= 49));
			int iTemp471 = int((fTemp429 >= 48));
			int iTemp472 = int((fTemp429 >= 45));
			int iTemp473 = int((fTemp429 >= 51));
			int iTemp474 = int((fTemp429 >= 52));
			int iTemp475 = int((fTemp429 >= 54));
			int iTemp476 = int((fTemp429 >= 53));
			int iTemp477 = int((fTemp429 >= 56));
			int iTemp478 = int((fTemp429 >= 57));
			int iTemp479 = int((fTemp429 >= 59));
			int iTemp480 = int((fTemp429 >= 58));
			int iTemp481 = int((fTemp429 >= 55));
			int iTemp482 = int((fTemp429 >= 50));
			int iTemp483 = int((fTemp429 >= 61));
			int iTemp484 = int((fTemp429 >= 62));
			int iTemp485 = int((fTemp429 >= 64));
			int iTemp486 = int((fTemp429 >= 63));
			int iTemp487 = int((fTemp429 >= 66));
			int iTemp488 = int((fTemp429 >= 67));
			int iTemp489 = int((fTemp429 >= 69));
			int iTemp490 = int((fTemp429 >= 68));
			int iTemp491 = int((fTemp429 >= 65));
			int iTemp492 = int((fTemp429 >= 71));
			int iTemp493 = int((fTemp429 >= 72));
			int iTemp494 = int((fTemp429 >= 73));
			int iTemp495 = int((fTemp429 >= 76));
			int iTemp496 = int((fTemp429 >= 77));
			int iTemp497 = int((fTemp429 >= 79));
			int iTemp498 = int((fTemp429 >= 78));
			int iTemp499 = int((fTemp429 >= 75));
			int iTemp500 = int((fTemp429 >= 70));
			int iTemp501 = int((fTemp429 >= 60));
			int iTemp502 = int((fTemp429 >= 40));
			fRec78[0] = ((fSlow14 * fRec78[1]) + (fSlow15 * ((iTemp502)?((iTemp501)?((iTemp500)?((iTemp499)?((iTemp498)?((iTemp497)?4950:3800):((iTemp496)?2700:((iTemp495)?700:325))):((iTemp494)?((int((fTemp429 >= 74)))?4950:3800):((iTemp493)?2830:((iTemp492)?800:450)))):((iTemp491)?((iTemp490)?((iTemp489)?4950:3600):((iTemp488)?2800:((iTemp487)?2000:350))):((iTemp486)?((iTemp485)?4950:3900):((iTemp484)?2900:((iTemp483)?1150:800))))):((iTemp482)?((iTemp481)?((iTemp480)?((iTemp479)?4950:3500):((iTemp478)?2530:((iTemp477)?700:325))):((iTemp476)?((iTemp475)?4950:3500):((iTemp474)?2830:((iTemp473)?800:450)))):((iTemp472)?((iTemp471)?((iTemp470)?4950:3300):((iTemp469)?2700:((iTemp468)?1600:400))):((iTemp467)?((iTemp466)?4950:3500):((iTemp465)?2800:((iTemp464)?1150:800)))))):((iTemp463)?((iTemp462)?((iTemp461)?((iTemp460)?((int((fTemp429 >= 39)))?3300:2900):((iTemp459)?2700:((iTemp458)?600:350))):((iTemp457)?((iTemp456)?3000:2800):((iTemp455)?2600:((iTemp454)?800:400)))):((iTemp453)?((iTemp452)?((int((fTemp429 >= 29)))?3580:3200):((iTemp451)?2600:((iTemp450)?1700:400))):((iTemp449)?((iTemp448)?3250:2900):((iTemp447)?2650:((iTemp446)?1080:650))))):((iTemp445)?((iTemp444)?((iTemp443)?((int((fTemp429 >= 19)))?2950:2675):((iTemp442)?2400:((iTemp441)?600:350))):((iTemp440)?((int((fTemp429 >= 14)))?2900:2600):((iTemp439)?2400:((iTemp438)?750:400)))):((iTemp437)?((iTemp436)?((int((fTemp429 >= 9)))?3100:2800):((iTemp435)?2400:((iTemp434)?1620:400))):((iTemp433)?((iTemp432)?2750:2450):((iTemp431)?2250:((iTemp430)?1040:600)))))))));
			float fTemp503 = (fConst43 * (fTemp253 + ((iSlow27 * ((fSlow20 * fRec78[0]) + (fSlow25 * fRec76[0]))) + (iSlow24 * ((fSlow23 * fRec77[0]) + (fSlow21 * fRec76[0]))))));
			float fTemp504 = (fTemp503 + (iTemp19 * fRec75[1]));
			fRec75[0] = (fTemp504 - floorf(fTemp504));
			fRec80[0] = ((fSlow14 * fRec80[1]) + (fSlow15 * ((iTemp348)?((iTemp347)?((iTemp346)?((iTemp345)?((iTemp344)?((iTemp343)?200:180):((iTemp342)?170:((iTemp341)?60:50))):((iTemp340)?120:((iTemp339)?100:((iTemp338)?80:40)))):((iTemp337)?((iTemp336)?((iTemp335)?200:150):((iTemp334)?120:((iTemp333)?100:60))):((iTemp332)?((iTemp331)?140:130):((iTemp330)?120:((iTemp329)?90:80))))):((iTemp328)?((iTemp327)?((iTemp326)?((iTemp325)?200:180):((iTemp324)?170:((iTemp323)?60:50))):((iTemp322)?((iTemp321)?135:130):((iTemp320)?100:((iTemp319)?80:70)))):((iTemp318)?((iTemp317)?((iTemp316)?200:150):((iTemp315)?120:((iTemp314)?80:60))):((iTemp313)?((iTemp312)?140:130):((iTemp311)?120:((iTemp310)?90:80)))))):((iTemp309)?((iTemp308)?((iTemp307)?((iTemp306)?120:((iTemp305)?100:((iTemp304)?60:40))):((iTemp303)?((iTemp302)?135:130):((iTemp301)?100:((iTemp300)?80:70)))):((iTemp299)?((iTemp298)?120:((iTemp297)?100:((iTemp296)?80:70))):((iTemp295)?((iTemp294)?140:130):((iTemp293)?120:((iTemp292)?90:80))))):((iTemp291)?((iTemp290)?((iTemp289)?120:((iTemp288)?100:((iTemp287)?80:40))):((iTemp286)?120:((iTemp285)?100:((iTemp284)?80:40)))):((iTemp283)?((iTemp282)?120:((iTemp281)?100:((iTemp280)?80:40))):((iTemp279)?((iTemp278)?130:120):((iTemp277)?110:((iTemp276)?70:60)))))))));
			fRec81[0] = ((fSlow14 * fRec81[1]) + (fSlow15 * ((iTemp428)?((iTemp427)?((iTemp426)?((iTemp425)?((iTemp424)?((iTemp423)?200:180):((iTemp422)?170:((iTemp421)?60:50))):((iTemp420)?120:((iTemp418)?100:((iTemp417)?80:40)))):((iTemp416)?((iTemp415)?((iTemp414)?200:150):((iTemp413)?120:((iTemp412)?100:60))):((iTemp411)?((iTemp410)?140:130):((iTemp409)?120:((iTemp408)?90:80))))):((iTemp407)?((iTemp406)?((iTemp405)?((iTemp404)?200:180):((iTemp403)?170:((iTemp402)?60:50))):((iTemp401)?((iTemp400)?135:130):((iTemp399)?100:((iTemp398)?80:70)))):((iTemp397)?((iTemp396)?((iTemp395)?200:150):((iTemp394)?120:((iTemp393)?80:60))):((iTemp392)?((iTemp391)?140:130):((iTemp390)?120:((iTemp389)?90:80)))))):((iTemp388)?((iTemp387)?((iTemp386)?((iTemp385)?120:((iTemp383)?100:((iTemp382)?60:40))):((iTemp381)?((iTemp380)?135:130):((iTemp379)?100:((iTemp378)?80:70)))):((iTemp377)?((iTemp376)?120:((iTemp374)?100:((iTemp373)?80:70))):((iTemp372)?((iTemp371)?140:130):((iTemp370)?120:((iTemp369)?90:80))))):((iTemp368)?((iTemp367)?((iTemp366)?120:((iTemp364)?100:((iTemp363)?80:40))):((iTemp362)?120:((iTemp360)?100:((iTemp359)?80:40)))):((iTemp358)?((iTemp357)?120:((iTemp355)?100:((iTemp354)?80:40))):((iTemp353)?((iTemp352)?130:120):((iTemp351)?110:((iTemp350)?70:60)))))))));
			fRec82[0] = ((fSlow14 * fRec82[1]) + (fSlow15 * ((iTemp502)?((iTemp501)?((iTemp500)?((iTemp499)?((iTemp498)?((iTemp497)?200:180):((iTemp496)?170:((iTemp495)?60:50))):((iTemp494)?120:((iTemp493)?100:((iTemp492)?80:40)))):((iTemp491)?((iTemp490)?((iTemp489)?200:150):((iTemp488)?120:((iTemp487)?100:60))):((iTemp486)?((iTemp485)?140:130):((iTemp484)?120:((iTemp483)?90:80))))):((iTemp482)?((iTemp481)?((iTemp480)?((iTemp479)?200:180):((iTemp478)?170:((iTemp477)?60:50))):((iTemp476)?((iTemp475)?135:130):((iTemp474)?100:((iTemp473)?80:70)))):((iTemp472)?((iTemp471)?((iTemp470)?200:150):((iTemp469)?120:((iTemp468)?80:60))):((iTemp467)?((iTemp466)?140:130):((iTemp465)?120:((iTemp464)?90:80)))))):((iTemp463)?((iTemp462)?((iTemp461)?((iTemp460)?120:((iTemp459)?100:((iTemp458)?60:40))):((iTemp457)?((iTemp456)?135:130):((iTemp455)?100:((iTemp454)?80:70)))):((iTemp453)?((iTemp452)?120:((iTemp451)?100:((iTemp450)?80:70))):((iTemp449)?((iTemp448)?140:130):((iTemp447)?120:((iTemp446)?90:80))))):((iTemp445)?((iTemp444)?((iTemp443)?120:((iTemp442)?100:((iTemp441)?80:40))):((iTemp440)?120:((iTemp439)?100:((iTemp438)?80:40)))):((iTemp437)?((iTemp436)?120:((iTemp435)?100:((iTemp434)?80:40))):((iTemp433)?((iTemp432)?130:120):((iTemp431)?110:((iTemp430)?70:60)))))))));
			float fTemp505 = (fTemp258 + ((iSlow27 * ((fSlow20 * fRec82[0]) + (fSlow25 * fRec80[0]))) + (iSlow24 * ((fSlow23 * fRec81[0]) + (fSlow21 * fRec80[0])))));
			float fTemp506 = (iTemp256 * fTemp505);
			fRec79[0] = (fTemp506 + (iTemp257 * fRec79[1]));
			float fTemp507 = expf((fConst5 * (0 - fRec79[0])));
			fRec83[0] = ((iTemp257 * fRec83[1]) + (fTemp506 * fTemp265));
			float fTemp508 = expf((fConst5 * (0 - fRec83[0])));
			fRec84[0] = (iVec2[2] - ((fRec84[1] * (0 - (fTemp508 + fTemp507))) + ((fRec84[2] * fTemp508) * fTemp507)));
			float fTemp509 = (fTemp503 + (iTemp268 * fRec85[1]));
			fRec85[0] = (fTemp509 - floorf(fTemp509));
			float fTemp510 = (iTemp270 * fTemp505);
			fRec86[0] = (fTemp510 + (iTemp271 * fRec86[1]));
			float fTemp511 = expf((fConst5 * (0 - fRec86[0])));
			fRec87[0] = ((iTemp271 * fRec87[1]) + (fTemp510 * fTemp265));
			float fTemp512 = expf((fConst5 * (0 - fRec87[0])));
			fRec88[0] = (iVec3[2] - ((fRec88[1] * (0 - (fTemp512 + fTemp511))) + ((fRec88[2] * fTemp512) * fTemp511)));
			fRec89[0] = ((fSlow14 * fRec89[1]) + (fSlow15 * powf(10,(0.05f * ((iTemp428)?((iTemp427)?((iTemp426)?((iTemp425)?((iTemp424)?((iTemp423)?-60:-40):((iTemp422)?-35:((iTemp421)?-16:0))):((iTemp420)?((iTemp419)?-50:-22):((iTemp418)?-22:((iTemp417)?-11:0)))):((iTemp416)?((iTemp415)?((iTemp414)?-56:-40):((iTemp413)?-15:((iTemp412)?-20:0))):((iTemp411)?((iTemp410)?-50:-20):((iTemp409)?-32:((iTemp408)?-6:0))))):((iTemp407)?((iTemp406)?((iTemp405)?((iTemp404)?-64:-40):((iTemp403)?-30:((iTemp402)?-12:0))):((iTemp401)?((iTemp400)?-55:-28):((iTemp399)?-16:((iTemp398)?-9:0)))):((iTemp397)?((iTemp396)?((iTemp395)?-60:-35):((iTemp394)?-30:((iTemp393)?-24:0))):((iTemp392)?((iTemp391)?-60:-36):((iTemp390)?-20:((iTemp389)?-4:0)))))):((iTemp388)?((iTemp387)?((iTemp386)?((iTemp385)?((iTemp384)?-26:-14):((iTemp383)?-17:((iTemp382)?-20:0))):((iTemp381)?((iTemp380)?-26:-12):((iTemp379)?-12:((iTemp378)?-10:0)))):((iTemp377)?((iTemp376)?((iTemp375)?-20:-14):((iTemp374)?-12:((iTemp373)?-14:0))):((iTemp372)?((iTemp371)?-22:-8):((iTemp370)?-7:((iTemp369)?-6:0))))):((iTemp368)?((iTemp367)?((iTemp366)?((iTemp365)?-36:-28):((iTemp364)?-32:((iTemp363)?-20:0))):((iTemp362)?((iTemp361)?-40:-20):((iTemp360)?-21:((iTemp359)?-11:0)))):((iTemp358)?((iTemp357)?((iTemp356)?-18:-12):((iTemp355)?-9:((iTemp354)?-12:0))):((iTemp353)?((iTemp352)?-20:-9):((iTemp351)?-9:((iTemp350)?-7:0)))))))))));
			float fTemp513 = (fTemp22 + 2);
			int iTemp514 = int((fTemp513 >= 1));
			int iTemp515 = int((fTemp513 >= 2));
			int iTemp516 = int((fTemp513 >= 4));
			int iTemp517 = int((fTemp513 >= 3));
			int iTemp518 = int((fTemp513 >= 6));
			int iTemp519 = int((fTemp513 >= 7));
			int iTemp520 = int((fTemp513 >= 8));
			int iTemp521 = int((fTemp513 >= 5));
			int iTemp522 = int((fTemp513 >= 11));
			int iTemp523 = int((fTemp513 >= 12));
			int iTemp524 = int((fTemp513 >= 13));
			int iTemp525 = int((fTemp513 >= 16));
			int iTemp526 = int((fTemp513 >= 17));
			int iTemp527 = int((fTemp513 >= 18));
			int iTemp528 = int((fTemp513 >= 15));
			int iTemp529 = int((fTemp513 >= 10));
			int iTemp530 = int((fTemp513 >= 21));
			int iTemp531 = int((fTemp513 >= 22));
			int iTemp532 = int((fTemp513 >= 24));
			int iTemp533 = int((fTemp513 >= 23));
			int iTemp534 = int((fTemp513 >= 26));
			int iTemp535 = int((fTemp513 >= 27));
			int iTemp536 = int((fTemp513 >= 28));
			int iTemp537 = int((fTemp513 >= 25));
			int iTemp538 = int((fTemp513 >= 31));
			int iTemp539 = int((fTemp513 >= 32));
			int iTemp540 = int((fTemp513 >= 34));
			int iTemp541 = int((fTemp513 >= 33));
			int iTemp542 = int((fTemp513 >= 36));
			int iTemp543 = int((fTemp513 >= 37));
			int iTemp544 = int((fTemp513 >= 38));
			int iTemp545 = int((fTemp513 >= 35));
			int iTemp546 = int((fTemp513 >= 30));
			int iTemp547 = int((fTemp513 >= 20));
			int iTemp548 = int((fTemp513 >= 41));
			int iTemp549 = int((fTemp513 >= 42));
			int iTemp550 = int((fTemp513 >= 44));
			int iTemp551 = int((fTemp513 >= 43));
			int iTemp552 = int((fTemp513 >= 46));
			int iTemp553 = int((fTemp513 >= 47));
			int iTemp554 = int((fTemp513 >= 49));
			int iTemp555 = int((fTemp513 >= 48));
			int iTemp556 = int((fTemp513 >= 45));
			int iTemp557 = int((fTemp513 >= 51));
			int iTemp558 = int((fTemp513 >= 52));
			int iTemp559 = int((fTemp513 >= 54));
			int iTemp560 = int((fTemp513 >= 53));
			int iTemp561 = int((fTemp513 >= 56));
			int iTemp562 = int((fTemp513 >= 57));
			int iTemp563 = int((fTemp513 >= 59));
			int iTemp564 = int((fTemp513 >= 58));
			int iTemp565 = int((fTemp513 >= 55));
			int iTemp566 = int((fTemp513 >= 50));
			int iTemp567 = int((fTemp513 >= 61));
			int iTemp568 = int((fTemp513 >= 62));
			int iTemp569 = int((fTemp513 >= 64));
			int iTemp570 = int((fTemp513 >= 63));
			int iTemp571 = int((fTemp513 >= 66));
			int iTemp572 = int((fTemp513 >= 67));
			int iTemp573 = int((fTemp513 >= 69));
			int iTemp574 = int((fTemp513 >= 68));
			int iTemp575 = int((fTemp513 >= 65));
			int iTemp576 = int((fTemp513 >= 71));
			int iTemp577 = int((fTemp513 >= 72));
			int iTemp578 = int((fTemp513 >= 73));
			int iTemp579 = int((fTemp513 >= 76));
			int iTemp580 = int((fTemp513 >= 77));
			int iTemp581 = int((fTemp513 >= 79));
			int iTemp582 = int((fTemp513 >= 78));
			int iTemp583 = int((fTemp513 >= 75));
			int iTemp584 = int((fTemp513 >= 70));
			int iTemp585 = int((fTemp513 >= 60));
			int iTemp586 = int((fTemp513 >= 40));
			fRec91[0] = ((fSlow14 * fRec91[1]) + (fSlow15 * ((iTemp586)?((iTemp585)?((iTemp584)?((iTemp583)?((iTemp582)?((iTemp581)?4950:3800):((iTemp580)?2700:((iTemp579)?700:325))):((iTemp578)?((int((fTemp513 >= 74)))?4950:3800):((iTemp577)?2830:((iTemp576)?800:450)))):((iTemp575)?((iTemp574)?((iTemp573)?4950:3600):((iTemp572)?2800:((iTemp571)?2000:350))):((iTemp570)?((iTemp569)?4950:3900):((iTemp568)?2900:((iTemp567)?1150:800))))):((iTemp566)?((iTemp565)?((iTemp564)?((iTemp563)?4950:3500):((iTemp562)?2530:((iTemp561)?700:325))):((iTemp560)?((iTemp559)?4950:3500):((iTemp558)?2830:((iTemp557)?800:450)))):((iTemp556)?((iTemp555)?((iTemp554)?4950:3300):((iTemp553)?2700:((iTemp552)?1600:400))):((iTemp551)?((iTemp550)?4950:3500):((iTemp549)?2800:((iTemp548)?1150:800)))))):((iTemp547)?((iTemp546)?((iTemp545)?((iTemp544)?((int((fTemp513 >= 39)))?3300:2900):((iTemp543)?2700:((iTemp542)?600:350))):((iTemp541)?((iTemp540)?3000:2800):((iTemp539)?2600:((iTemp538)?800:400)))):((iTemp537)?((iTemp536)?((int((fTemp513 >= 29)))?3580:3200):((iTemp535)?2600:((iTemp534)?1700:400))):((iTemp533)?((iTemp532)?3250:2900):((iTemp531)?2650:((iTemp530)?1080:650))))):((iTemp529)?((iTemp528)?((iTemp527)?((int((fTemp513 >= 19)))?2950:2675):((iTemp526)?2400:((iTemp525)?600:350))):((iTemp524)?((int((fTemp513 >= 14)))?2900:2600):((iTemp523)?2400:((iTemp522)?750:400)))):((iTemp521)?((iTemp520)?((int((fTemp513 >= 9)))?3100:2800):((iTemp519)?2400:((iTemp518)?1620:400))):((iTemp517)?((iTemp516)?2750:2450):((iTemp515)?2250:((iTemp514)?1040:600)))))))));
			float fTemp587 = (fTemp97 + 2);
			int iTemp588 = int((fTemp587 >= 1));
			int iTemp589 = int((fTemp587 >= 2));
			int iTemp590 = int((fTemp587 >= 4));
			int iTemp591 = int((fTemp587 >= 3));
			int iTemp592 = int((fTemp587 >= 6));
			int iTemp593 = int((fTemp587 >= 7));
			int iTemp594 = int((fTemp587 >= 9));
			int iTemp595 = int((fTemp587 >= 8));
			int iTemp596 = int((fTemp587 >= 5));
			int iTemp597 = int((fTemp587 >= 11));
			int iTemp598 = int((fTemp587 >= 12));
			int iTemp599 = int((fTemp587 >= 14));
			int iTemp600 = int((fTemp587 >= 13));
			int iTemp601 = int((fTemp587 >= 16));
			int iTemp602 = int((fTemp587 >= 17));
			int iTemp603 = int((fTemp587 >= 19));
			int iTemp604 = int((fTemp587 >= 18));
			int iTemp605 = int((fTemp587 >= 15));
			int iTemp606 = int((fTemp587 >= 10));
			int iTemp607 = int((fTemp587 >= 21));
			int iTemp608 = int((fTemp587 >= 22));
			int iTemp609 = int((fTemp587 >= 24));
			int iTemp610 = int((fTemp587 >= 23));
			int iTemp611 = int((fTemp587 >= 26));
			int iTemp612 = int((fTemp587 >= 27));
			int iTemp613 = int((fTemp587 >= 29));
			int iTemp614 = int((fTemp587 >= 28));
			int iTemp615 = int((fTemp587 >= 25));
			int iTemp616 = int((fTemp587 >= 31));
			int iTemp617 = int((fTemp587 >= 32));
			int iTemp618 = int((fTemp587 >= 34));
			int iTemp619 = int((fTemp587 >= 33));
			int iTemp620 = int((fTemp587 >= 36));
			int iTemp621 = int((fTemp587 >= 37));
			int iTemp622 = int((fTemp587 >= 39));
			int iTemp623 = int((fTemp587 >= 38));
			int iTemp624 = int((fTemp587 >= 35));
			int iTemp625 = int((fTemp587 >= 30));
			int iTemp626 = int((fTemp587 >= 20));
			int iTemp627 = int((fTemp587 >= 41));
			int iTemp628 = int((fTemp587 >= 42));
			int iTemp629 = int((fTemp587 >= 44));
			int iTemp630 = int((fTemp587 >= 43));
			int iTemp631 = int((fTemp587 >= 46));
			int iTemp632 = int((fTemp587 >= 47));
			int iTemp633 = int((fTemp587 >= 49));
			int iTemp634 = int((fTemp587 >= 48));
			int iTemp635 = int((fTemp587 >= 45));
			int iTemp636 = int((fTemp587 >= 51));
			int iTemp637 = int((fTemp587 >= 52));
			int iTemp638 = int((fTemp587 >= 54));
			int iTemp639 = int((fTemp587 >= 53));
			int iTemp640 = int((fTemp587 >= 56));
			int iTemp641 = int((fTemp587 >= 57));
			int iTemp642 = int((fTemp587 >= 59));
			int iTemp643 = int((fTemp587 >= 58));
			int iTemp644 = int((fTemp587 >= 55));
			int iTemp645 = int((fTemp587 >= 50));
			int iTemp646 = int((fTemp587 >= 61));
			int iTemp647 = int((fTemp587 >= 62));
			int iTemp648 = int((fTemp587 >= 64));
			int iTemp649 = int((fTemp587 >= 63));
			int iTemp650 = int((fTemp587 >= 66));
			int iTemp651 = int((fTemp587 >= 67));
			int iTemp652 = int((fTemp587 >= 69));
			int iTemp653 = int((fTemp587 >= 68));
			int iTemp654 = int((fTemp587 >= 65));
			int iTemp655 = int((fTemp587 >= 71));
			int iTemp656 = int((fTemp587 >= 72));
			int iTemp657 = int((fTemp587 >= 74));
			int iTemp658 = int((fTemp587 >= 73));
			int iTemp659 = int((fTemp587 >= 76));
			int iTemp660 = int((fTemp587 >= 77));
			int iTemp661 = int((fTemp587 >= 79));
			int iTemp662 = int((fTemp587 >= 78));
			int iTemp663 = int((fTemp587 >= 75));
			int iTemp664 = int((fTemp587 >= 70));
			int iTemp665 = int((fTemp587 >= 60));
			int iTemp666 = int((fTemp587 >= 40));
			fRec92[0] = ((fSlow14 * fRec92[1]) + (fSlow15 * ((iTemp666)?((iTemp665)?((iTemp664)?((iTemp663)?((iTemp662)?((iTemp661)?4950:3800):((iTemp660)?2700:((iTemp659)?700:325))):((iTemp658)?((iTemp657)?4950:3800):((iTemp656)?2830:((iTemp655)?800:450)))):((iTemp654)?((iTemp653)?((iTemp652)?4950:3600):((iTemp651)?2800:((iTemp650)?2000:350))):((iTemp649)?((iTemp648)?4950:3900):((iTemp647)?2900:((iTemp646)?1150:800))))):((iTemp645)?((iTemp644)?((iTemp643)?((iTemp642)?4950:3500):((iTemp641)?2530:((iTemp640)?700:325))):((iTemp639)?((iTemp638)?4950:3500):((iTemp637)?2830:((iTemp636)?800:450)))):((iTemp635)?((iTemp634)?((iTemp633)?4950:3300):((iTemp632)?2700:((iTemp631)?1600:400))):((iTemp630)?((iTemp629)?4950:3500):((iTemp628)?2800:((iTemp627)?1150:800)))))):((iTemp626)?((iTemp625)?((iTemp624)?((iTemp623)?((iTemp622)?3300:2900):((iTemp621)?2700:((iTemp620)?600:350))):((iTemp619)?((iTemp618)?3000:2800):((iTemp617)?2600:((iTemp616)?800:400)))):((iTemp615)?((iTemp614)?((iTemp613)?3580:3200):((iTemp612)?2600:((iTemp611)?1700:400))):((iTemp610)?((iTemp609)?3250:2900):((iTemp608)?2650:((iTemp607)?1080:650))))):((iTemp606)?((iTemp605)?((iTemp604)?((iTemp603)?2950:2675):((iTemp602)?2400:((iTemp601)?600:350))):((iTemp600)?((iTemp599)?2900:2600):((iTemp598)?2400:((iTemp597)?750:400)))):((iTemp596)?((iTemp595)?((iTemp594)?3100:2800):((iTemp593)?2400:((iTemp592)?1620:400))):((iTemp591)?((iTemp590)?2750:2450):((iTemp589)?2250:((iTemp588)?1040:600)))))))));
			float fTemp667 = (fTemp178 + 2);
			int iTemp668 = int((fTemp667 >= 1));
			int iTemp669 = int((fTemp667 >= 2));
			int iTemp670 = int((fTemp667 >= 4));
			int iTemp671 = int((fTemp667 >= 3));
			int iTemp672 = int((fTemp667 >= 6));
			int iTemp673 = int((fTemp667 >= 7));
			int iTemp674 = int((fTemp667 >= 8));
			int iTemp675 = int((fTemp667 >= 5));
			int iTemp676 = int((fTemp667 >= 11));
			int iTemp677 = int((fTemp667 >= 12));
			int iTemp678 = int((fTemp667 >= 13));
			int iTemp679 = int((fTemp667 >= 16));
			int iTemp680 = int((fTemp667 >= 17));
			int iTemp681 = int((fTemp667 >= 18));
			int iTemp682 = int((fTemp667 >= 15));
			int iTemp683 = int((fTemp667 >= 10));
			int iTemp684 = int((fTemp667 >= 21));
			int iTemp685 = int((fTemp667 >= 22));
			int iTemp686 = int((fTemp667 >= 24));
			int iTemp687 = int((fTemp667 >= 23));
			int iTemp688 = int((fTemp667 >= 26));
			int iTemp689 = int((fTemp667 >= 27));
			int iTemp690 = int((fTemp667 >= 28));
			int iTemp691 = int((fTemp667 >= 25));
			int iTemp692 = int((fTemp667 >= 31));
			int iTemp693 = int((fTemp667 >= 32));
			int iTemp694 = int((fTemp667 >= 34));
			int iTemp695 = int((fTemp667 >= 33));
			int iTemp696 = int((fTemp667 >= 36));
			int iTemp697 = int((fTemp667 >= 37));
			int iTemp698 = int((fTemp667 >= 38));
			int iTemp699 = int((fTemp667 >= 35));
			int iTemp700 = int((fTemp667 >= 30));
			int iTemp701 = int((fTemp667 >= 20));
			int iTemp702 = int((fTemp667 >= 41));
			int iTemp703 = int((fTemp667 >= 42));
			int iTemp704 = int((fTemp667 >= 44));
			int iTemp705 = int((fTemp667 >= 43));
			int iTemp706 = int((fTemp667 >= 46));
			int iTemp707 = int((fTemp667 >= 47));
			int iTemp708 = int((fTemp667 >= 49));
			int iTemp709 = int((fTemp667 >= 48));
			int iTemp710 = int((fTemp667 >= 45));
			int iTemp711 = int((fTemp667 >= 51));
			int iTemp712 = int((fTemp667 >= 52));
			int iTemp713 = int((fTemp667 >= 54));
			int iTemp714 = int((fTemp667 >= 53));
			int iTemp715 = int((fTemp667 >= 56));
			int iTemp716 = int((fTemp667 >= 57));
			int iTemp717 = int((fTemp667 >= 59));
			int iTemp718 = int((fTemp667 >= 58));
			int iTemp719 = int((fTemp667 >= 55));
			int iTemp720 = int((fTemp667 >= 50));
			int iTemp721 = int((fTemp667 >= 61));
			int iTemp722 = int((fTemp667 >= 62));
			int iTemp723 = int((fTemp667 >= 64));
			int iTemp724 = int((fTemp667 >= 63));
			int iTemp725 = int((fTemp667 >= 66));
			int iTemp726 = int((fTemp667 >= 67));
			int iTemp727 = int((fTemp667 >= 69));
			int iTemp728 = int((fTemp667 >= 68));
			int iTemp729 = int((fTemp667 >= 65));
			int iTemp730 = int((fTemp667 >= 71));
			int iTemp731 = int((fTemp667 >= 72));
			int iTemp732 = int((fTemp667 >= 73));
			int iTemp733 = int((fTemp667 >= 76));
			int iTemp734 = int((fTemp667 >= 77));
			int iTemp735 = int((fTemp667 >= 79));
			int iTemp736 = int((fTemp667 >= 78));
			int iTemp737 = int((fTemp667 >= 75));
			int iTemp738 = int((fTemp667 >= 70));
			int iTemp739 = int((fTemp667 >= 60));
			int iTemp740 = int((fTemp667 >= 40));
			fRec93[0] = ((fSlow14 * fRec93[1]) + (fSlow15 * ((iTemp740)?((iTemp739)?((iTemp738)?((iTemp737)?((iTemp736)?((iTemp735)?4950:3800):((iTemp734)?2700:((iTemp733)?700:325))):((iTemp732)?((int((fTemp667 >= 74)))?4950:3800):((iTemp731)?2830:((iTemp730)?800:450)))):((iTemp729)?((iTemp728)?((iTemp727)?4950:3600):((iTemp726)?2800:((iTemp725)?2000:350))):((iTemp724)?((iTemp723)?4950:3900):((iTemp722)?2900:((iTemp721)?1150:800))))):((iTemp720)?((iTemp719)?((iTemp718)?((iTemp717)?4950:3500):((iTemp716)?2530:((iTemp715)?700:325))):((iTemp714)?((iTemp713)?4950:3500):((iTemp712)?2830:((iTemp711)?800:450)))):((iTemp710)?((iTemp709)?((iTemp708)?4950:3300):((iTemp707)?2700:((iTemp706)?1600:400))):((iTemp705)?((iTemp704)?4950:3500):((iTemp703)?2800:((iTemp702)?1150:800)))))):((iTemp701)?((iTemp700)?((iTemp699)?((iTemp698)?((int((fTemp667 >= 39)))?3300:2900):((iTemp697)?2700:((iTemp696)?600:350))):((iTemp695)?((iTemp694)?3000:2800):((iTemp693)?2600:((iTemp692)?800:400)))):((iTemp691)?((iTemp690)?((int((fTemp667 >= 29)))?3580:3200):((iTemp689)?2600:((iTemp688)?1700:400))):((iTemp687)?((iTemp686)?3250:2900):((iTemp685)?2650:((iTemp684)?1080:650))))):((iTemp683)?((iTemp682)?((iTemp681)?((int((fTemp667 >= 19)))?2950:2675):((iTemp680)?2400:((iTemp679)?600:350))):((iTemp678)?((int((fTemp667 >= 14)))?2900:2600):((iTemp677)?2400:((iTemp676)?750:400)))):((iTemp675)?((iTemp674)?((int((fTemp667 >= 9)))?3100:2800):((iTemp673)?2400:((iTemp672)?1620:400))):((iTemp671)?((iTemp670)?2750:2450):((iTemp669)?2250:((iTemp668)?1040:600)))))))));
			float fTemp741 = (fConst43 * (fTemp253 + ((iSlow27 * ((fSlow20 * fRec93[0]) + (fSlow25 * fRec91[0]))) + (iSlow24 * ((fSlow23 * fRec92[0]) + (fSlow21 * fRec91[0]))))));
			float fTemp742 = (fTemp741 + (iTemp19 * fRec90[1]));
			fRec90[0] = (fTemp742 - floorf(fTemp742));
			fRec95[0] = ((fSlow14 * fRec95[1]) + (fSlow15 * ((iTemp586)?((iTemp585)?((iTemp584)?((iTemp583)?((iTemp582)?((iTemp581)?200:180):((iTemp580)?170:((iTemp579)?60:50))):((iTemp578)?120:((iTemp577)?100:((iTemp576)?80:40)))):((iTemp575)?((iTemp574)?((iTemp573)?200:150):((iTemp572)?120:((iTemp571)?100:60))):((iTemp570)?((iTemp569)?140:130):((iTemp568)?120:((iTemp567)?90:80))))):((iTemp566)?((iTemp565)?((iTemp564)?((iTemp563)?200:180):((iTemp562)?170:((iTemp561)?60:50))):((iTemp560)?((iTemp559)?135:130):((iTemp558)?100:((iTemp557)?80:70)))):((iTemp556)?((iTemp555)?((iTemp554)?200:150):((iTemp553)?120:((iTemp552)?80:60))):((iTemp551)?((iTemp550)?140:130):((iTemp549)?120:((iTemp548)?90:80)))))):((iTemp547)?((iTemp546)?((iTemp545)?((iTemp544)?120:((iTemp543)?100:((iTemp542)?60:40))):((iTemp541)?((iTemp540)?135:130):((iTemp539)?100:((iTemp538)?80:70)))):((iTemp537)?((iTemp536)?120:((iTemp535)?100:((iTemp534)?80:70))):((iTemp533)?((iTemp532)?140:130):((iTemp531)?120:((iTemp530)?90:80))))):((iTemp529)?((iTemp528)?((iTemp527)?120:((iTemp526)?100:((iTemp525)?80:40))):((iTemp524)?120:((iTemp523)?100:((iTemp522)?80:40)))):((iTemp521)?((iTemp520)?120:((iTemp519)?100:((iTemp518)?80:40))):((iTemp517)?((iTemp516)?130:120):((iTemp515)?110:((iTemp514)?70:60)))))))));
			fRec96[0] = ((fSlow14 * fRec96[1]) + (fSlow15 * ((iTemp666)?((iTemp665)?((iTemp664)?((iTemp663)?((iTemp662)?((iTemp661)?200:180):((iTemp660)?170:((iTemp659)?60:50))):((iTemp658)?120:((iTemp656)?100:((iTemp655)?80:40)))):((iTemp654)?((iTemp653)?((iTemp652)?200:150):((iTemp651)?120:((iTemp650)?100:60))):((iTemp649)?((iTemp648)?140:130):((iTemp647)?120:((iTemp646)?90:80))))):((iTemp645)?((iTemp644)?((iTemp643)?((iTemp642)?200:180):((iTemp641)?170:((iTemp640)?60:50))):((iTemp639)?((iTemp638)?135:130):((iTemp637)?100:((iTemp636)?80:70)))):((iTemp635)?((iTemp634)?((iTemp633)?200:150):((iTemp632)?120:((iTemp631)?80:60))):((iTemp630)?((iTemp629)?140:130):((iTemp628)?120:((iTemp627)?90:80)))))):((iTemp626)?((iTemp625)?((iTemp624)?((iTemp623)?120:((iTemp621)?100:((iTemp620)?60:40))):((iTemp619)?((iTemp618)?135:130):((iTemp617)?100:((iTemp616)?80:70)))):((iTemp615)?((iTemp614)?120:((iTemp612)?100:((iTemp611)?80:70))):((iTemp610)?((iTemp609)?140:130):((iTemp608)?120:((iTemp607)?90:80))))):((iTemp606)?((iTemp605)?((iTemp604)?120:((iTemp602)?100:((iTemp601)?80:40))):((iTemp600)?120:((iTemp598)?100:((iTemp597)?80:40)))):((iTemp596)?((iTemp595)?120:((iTemp593)?100:((iTemp592)?80:40))):((iTemp591)?((iTemp590)?130:120):((iTemp589)?110:((iTemp588)?70:60)))))))));
			fRec97[0] = ((fSlow14 * fRec97[1]) + (fSlow15 * ((iTemp740)?((iTemp739)?((iTemp738)?((iTemp737)?((iTemp736)?((iTemp735)?200:180):((iTemp734)?170:((iTemp733)?60:50))):((iTemp732)?120:((iTemp731)?100:((iTemp730)?80:40)))):((iTemp729)?((iTemp728)?((iTemp727)?200:150):((iTemp726)?120:((iTemp725)?100:60))):((iTemp724)?((iTemp723)?140:130):((iTemp722)?120:((iTemp721)?90:80))))):((iTemp720)?((iTemp719)?((iTemp718)?((iTemp717)?200:180):((iTemp716)?170:((iTemp715)?60:50))):((iTemp714)?((iTemp713)?135:130):((iTemp712)?100:((iTemp711)?80:70)))):((iTemp710)?((iTemp709)?((iTemp708)?200:150):((iTemp707)?120:((iTemp706)?80:60))):((iTemp705)?((iTemp704)?140:130):((iTemp703)?120:((iTemp702)?90:80)))))):((iTemp701)?((iTemp700)?((iTemp699)?((iTemp698)?120:((iTemp697)?100:((iTemp696)?60:40))):((iTemp695)?((iTemp694)?135:130):((iTemp693)?100:((iTemp692)?80:70)))):((iTemp691)?((iTemp690)?120:((iTemp689)?100:((iTemp688)?80:70))):((iTemp687)?((iTemp686)?140:130):((iTemp685)?120:((iTemp684)?90:80))))):((iTemp683)?((iTemp682)?((iTemp681)?120:((iTemp680)?100:((iTemp679)?80:40))):((iTemp678)?120:((iTemp677)?100:((iTemp676)?80:40)))):((iTemp675)?((iTemp674)?120:((iTemp673)?100:((iTemp672)?80:40))):((iTemp671)?((iTemp670)?130:120):((iTemp669)?110:((iTemp668)?70:60)))))))));
			float fTemp743 = (fTemp258 + ((iSlow27 * ((fSlow20 * fRec97[0]) + (fSlow25 * fRec95[0]))) + (iSlow24 * ((fSlow23 * fRec96[0]) + (fSlow21 * fRec95[0])))));
			float fTemp744 = (iTemp256 * fTemp743);
			fRec94[0] = (fTemp744 + (iTemp257 * fRec94[1]));
			float fTemp745 = expf((fConst5 * (0 - fRec94[0])));
			fRec98[0] = ((iTemp257 * fRec98[1]) + (fTemp744 * fTemp265));
			float fTemp746 = expf((fConst5 * (0 - fRec98[0])));
			fRec99[0] = (iVec2[2] - ((fRec99[1] * (0 - (fTemp746 + fTemp745))) + ((fRec99[2] * fTemp746) * fTemp745)));
			float fTemp747 = (fTemp741 + (iTemp268 * fRec100[1]));
			fRec100[0] = (fTemp747 - floorf(fTemp747));
			float fTemp748 = (iTemp270 * fTemp743);
			fRec101[0] = (fTemp748 + (iTemp271 * fRec101[1]));
			float fTemp749 = expf((fConst5 * (0 - fRec101[0])));
			fRec102[0] = ((iTemp271 * fRec102[1]) + (fTemp748 * fTemp265));
			float fTemp750 = expf((fConst5 * (0 - fRec102[0])));
			fRec103[0] = (iVec3[2] - ((fRec103[1] * (0 - (fTemp750 + fTemp749))) + ((fRec103[2] * fTemp750) * fTemp749)));
			fRec104[0] = ((fSlow14 * fRec104[1]) + (fSlow15 * powf(10,(0.05f * ((iTemp666)?((iTemp665)?((iTemp664)?((iTemp663)?((iTemp662)?((iTemp661)?-60:-40):((iTemp660)?-35:((iTemp659)?-16:0))):((iTemp658)?((iTemp657)?-50:-22):((iTemp656)?-22:((iTemp655)?-11:0)))):((iTemp654)?((iTemp653)?((iTemp652)?-56:-40):((iTemp651)?-15:((iTemp650)?-20:0))):((iTemp649)?((iTemp648)?-50:-20):((iTemp647)?-32:((iTemp646)?-6:0))))):((iTemp645)?((iTemp644)?((iTemp643)?((iTemp642)?-64:-40):((iTemp641)?-30:((iTemp640)?-12:0))):((iTemp639)?((iTemp638)?-55:-28):((iTemp637)?-16:((iTemp636)?-9:0)))):((iTemp635)?((iTemp634)?((iTemp633)?-60:-35):((iTemp632)?-30:((iTemp631)?-24:0))):((iTemp630)?((iTemp629)?-60:-36):((iTemp628)?-20:((iTemp627)?-4:0)))))):((iTemp626)?((iTemp625)?((iTemp624)?((iTemp623)?((iTemp622)?-26:-14):((iTemp621)?-17:((iTemp620)?-20:0))):((iTemp619)?((iTemp618)?-26:-12):((iTemp617)?-12:((iTemp616)?-10:0)))):((iTemp615)?((iTemp614)?((iTemp613)?-20:-14):((iTemp612)?-12:((iTemp611)?-14:0))):((iTemp610)?((iTemp609)?-22:-8):((iTemp608)?-7:((iTemp607)?-6:0))))):((iTemp606)?((iTemp605)?((iTemp604)?((iTemp603)?-36:-28):((iTemp602)?-32:((iTemp601)?-20:0))):((iTemp600)?((iTemp599)?-40:-20):((iTemp598)?-21:((iTemp597)?-11:0)))):((iTemp596)?((iTemp595)?((iTemp594)?-18:-12):((iTemp593)?-9:((iTemp592)?-12:0))):((iTemp591)?((iTemp590)?-20:-9):((iTemp589)?-9:((iTemp588)?-7:0)))))))))));
			float fTemp751 = (fTemp22 + 1);
			int iTemp752 = int((fTemp751 >= 1));
			int iTemp753 = int((fTemp751 >= 2));
			int iTemp754 = int((fTemp751 >= 4));
			int iTemp755 = int((fTemp751 >= 3));
			int iTemp756 = int((fTemp751 >= 6));
			int iTemp757 = int((fTemp751 >= 7));
			int iTemp758 = int((fTemp751 >= 8));
			int iTemp759 = int((fTemp751 >= 5));
			int iTemp760 = int((fTemp751 >= 11));
			int iTemp761 = int((fTemp751 >= 12));
			int iTemp762 = int((fTemp751 >= 13));
			int iTemp763 = int((fTemp751 >= 16));
			int iTemp764 = int((fTemp751 >= 17));
			int iTemp765 = int((fTemp751 >= 18));
			int iTemp766 = int((fTemp751 >= 15));
			int iTemp767 = int((fTemp751 >= 10));
			int iTemp768 = int((fTemp751 >= 21));
			int iTemp769 = int((fTemp751 >= 22));
			int iTemp770 = int((fTemp751 >= 24));
			int iTemp771 = int((fTemp751 >= 23));
			int iTemp772 = int((fTemp751 >= 26));
			int iTemp773 = int((fTemp751 >= 27));
			int iTemp774 = int((fTemp751 >= 28));
			int iTemp775 = int((fTemp751 >= 25));
			int iTemp776 = int((fTemp751 >= 31));
			int iTemp777 = int((fTemp751 >= 32));
			int iTemp778 = int((fTemp751 >= 34));
			int iTemp779 = int((fTemp751 >= 33));
			int iTemp780 = int((fTemp751 >= 36));
			int iTemp781 = int((fTemp751 >= 37));
			int iTemp782 = int((fTemp751 >= 38));
			int iTemp783 = int((fTemp751 >= 35));
			int iTemp784 = int((fTemp751 >= 30));
			int iTemp785 = int((fTemp751 >= 20));
			int iTemp786 = int((fTemp751 >= 41));
			int iTemp787 = int((fTemp751 >= 42));
			int iTemp788 = int((fTemp751 >= 44));
			int iTemp789 = int((fTemp751 >= 43));
			int iTemp790 = int((fTemp751 >= 46));
			int iTemp791 = int((fTemp751 >= 47));
			int iTemp792 = int((fTemp751 >= 49));
			int iTemp793 = int((fTemp751 >= 48));
			int iTemp794 = int((fTemp751 >= 45));
			int iTemp795 = int((fTemp751 >= 51));
			int iTemp796 = int((fTemp751 >= 52));
			int iTemp797 = int((fTemp751 >= 54));
			int iTemp798 = int((fTemp751 >= 53));
			int iTemp799 = int((fTemp751 >= 56));
			int iTemp800 = int((fTemp751 >= 57));
			int iTemp801 = int((fTemp751 >= 59));
			int iTemp802 = int((fTemp751 >= 58));
			int iTemp803 = int((fTemp751 >= 55));
			int iTemp804 = int((fTemp751 >= 50));
			int iTemp805 = int((fTemp751 >= 61));
			int iTemp806 = int((fTemp751 >= 62));
			int iTemp807 = int((fTemp751 >= 64));
			int iTemp808 = int((fTemp751 >= 63));
			int iTemp809 = int((fTemp751 >= 66));
			int iTemp810 = int((fTemp751 >= 67));
			int iTemp811 = int((fTemp751 >= 69));
			int iTemp812 = int((fTemp751 >= 68));
			int iTemp813 = int((fTemp751 >= 65));
			int iTemp814 = int((fTemp751 >= 71));
			int iTemp815 = int((fTemp751 >= 72));
			int iTemp816 = int((fTemp751 >= 73));
			int iTemp817 = int((fTemp751 >= 76));
			int iTemp818 = int((fTemp751 >= 77));
			int iTemp819 = int((fTemp751 >= 79));
			int iTemp820 = int((fTemp751 >= 78));
			int iTemp821 = int((fTemp751 >= 75));
			int iTemp822 = int((fTemp751 >= 70));
			int iTemp823 = int((fTemp751 >= 60));
			int iTemp824 = int((fTemp751 >= 40));
			fRec106[0] = ((fSlow14 * fRec106[1]) + (fSlow15 * ((iTemp824)?((iTemp823)?((iTemp822)?((iTemp821)?((iTemp820)?((iTemp819)?4950:3800):((iTemp818)?2700:((iTemp817)?700:325))):((iTemp816)?((int((fTemp751 >= 74)))?4950:3800):((iTemp815)?2830:((iTemp814)?800:450)))):((iTemp813)?((iTemp812)?((iTemp811)?4950:3600):((iTemp810)?2800:((iTemp809)?2000:350))):((iTemp808)?((iTemp807)?4950:3900):((iTemp806)?2900:((iTemp805)?1150:800))))):((iTemp804)?((iTemp803)?((iTemp802)?((iTemp801)?4950:3500):((iTemp800)?2530:((iTemp799)?700:325))):((iTemp798)?((iTemp797)?4950:3500):((iTemp796)?2830:((iTemp795)?800:450)))):((iTemp794)?((iTemp793)?((iTemp792)?4950:3300):((iTemp791)?2700:((iTemp790)?1600:400))):((iTemp789)?((iTemp788)?4950:3500):((iTemp787)?2800:((iTemp786)?1150:800)))))):((iTemp785)?((iTemp784)?((iTemp783)?((iTemp782)?((int((fTemp751 >= 39)))?3300:2900):((iTemp781)?2700:((iTemp780)?600:350))):((iTemp779)?((iTemp778)?3000:2800):((iTemp777)?2600:((iTemp776)?800:400)))):((iTemp775)?((iTemp774)?((int((fTemp751 >= 29)))?3580:3200):((iTemp773)?2600:((iTemp772)?1700:400))):((iTemp771)?((iTemp770)?3250:2900):((iTemp769)?2650:((iTemp768)?1080:650))))):((iTemp767)?((iTemp766)?((iTemp765)?((int((fTemp751 >= 19)))?2950:2675):((iTemp764)?2400:((iTemp763)?600:350))):((iTemp762)?((int((fTemp751 >= 14)))?2900:2600):((iTemp761)?2400:((iTemp760)?750:400)))):((iTemp759)?((iTemp758)?((int((fTemp751 >= 9)))?3100:2800):((iTemp757)?2400:((iTemp756)?1620:400))):((iTemp755)?((iTemp754)?2750:2450):((iTemp753)?2250:((iTemp752)?1040:600)))))))));
			float fTemp825 = ((2 * fRec36[0]) + 30);
			float fTemp826 = (fRec36[0] + -200);
			int iTemp827 = (fRec36[0] >= 200);
			float fTemp828 = ((int(((fRec106[0] >= 1300) & iTemp827)))?(fRec106[0] - (0.00095238094f * (fTemp826 * (fRec106[0] + -1300)))):((int((fRec106[0] <= fTemp825)))?fTemp825:fRec106[0]));
			float fTemp829 = (fTemp97 + 1);
			int iTemp830 = int((fTemp829 >= 1));
			int iTemp831 = int((fTemp829 >= 2));
			int iTemp832 = int((fTemp829 >= 4));
			int iTemp833 = int((fTemp829 >= 3));
			int iTemp834 = int((fTemp829 >= 6));
			int iTemp835 = int((fTemp829 >= 7));
			int iTemp836 = int((fTemp829 >= 9));
			int iTemp837 = int((fTemp829 >= 8));
			int iTemp838 = int((fTemp829 >= 5));
			int iTemp839 = int((fTemp829 >= 11));
			int iTemp840 = int((fTemp829 >= 12));
			int iTemp841 = int((fTemp829 >= 14));
			int iTemp842 = int((fTemp829 >= 13));
			int iTemp843 = int((fTemp829 >= 16));
			int iTemp844 = int((fTemp829 >= 17));
			int iTemp845 = int((fTemp829 >= 19));
			int iTemp846 = int((fTemp829 >= 18));
			int iTemp847 = int((fTemp829 >= 15));
			int iTemp848 = int((fTemp829 >= 10));
			int iTemp849 = int((fTemp829 >= 21));
			int iTemp850 = int((fTemp829 >= 22));
			int iTemp851 = int((fTemp829 >= 24));
			int iTemp852 = int((fTemp829 >= 23));
			int iTemp853 = int((fTemp829 >= 26));
			int iTemp854 = int((fTemp829 >= 27));
			int iTemp855 = int((fTemp829 >= 29));
			int iTemp856 = int((fTemp829 >= 28));
			int iTemp857 = int((fTemp829 >= 25));
			int iTemp858 = int((fTemp829 >= 31));
			int iTemp859 = int((fTemp829 >= 32));
			int iTemp860 = int((fTemp829 >= 34));
			int iTemp861 = int((fTemp829 >= 33));
			int iTemp862 = int((fTemp829 >= 36));
			int iTemp863 = int((fTemp829 >= 37));
			int iTemp864 = int((fTemp829 >= 39));
			int iTemp865 = int((fTemp829 >= 38));
			int iTemp866 = int((fTemp829 >= 35));
			int iTemp867 = int((fTemp829 >= 30));
			int iTemp868 = int((fTemp829 >= 20));
			int iTemp869 = int((fTemp829 >= 41));
			int iTemp870 = int((fTemp829 >= 42));
			int iTemp871 = int((fTemp829 >= 44));
			int iTemp872 = int((fTemp829 >= 43));
			int iTemp873 = int((fTemp829 >= 46));
			int iTemp874 = int((fTemp829 >= 47));
			int iTemp875 = int((fTemp829 >= 49));
			int iTemp876 = int((fTemp829 >= 48));
			int iTemp877 = int((fTemp829 >= 45));
			int iTemp878 = int((fTemp829 >= 51));
			int iTemp879 = int((fTemp829 >= 52));
			int iTemp880 = int((fTemp829 >= 54));
			int iTemp881 = int((fTemp829 >= 53));
			int iTemp882 = int((fTemp829 >= 56));
			int iTemp883 = int((fTemp829 >= 57));
			int iTemp884 = int((fTemp829 >= 59));
			int iTemp885 = int((fTemp829 >= 58));
			int iTemp886 = int((fTemp829 >= 55));
			int iTemp887 = int((fTemp829 >= 50));
			int iTemp888 = int((fTemp829 >= 61));
			int iTemp889 = int((fTemp829 >= 62));
			int iTemp890 = int((fTemp829 >= 64));
			int iTemp891 = int((fTemp829 >= 63));
			int iTemp892 = int((fTemp829 >= 66));
			int iTemp893 = int((fTemp829 >= 67));
			int iTemp894 = int((fTemp829 >= 69));
			int iTemp895 = int((fTemp829 >= 68));
			int iTemp896 = int((fTemp829 >= 65));
			int iTemp897 = int((fTemp829 >= 71));
			int iTemp898 = int((fTemp829 >= 72));
			int iTemp899 = int((fTemp829 >= 74));
			int iTemp900 = int((fTemp829 >= 73));
			int iTemp901 = int((fTemp829 >= 76));
			int iTemp902 = int((fTemp829 >= 77));
			int iTemp903 = int((fTemp829 >= 79));
			int iTemp904 = int((fTemp829 >= 78));
			int iTemp905 = int((fTemp829 >= 75));
			int iTemp906 = int((fTemp829 >= 70));
			int iTemp907 = int((fTemp829 >= 60));
			int iTemp908 = int((fTemp829 >= 40));
			fRec107[0] = ((fSlow14 * fRec107[1]) + (fSlow15 * ((iTemp908)?((iTemp907)?((iTemp906)?((iTemp905)?((iTemp904)?((iTemp903)?4950:3800):((iTemp902)?2700:((iTemp901)?700:325))):((iTemp900)?((iTemp899)?4950:3800):((iTemp898)?2830:((iTemp897)?800:450)))):((iTemp896)?((iTemp895)?((iTemp894)?4950:3600):((iTemp893)?2800:((iTemp892)?2000:350))):((iTemp891)?((iTemp890)?4950:3900):((iTemp889)?2900:((iTemp888)?1150:800))))):((iTemp887)?((iTemp886)?((iTemp885)?((iTemp884)?4950:3500):((iTemp883)?2530:((iTemp882)?700:325))):((iTemp881)?((iTemp880)?4950:3500):((iTemp879)?2830:((iTemp878)?800:450)))):((iTemp877)?((iTemp876)?((iTemp875)?4950:3300):((iTemp874)?2700:((iTemp873)?1600:400))):((iTemp872)?((iTemp871)?4950:3500):((iTemp870)?2800:((iTemp869)?1150:800)))))):((iTemp868)?((iTemp867)?((iTemp866)?((iTemp865)?((iTemp864)?3300:2900):((iTemp863)?2700:((iTemp862)?600:350))):((iTemp861)?((iTemp860)?3000:2800):((iTemp859)?2600:((iTemp858)?800:400)))):((iTemp857)?((iTemp856)?((iTemp855)?3580:3200):((iTemp854)?2600:((iTemp853)?1700:400))):((iTemp852)?((iTemp851)?3250:2900):((iTemp850)?2650:((iTemp849)?1080:650))))):((iTemp848)?((iTemp847)?((iTemp846)?((iTemp845)?2950:2675):((iTemp844)?2400:((iTemp843)?600:350))):((iTemp842)?((iTemp841)?2900:2600):((iTemp840)?2400:((iTemp839)?750:400)))):((iTemp838)?((iTemp837)?((iTemp836)?3100:2800):((iTemp835)?2400:((iTemp834)?1620:400))):((iTemp833)?((iTemp832)?2750:2450):((iTemp831)?2250:((iTemp830)?1040:600)))))))));
			float fTemp909 = (fTemp178 + 1);
			int iTemp910 = int((fTemp909 >= 1));
			int iTemp911 = int((fTemp909 >= 2));
			int iTemp912 = int((fTemp909 >= 4));
			int iTemp913 = int((fTemp909 >= 3));
			int iTemp914 = int((fTemp909 >= 6));
			int iTemp915 = int((fTemp909 >= 7));
			int iTemp916 = int((fTemp909 >= 8));
			int iTemp917 = int((fTemp909 >= 5));
			int iTemp918 = int((fTemp909 >= 11));
			int iTemp919 = int((fTemp909 >= 12));
			int iTemp920 = int((fTemp909 >= 13));
			int iTemp921 = int((fTemp909 >= 16));
			int iTemp922 = int((fTemp909 >= 17));
			int iTemp923 = int((fTemp909 >= 18));
			int iTemp924 = int((fTemp909 >= 15));
			int iTemp925 = int((fTemp909 >= 10));
			int iTemp926 = int((fTemp909 >= 21));
			int iTemp927 = int((fTemp909 >= 22));
			int iTemp928 = int((fTemp909 >= 24));
			int iTemp929 = int((fTemp909 >= 23));
			int iTemp930 = int((fTemp909 >= 26));
			int iTemp931 = int((fTemp909 >= 27));
			int iTemp932 = int((fTemp909 >= 28));
			int iTemp933 = int((fTemp909 >= 25));
			int iTemp934 = int((fTemp909 >= 31));
			int iTemp935 = int((fTemp909 >= 32));
			int iTemp936 = int((fTemp909 >= 34));
			int iTemp937 = int((fTemp909 >= 33));
			int iTemp938 = int((fTemp909 >= 36));
			int iTemp939 = int((fTemp909 >= 37));
			int iTemp940 = int((fTemp909 >= 38));
			int iTemp941 = int((fTemp909 >= 35));
			int iTemp942 = int((fTemp909 >= 30));
			int iTemp943 = int((fTemp909 >= 20));
			int iTemp944 = int((fTemp909 >= 41));
			int iTemp945 = int((fTemp909 >= 42));
			int iTemp946 = int((fTemp909 >= 44));
			int iTemp947 = int((fTemp909 >= 43));
			int iTemp948 = int((fTemp909 >= 46));
			int iTemp949 = int((fTemp909 >= 47));
			int iTemp950 = int((fTemp909 >= 49));
			int iTemp951 = int((fTemp909 >= 48));
			int iTemp952 = int((fTemp909 >= 45));
			int iTemp953 = int((fTemp909 >= 51));
			int iTemp954 = int((fTemp909 >= 52));
			int iTemp955 = int((fTemp909 >= 54));
			int iTemp956 = int((fTemp909 >= 53));
			int iTemp957 = int((fTemp909 >= 56));
			int iTemp958 = int((fTemp909 >= 57));
			int iTemp959 = int((fTemp909 >= 59));
			int iTemp960 = int((fTemp909 >= 58));
			int iTemp961 = int((fTemp909 >= 55));
			int iTemp962 = int((fTemp909 >= 50));
			int iTemp963 = int((fTemp909 >= 61));
			int iTemp964 = int((fTemp909 >= 62));
			int iTemp965 = int((fTemp909 >= 64));
			int iTemp966 = int((fTemp909 >= 63));
			int iTemp967 = int((fTemp909 >= 66));
			int iTemp968 = int((fTemp909 >= 67));
			int iTemp969 = int((fTemp909 >= 69));
			int iTemp970 = int((fTemp909 >= 68));
			int iTemp971 = int((fTemp909 >= 65));
			int iTemp972 = int((fTemp909 >= 71));
			int iTemp973 = int((fTemp909 >= 72));
			int iTemp974 = int((fTemp909 >= 73));
			int iTemp975 = int((fTemp909 >= 76));
			int iTemp976 = int((fTemp909 >= 77));
			int iTemp977 = int((fTemp909 >= 79));
			int iTemp978 = int((fTemp909 >= 78));
			int iTemp979 = int((fTemp909 >= 75));
			int iTemp980 = int((fTemp909 >= 70));
			int iTemp981 = int((fTemp909 >= 60));
			int iTemp982 = int((fTemp909 >= 40));
			fRec108[0] = ((fSlow14 * fRec108[1]) + (fSlow15 * ((iTemp982)?((iTemp981)?((iTemp980)?((iTemp979)?((iTemp978)?((iTemp977)?4950:3800):((iTemp976)?2700:((iTemp975)?700:325))):((iTemp974)?((int((fTemp909 >= 74)))?4950:3800):((iTemp973)?2830:((iTemp972)?800:450)))):((iTemp971)?((iTemp970)?((iTemp969)?4950:3600):((iTemp968)?2800:((iTemp967)?2000:350))):((iTemp966)?((iTemp965)?4950:3900):((iTemp964)?2900:((iTemp963)?1150:800))))):((iTemp962)?((iTemp961)?((iTemp960)?((iTemp959)?4950:3500):((iTemp958)?2530:((iTemp957)?700:325))):((iTemp956)?((iTemp955)?4950:3500):((iTemp954)?2830:((iTemp953)?800:450)))):((iTemp952)?((iTemp951)?((iTemp950)?4950:3300):((iTemp949)?2700:((iTemp948)?1600:400))):((iTemp947)?((iTemp946)?4950:3500):((iTemp945)?2800:((iTemp944)?1150:800)))))):((iTemp943)?((iTemp942)?((iTemp941)?((iTemp940)?((int((fTemp909 >= 39)))?3300:2900):((iTemp939)?2700:((iTemp938)?600:350))):((iTemp937)?((iTemp936)?3000:2800):((iTemp935)?2600:((iTemp934)?800:400)))):((iTemp933)?((iTemp932)?((int((fTemp909 >= 29)))?3580:3200):((iTemp931)?2600:((iTemp930)?1700:400))):((iTemp929)?((iTemp928)?3250:2900):((iTemp927)?2650:((iTemp926)?1080:650))))):((iTemp925)?((iTemp924)?((iTemp923)?((int((fTemp909 >= 19)))?2950:2675):((iTemp922)?2400:((iTemp921)?600:350))):((iTemp920)?((int((fTemp909 >= 14)))?2900:2600):((iTemp919)?2400:((iTemp918)?750:400)))):((iTemp917)?((iTemp916)?((int((fTemp909 >= 9)))?3100:2800):((iTemp915)?2400:((iTemp914)?1620:400))):((iTemp913)?((iTemp912)?2750:2450):((iTemp911)?2250:((iTemp910)?1040:600)))))))));
			float fTemp983 = (fConst43 * (fTemp253 + ((iSlow27 * ((fSlow20 * ((int(((fRec108[0] >= 1300) & iTemp827)))?(fRec108[0] - (0.00095238094f * (fTemp826 * (fRec108[0] + -1300)))):((int((fRec108[0] <= fTemp825)))?fTemp825:fRec108[0]))) + (fSlow25 * fTemp828))) + (iSlow24 * ((fSlow23 * ((int(((fRec107[0] >= 1300) & iTemp827)))?(fRec107[0] - (0.00095238094f * (fTemp826 * (fRec107[0] + -1300)))):((int((fRec107[0] <= fTemp825)))?fTemp825:fRec107[0]))) + (fSlow21 * fTemp828))))));
			float fTemp984 = (fTemp983 + (iTemp19 * fRec105[1]));
			fRec105[0] = (fTemp984 - floorf(fTemp984));
			fRec110[0] = ((fSlow14 * fRec110[1]) + (fSlow15 * ((iTemp824)?((iTemp823)?((iTemp822)?((iTemp821)?((iTemp820)?((iTemp819)?200:180):((iTemp818)?170:((iTemp817)?60:50))):((iTemp816)?120:((iTemp815)?100:((iTemp814)?80:40)))):((iTemp813)?((iTemp812)?((iTemp811)?200:150):((iTemp810)?120:((iTemp809)?100:60))):((iTemp808)?((iTemp807)?140:130):((iTemp806)?120:((iTemp805)?90:80))))):((iTemp804)?((iTemp803)?((iTemp802)?((iTemp801)?200:180):((iTemp800)?170:((iTemp799)?60:50))):((iTemp798)?((iTemp797)?135:130):((iTemp796)?100:((iTemp795)?80:70)))):((iTemp794)?((iTemp793)?((iTemp792)?200:150):((iTemp791)?120:((iTemp790)?80:60))):((iTemp789)?((iTemp788)?140:130):((iTemp787)?120:((iTemp786)?90:80)))))):((iTemp785)?((iTemp784)?((iTemp783)?((iTemp782)?120:((iTemp781)?100:((iTemp780)?60:40))):((iTemp779)?((iTemp778)?135:130):((iTemp777)?100:((iTemp776)?80:70)))):((iTemp775)?((iTemp774)?120:((iTemp773)?100:((iTemp772)?80:70))):((iTemp771)?((iTemp770)?140:130):((iTemp769)?120:((iTemp768)?90:80))))):((iTemp767)?((iTemp766)?((iTemp765)?120:((iTemp764)?100:((iTemp763)?80:40))):((iTemp762)?120:((iTemp761)?100:((iTemp760)?80:40)))):((iTemp759)?((iTemp758)?120:((iTemp757)?100:((iTemp756)?80:40))):((iTemp755)?((iTemp754)?130:120):((iTemp753)?110:((iTemp752)?70:60)))))))));
			fRec111[0] = ((fSlow14 * fRec111[1]) + (fSlow15 * ((iTemp908)?((iTemp907)?((iTemp906)?((iTemp905)?((iTemp904)?((iTemp903)?200:180):((iTemp902)?170:((iTemp901)?60:50))):((iTemp900)?120:((iTemp898)?100:((iTemp897)?80:40)))):((iTemp896)?((iTemp895)?((iTemp894)?200:150):((iTemp893)?120:((iTemp892)?100:60))):((iTemp891)?((iTemp890)?140:130):((iTemp889)?120:((iTemp888)?90:80))))):((iTemp887)?((iTemp886)?((iTemp885)?((iTemp884)?200:180):((iTemp883)?170:((iTemp882)?60:50))):((iTemp881)?((iTemp880)?135:130):((iTemp879)?100:((iTemp878)?80:70)))):((iTemp877)?((iTemp876)?((iTemp875)?200:150):((iTemp874)?120:((iTemp873)?80:60))):((iTemp872)?((iTemp871)?140:130):((iTemp870)?120:((iTemp869)?90:80)))))):((iTemp868)?((iTemp867)?((iTemp866)?((iTemp865)?120:((iTemp863)?100:((iTemp862)?60:40))):((iTemp861)?((iTemp860)?135:130):((iTemp859)?100:((iTemp858)?80:70)))):((iTemp857)?((iTemp856)?120:((iTemp854)?100:((iTemp853)?80:70))):((iTemp852)?((iTemp851)?140:130):((iTemp850)?120:((iTemp849)?90:80))))):((iTemp848)?((iTemp847)?((iTemp846)?120:((iTemp844)?100:((iTemp843)?80:40))):((iTemp842)?120:((iTemp840)?100:((iTemp839)?80:40)))):((iTemp838)?((iTemp837)?120:((iTemp835)?100:((iTemp834)?80:40))):((iTemp833)?((iTemp832)?130:120):((iTemp831)?110:((iTemp830)?70:60)))))))));
			fRec112[0] = ((fSlow14 * fRec112[1]) + (fSlow15 * ((iTemp982)?((iTemp981)?((iTemp980)?((iTemp979)?((iTemp978)?((iTemp977)?200:180):((iTemp976)?170:((iTemp975)?60:50))):((iTemp974)?120:((iTemp973)?100:((iTemp972)?80:40)))):((iTemp971)?((iTemp970)?((iTemp969)?200:150):((iTemp968)?120:((iTemp967)?100:60))):((iTemp966)?((iTemp965)?140:130):((iTemp964)?120:((iTemp963)?90:80))))):((iTemp962)?((iTemp961)?((iTemp960)?((iTemp959)?200:180):((iTemp958)?170:((iTemp957)?60:50))):((iTemp956)?((iTemp955)?135:130):((iTemp954)?100:((iTemp953)?80:70)))):((iTemp952)?((iTemp951)?((iTemp950)?200:150):((iTemp949)?120:((iTemp948)?80:60))):((iTemp947)?((iTemp946)?140:130):((iTemp945)?120:((iTemp944)?90:80)))))):((iTemp943)?((iTemp942)?((iTemp941)?((iTemp940)?120:((iTemp939)?100:((iTemp938)?60:40))):((iTemp937)?((iTemp936)?135:130):((iTemp935)?100:((iTemp934)?80:70)))):((iTemp933)?((iTemp932)?120:((iTemp931)?100:((iTemp930)?80:70))):((iTemp929)?((iTemp928)?140:130):((iTemp927)?120:((iTemp926)?90:80))))):((iTemp925)?((iTemp924)?((iTemp923)?120:((iTemp922)?100:((iTemp921)?80:40))):((iTemp920)?120:((iTemp919)?100:((iTemp918)?80:40)))):((iTemp917)?((iTemp916)?120:((iTemp915)?100:((iTemp914)?80:40))):((iTemp913)?((iTemp912)?130:120):((iTemp911)?110:((iTemp910)?70:60)))))))));
			float fTemp985 = (fTemp258 + ((iSlow27 * ((fSlow20 * fRec112[0]) + (fSlow25 * fRec110[0]))) + (iSlow24 * ((fSlow23 * fRec111[0]) + (fSlow21 * fRec110[0])))));
			float fTemp986 = (iTemp256 * fTemp985);
			fRec109[0] = (fTemp986 + (iTemp257 * fRec109[1]));
			float fTemp987 = expf((fConst5 * (0 - fRec109[0])));
			fRec113[0] = ((iTemp257 * fRec113[1]) + (fTemp986 * fTemp265));
			float fTemp988 = expf((fConst5 * (0 - fRec113[0])));
			fRec114[0] = (iVec2[2] - ((fRec114[1] * (0 - (fTemp988 + fTemp987))) + ((fRec114[2] * fTemp988) * fTemp987)));
			float fTemp989 = (fTemp983 + (iTemp268 * fRec115[1]));
			fRec115[0] = (fTemp989 - floorf(fTemp989));
			float fTemp990 = (iTemp270 * fTemp985);
			fRec116[0] = (fTemp990 + (iTemp271 * fRec116[1]));
			float fTemp991 = expf((fConst5 * (0 - fRec116[0])));
			fRec117[0] = ((iTemp271 * fRec117[1]) + (fTemp990 * fTemp265));
			float fTemp992 = expf((fConst5 * (0 - fRec117[0])));
			fRec118[0] = (iVec3[2] - ((fRec118[1] * (0 - (fTemp992 + fTemp991))) + ((fRec118[2] * fTemp992) * fTemp991)));
			fRec119[0] = ((fSlow14 * fRec119[1]) + (fSlow15 * powf(10,(0.05f * ((iTemp908)?((iTemp907)?((iTemp906)?((iTemp905)?((iTemp904)?((iTemp903)?-60:-40):((iTemp902)?-35:((iTemp901)?-16:0))):((iTemp900)?((iTemp899)?-50:-22):((iTemp898)?-22:((iTemp897)?-11:0)))):((iTemp896)?((iTemp895)?((iTemp894)?-56:-40):((iTemp893)?-15:((iTemp892)?-20:0))):((iTemp891)?((iTemp890)?-50:-20):((iTemp889)?-32:((iTemp888)?-6:0))))):((iTemp887)?((iTemp886)?((iTemp885)?((iTemp884)?-64:-40):((iTemp883)?-30:((iTemp882)?-12:0))):((iTemp881)?((iTemp880)?-55:-28):((iTemp879)?-16:((iTemp878)?-9:0)))):((iTemp877)?((iTemp876)?((iTemp875)?-60:-35):((iTemp874)?-30:((iTemp873)?-24:0))):((iTemp872)?((iTemp871)?-60:-36):((iTemp870)?-20:((iTemp869)?-4:0)))))):((iTemp868)?((iTemp867)?((iTemp866)?((iTemp865)?((iTemp864)?-26:-14):((iTemp863)?-17:((iTemp862)?-20:0))):((iTemp861)?((iTemp860)?-26:-12):((iTemp859)?-12:((iTemp858)?-10:0)))):((iTemp857)?((iTemp856)?((iTemp855)?-20:-14):((iTemp854)?-12:((iTemp853)?-14:0))):((iTemp852)?((iTemp851)?-22:-8):((iTemp850)?-7:((iTemp849)?-6:0))))):((iTemp848)?((iTemp847)?((iTemp846)?((iTemp845)?-36:-28):((iTemp844)?-32:((iTemp843)?-20:0))):((iTemp842)?((iTemp841)?-40:-20):((iTemp840)?-21:((iTemp839)?-11:0)))):((iTemp838)?((iTemp837)?((iTemp836)?-18:-12):((iTemp835)?-9:((iTemp834)?-12:0))):((iTemp833)?((iTemp832)?-20:-9):((iTemp831)?-9:((iTemp830)?-7:0)))))))))));
			int iTemp993 = int((fTemp22 >= 1));
			int iTemp994 = int((fTemp22 >= 2));
			int iTemp995 = int((fTemp22 >= 4));
			int iTemp996 = int((fTemp22 >= 3));
			int iTemp997 = int((fTemp22 >= 6));
			int iTemp998 = int((fTemp22 >= 7));
			int iTemp999 = int((fTemp22 >= 8));
			int iTemp1000 = int((fTemp22 >= 5));
			int iTemp1001 = int((fTemp22 >= 11));
			int iTemp1002 = int((fTemp22 >= 12));
			int iTemp1003 = int((fTemp22 >= 13));
			int iTemp1004 = int((fTemp22 >= 16));
			int iTemp1005 = int((fTemp22 >= 17));
			int iTemp1006 = int((fTemp22 >= 18));
			int iTemp1007 = int((fTemp22 >= 15));
			int iTemp1008 = int((fTemp22 >= 10));
			int iTemp1009 = int((fTemp22 >= 21));
			int iTemp1010 = int((fTemp22 >= 22));
			int iTemp1011 = int((fTemp22 >= 24));
			int iTemp1012 = int((fTemp22 >= 23));
			int iTemp1013 = int((fTemp22 >= 26));
			int iTemp1014 = int((fTemp22 >= 27));
			int iTemp1015 = int((fTemp22 >= 28));
			int iTemp1016 = int((fTemp22 >= 25));
			int iTemp1017 = int((fTemp22 >= 31));
			int iTemp1018 = int((fTemp22 >= 32));
			int iTemp1019 = int((fTemp22 >= 34));
			int iTemp1020 = int((fTemp22 >= 33));
			int iTemp1021 = int((fTemp22 >= 36));
			int iTemp1022 = int((fTemp22 >= 37));
			int iTemp1023 = int((fTemp22 >= 38));
			int iTemp1024 = int((fTemp22 >= 35));
			int iTemp1025 = int((fTemp22 >= 30));
			int iTemp1026 = int((fTemp22 >= 20));
			int iTemp1027 = int((fTemp22 >= 41));
			int iTemp1028 = int((fTemp22 >= 42));
			int iTemp1029 = int((fTemp22 >= 44));
			int iTemp1030 = int((fTemp22 >= 43));
			int iTemp1031 = int((fTemp22 >= 46));
			int iTemp1032 = int((fTemp22 >= 47));
			int iTemp1033 = int((fTemp22 >= 49));
			int iTemp1034 = int((fTemp22 >= 48));
			int iTemp1035 = int((fTemp22 >= 45));
			int iTemp1036 = int((fTemp22 >= 51));
			int iTemp1037 = int((fTemp22 >= 52));
			int iTemp1038 = int((fTemp22 >= 54));
			int iTemp1039 = int((fTemp22 >= 53));
			int iTemp1040 = int((fTemp22 >= 56));
			int iTemp1041 = int((fTemp22 >= 57));
			int iTemp1042 = int((fTemp22 >= 59));
			int iTemp1043 = int((fTemp22 >= 58));
			int iTemp1044 = int((fTemp22 >= 55));
			int iTemp1045 = int((fTemp22 >= 50));
			int iTemp1046 = int((fTemp22 >= 61));
			int iTemp1047 = int((fTemp22 >= 62));
			int iTemp1048 = int((fTemp22 >= 64));
			int iTemp1049 = int((fTemp22 >= 63));
			int iTemp1050 = int((fTemp22 >= 66));
			int iTemp1051 = int((fTemp22 >= 67));
			int iTemp1052 = int((fTemp22 >= 69));
			int iTemp1053 = int((fTemp22 >= 68));
			int iTemp1054 = int((fTemp22 >= 65));
			int iTemp1055 = int((fTemp22 >= 71));
			int iTemp1056 = int((fTemp22 >= 72));
			int iTemp1057 = int((fTemp22 >= 73));
			int iTemp1058 = int((fTemp22 >= 76));
			int iTemp1059 = int((fTemp22 >= 77));
			int iTemp1060 = int((fTemp22 >= 79));
			int iTemp1061 = int((fTemp22 >= 78));
			int iTemp1062 = int((fTemp22 >= 75));
			int iTemp1063 = int((fTemp22 >= 70));
			int iTemp1064 = int((fTemp22 >= 60));
			int iTemp1065 = int((fTemp22 >= 40));
			fRec121[0] = ((fSlow14 * fRec121[1]) + (fSlow15 * ((iTemp1065)?((iTemp1064)?((iTemp1063)?((iTemp1062)?((iTemp1061)?((iTemp1060)?4950:3800):((iTemp1059)?2700:((iTemp1058)?700:325))):((iTemp1057)?((int((fTemp22 >= 74)))?4950:3800):((iTemp1056)?2830:((iTemp1055)?800:450)))):((iTemp1054)?((iTemp1053)?((iTemp1052)?4950:3600):((iTemp1051)?2800:((iTemp1050)?2000:350))):((iTemp1049)?((iTemp1048)?4950:3900):((iTemp1047)?2900:((iTemp1046)?1150:800))))):((iTemp1045)?((iTemp1044)?((iTemp1043)?((iTemp1042)?4950:3500):((iTemp1041)?2530:((iTemp1040)?700:325))):((iTemp1039)?((iTemp1038)?4950:3500):((iTemp1037)?2830:((iTemp1036)?800:450)))):((iTemp1035)?((iTemp1034)?((iTemp1033)?4950:3300):((iTemp1032)?2700:((iTemp1031)?1600:400))):((iTemp1030)?((iTemp1029)?4950:3500):((iTemp1028)?2800:((iTemp1027)?1150:800)))))):((iTemp1026)?((iTemp1025)?((iTemp1024)?((iTemp1023)?((int((fTemp22 >= 39)))?3300:2900):((iTemp1022)?2700:((iTemp1021)?600:350))):((iTemp1020)?((iTemp1019)?3000:2800):((iTemp1018)?2600:((iTemp1017)?800:400)))):((iTemp1016)?((iTemp1015)?((int((fTemp22 >= 29)))?3580:3200):((iTemp1014)?2600:((iTemp1013)?1700:400))):((iTemp1012)?((iTemp1011)?3250:2900):((iTemp1010)?2650:((iTemp1009)?1080:650))))):((iTemp1008)?((iTemp1007)?((iTemp1006)?((int((fTemp22 >= 19)))?2950:2675):((iTemp1005)?2400:((iTemp1004)?600:350))):((iTemp1003)?((int((fTemp22 >= 14)))?2900:2600):((iTemp1002)?2400:((iTemp1001)?750:400)))):((iTemp1000)?((iTemp999)?((int((fTemp22 >= 9)))?3100:2800):((iTemp998)?2400:((iTemp997)?1620:400))):((iTemp996)?((iTemp995)?2750:2450):((iTemp994)?2250:((iTemp993)?1040:600)))))))));
			float fTemp1066 = ((int((fRec121[0] <= fRec36[0])))?fRec36[0]:fRec121[0]);
			int iTemp1067 = int((fTemp97 >= 1));
			int iTemp1068 = int((fTemp97 >= 2));
			int iTemp1069 = int((fTemp97 >= 4));
			int iTemp1070 = int((fTemp97 >= 3));
			int iTemp1071 = int((fTemp97 >= 6));
			int iTemp1072 = int((fTemp97 >= 7));
			int iTemp1073 = int((fTemp97 >= 9));
			int iTemp1074 = int((fTemp97 >= 8));
			int iTemp1075 = int((fTemp97 >= 5));
			int iTemp1076 = int((fTemp97 >= 11));
			int iTemp1077 = int((fTemp97 >= 12));
			int iTemp1078 = int((fTemp97 >= 14));
			int iTemp1079 = int((fTemp97 >= 13));
			int iTemp1080 = int((fTemp97 >= 16));
			int iTemp1081 = int((fTemp97 >= 17));
			int iTemp1082 = int((fTemp97 >= 19));
			int iTemp1083 = int((fTemp97 >= 18));
			int iTemp1084 = int((fTemp97 >= 15));
			int iTemp1085 = int((fTemp97 >= 10));
			int iTemp1086 = int((fTemp97 >= 21));
			int iTemp1087 = int((fTemp97 >= 22));
			int iTemp1088 = int((fTemp97 >= 24));
			int iTemp1089 = int((fTemp97 >= 23));
			int iTemp1090 = int((fTemp97 >= 26));
			int iTemp1091 = int((fTemp97 >= 27));
			int iTemp1092 = int((fTemp97 >= 29));
			int iTemp1093 = int((fTemp97 >= 28));
			int iTemp1094 = int((fTemp97 >= 25));
			int iTemp1095 = int((fTemp97 >= 31));
			int iTemp1096 = int((fTemp97 >= 32));
			int iTemp1097 = int((fTemp97 >= 34));
			int iTemp1098 = int((fTemp97 >= 33));
			int iTemp1099 = int((fTemp97 >= 36));
			int iTemp1100 = int((fTemp97 >= 37));
			int iTemp1101 = int((fTemp97 >= 39));
			int iTemp1102 = int((fTemp97 >= 38));
			int iTemp1103 = int((fTemp97 >= 35));
			int iTemp1104 = int((fTemp97 >= 30));
			int iTemp1105 = int((fTemp97 >= 20));
			int iTemp1106 = int((fTemp97 >= 41));
			int iTemp1107 = int((fTemp97 >= 42));
			int iTemp1108 = int((fTemp97 >= 44));
			int iTemp1109 = int((fTemp97 >= 43));
			int iTemp1110 = int((fTemp97 >= 46));
			int iTemp1111 = int((fTemp97 >= 47));
			int iTemp1112 = int((fTemp97 >= 49));
			int iTemp1113 = int((fTemp97 >= 48));
			int iTemp1114 = int((fTemp97 >= 45));
			int iTemp1115 = int((fTemp97 >= 51));
			int iTemp1116 = int((fTemp97 >= 52));
			int iTemp1117 = int((fTemp97 >= 54));
			int iTemp1118 = int((fTemp97 >= 53));
			int iTemp1119 = int((fTemp97 >= 56));
			int iTemp1120 = int((fTemp97 >= 57));
			int iTemp1121 = int((fTemp97 >= 59));
			int iTemp1122 = int((fTemp97 >= 58));
			int iTemp1123 = int((fTemp97 >= 55));
			int iTemp1124 = int((fTemp97 >= 50));
			int iTemp1125 = int((fTemp97 >= 61));
			int iTemp1126 = int((fTemp97 >= 62));
			int iTemp1127 = int((fTemp97 >= 64));
			int iTemp1128 = int((fTemp97 >= 63));
			int iTemp1129 = int((fTemp97 >= 66));
			int iTemp1130 = int((fTemp97 >= 67));
			int iTemp1131 = int((fTemp97 >= 69));
			int iTemp1132 = int((fTemp97 >= 68));
			int iTemp1133 = int((fTemp97 >= 65));
			int iTemp1134 = int((fTemp97 >= 71));
			int iTemp1135 = int((fTemp97 >= 72));
			int iTemp1136 = int((fTemp97 >= 74));
			int iTemp1137 = int((fTemp97 >= 73));
			int iTemp1138 = int((fTemp97 >= 76));
			int iTemp1139 = int((fTemp97 >= 77));
			int iTemp1140 = int((fTemp97 >= 79));
			int iTemp1141 = int((fTemp97 >= 78));
			int iTemp1142 = int((fTemp97 >= 75));
			int iTemp1143 = int((fTemp97 >= 70));
			int iTemp1144 = int((fTemp97 >= 60));
			int iTemp1145 = int((fTemp97 >= 40));
			fRec122[0] = ((fSlow14 * fRec122[1]) + (fSlow15 * ((iTemp1145)?((iTemp1144)?((iTemp1143)?((iTemp1142)?((iTemp1141)?((iTemp1140)?4950:3800):((iTemp1139)?2700:((iTemp1138)?700:325))):((iTemp1137)?((iTemp1136)?4950:3800):((iTemp1135)?2830:((iTemp1134)?800:450)))):((iTemp1133)?((iTemp1132)?((iTemp1131)?4950:3600):((iTemp1130)?2800:((iTemp1129)?2000:350))):((iTemp1128)?((iTemp1127)?4950:3900):((iTemp1126)?2900:((iTemp1125)?1150:800))))):((iTemp1124)?((iTemp1123)?((iTemp1122)?((iTemp1121)?4950:3500):((iTemp1120)?2530:((iTemp1119)?700:325))):((iTemp1118)?((iTemp1117)?4950:3500):((iTemp1116)?2830:((iTemp1115)?800:450)))):((iTemp1114)?((iTemp1113)?((iTemp1112)?4950:3300):((iTemp1111)?2700:((iTemp1110)?1600:400))):((iTemp1109)?((iTemp1108)?4950:3500):((iTemp1107)?2800:((iTemp1106)?1150:800)))))):((iTemp1105)?((iTemp1104)?((iTemp1103)?((iTemp1102)?((iTemp1101)?3300:2900):((iTemp1100)?2700:((iTemp1099)?600:350))):((iTemp1098)?((iTemp1097)?3000:2800):((iTemp1096)?2600:((iTemp1095)?800:400)))):((iTemp1094)?((iTemp1093)?((iTemp1092)?3580:3200):((iTemp1091)?2600:((iTemp1090)?1700:400))):((iTemp1089)?((iTemp1088)?3250:2900):((iTemp1087)?2650:((iTemp1086)?1080:650))))):((iTemp1085)?((iTemp1084)?((iTemp1083)?((iTemp1082)?2950:2675):((iTemp1081)?2400:((iTemp1080)?600:350))):((iTemp1079)?((iTemp1078)?2900:2600):((iTemp1077)?2400:((iTemp1076)?750:400)))):((iTemp1075)?((iTemp1074)?((iTemp1073)?3100:2800):((iTemp1072)?2400:((iTemp1071)?1620:400))):((iTemp1070)?((iTemp1069)?2750:2450):((iTemp1068)?2250:((iTemp1067)?1040:600)))))))));
			int iTemp1146 = int((fTemp178 >= 1));
			int iTemp1147 = int((fTemp178 >= 2));
			int iTemp1148 = int((fTemp178 >= 4));
			int iTemp1149 = int((fTemp178 >= 3));
			int iTemp1150 = int((fTemp178 >= 6));
			int iTemp1151 = int((fTemp178 >= 7));
			int iTemp1152 = int((fTemp178 >= 8));
			int iTemp1153 = int((fTemp178 >= 5));
			int iTemp1154 = int((fTemp178 >= 11));
			int iTemp1155 = int((fTemp178 >= 12));
			int iTemp1156 = int((fTemp178 >= 13));
			int iTemp1157 = int((fTemp178 >= 16));
			int iTemp1158 = int((fTemp178 >= 17));
			int iTemp1159 = int((fTemp178 >= 18));
			int iTemp1160 = int((fTemp178 >= 15));
			int iTemp1161 = int((fTemp178 >= 10));
			int iTemp1162 = int((fTemp178 >= 21));
			int iTemp1163 = int((fTemp178 >= 22));
			int iTemp1164 = int((fTemp178 >= 24));
			int iTemp1165 = int((fTemp178 >= 23));
			int iTemp1166 = int((fTemp178 >= 26));
			int iTemp1167 = int((fTemp178 >= 27));
			int iTemp1168 = int((fTemp178 >= 28));
			int iTemp1169 = int((fTemp178 >= 25));
			int iTemp1170 = int((fTemp178 >= 31));
			int iTemp1171 = int((fTemp178 >= 32));
			int iTemp1172 = int((fTemp178 >= 34));
			int iTemp1173 = int((fTemp178 >= 33));
			int iTemp1174 = int((fTemp178 >= 36));
			int iTemp1175 = int((fTemp178 >= 37));
			int iTemp1176 = int((fTemp178 >= 38));
			int iTemp1177 = int((fTemp178 >= 35));
			int iTemp1178 = int((fTemp178 >= 30));
			int iTemp1179 = int((fTemp178 >= 20));
			int iTemp1180 = int((fTemp178 >= 41));
			int iTemp1181 = int((fTemp178 >= 42));
			int iTemp1182 = int((fTemp178 >= 44));
			int iTemp1183 = int((fTemp178 >= 43));
			int iTemp1184 = int((fTemp178 >= 46));
			int iTemp1185 = int((fTemp178 >= 47));
			int iTemp1186 = int((fTemp178 >= 49));
			int iTemp1187 = int((fTemp178 >= 48));
			int iTemp1188 = int((fTemp178 >= 45));
			int iTemp1189 = int((fTemp178 >= 51));
			int iTemp1190 = int((fTemp178 >= 52));
			int iTemp1191 = int((fTemp178 >= 54));
			int iTemp1192 = int((fTemp178 >= 53));
			int iTemp1193 = int((fTemp178 >= 56));
			int iTemp1194 = int((fTemp178 >= 57));
			int iTemp1195 = int((fTemp178 >= 59));
			int iTemp1196 = int((fTemp178 >= 58));
			int iTemp1197 = int((fTemp178 >= 55));
			int iTemp1198 = int((fTemp178 >= 50));
			int iTemp1199 = int((fTemp178 >= 61));
			int iTemp1200 = int((fTemp178 >= 62));
			int iTemp1201 = int((fTemp178 >= 64));
			int iTemp1202 = int((fTemp178 >= 63));
			int iTemp1203 = int((fTemp178 >= 66));
			int iTemp1204 = int((fTemp178 >= 67));
			int iTemp1205 = int((fTemp178 >= 69));
			int iTemp1206 = int((fTemp178 >= 68));
			int iTemp1207 = int((fTemp178 >= 65));
			int iTemp1208 = int((fTemp178 >= 71));
			int iTemp1209 = int((fTemp178 >= 72));
			int iTemp1210 = int((fTemp178 >= 73));
			int iTemp1211 = int((fTemp178 >= 76));
			int iTemp1212 = int((fTemp178 >= 77));
			int iTemp1213 = int((fTemp178 >= 79));
			int iTemp1214 = int((fTemp178 >= 78));
			int iTemp1215 = int((fTemp178 >= 75));
			int iTemp1216 = int((fTemp178 >= 70));
			int iTemp1217 = int((fTemp178 >= 60));
			int iTemp1218 = int((fTemp178 >= 40));
			fRec123[0] = ((fSlow14 * fRec123[1]) + (fSlow15 * ((iTemp1218)?((iTemp1217)?((iTemp1216)?((iTemp1215)?((iTemp1214)?((iTemp1213)?4950:3800):((iTemp1212)?2700:((iTemp1211)?700:325))):((iTemp1210)?((int((fTemp178 >= 74)))?4950:3800):((iTemp1209)?2830:((iTemp1208)?800:450)))):((iTemp1207)?((iTemp1206)?((iTemp1205)?4950:3600):((iTemp1204)?2800:((iTemp1203)?2000:350))):((iTemp1202)?((iTemp1201)?4950:3900):((iTemp1200)?2900:((iTemp1199)?1150:800))))):((iTemp1198)?((iTemp1197)?((iTemp1196)?((iTemp1195)?4950:3500):((iTemp1194)?2530:((iTemp1193)?700:325))):((iTemp1192)?((iTemp1191)?4950:3500):((iTemp1190)?2830:((iTemp1189)?800:450)))):((iTemp1188)?((iTemp1187)?((iTemp1186)?4950:3300):((iTemp1185)?2700:((iTemp1184)?1600:400))):((iTemp1183)?((iTemp1182)?4950:3500):((iTemp1181)?2800:((iTemp1180)?1150:800)))))):((iTemp1179)?((iTemp1178)?((iTemp1177)?((iTemp1176)?((int((fTemp178 >= 39)))?3300:2900):((iTemp1175)?2700:((iTemp1174)?600:350))):((iTemp1173)?((iTemp1172)?3000:2800):((iTemp1171)?2600:((iTemp1170)?800:400)))):((iTemp1169)?((iTemp1168)?((int((fTemp178 >= 29)))?3580:3200):((iTemp1167)?2600:((iTemp1166)?1700:400))):((iTemp1165)?((iTemp1164)?3250:2900):((iTemp1163)?2650:((iTemp1162)?1080:650))))):((iTemp1161)?((iTemp1160)?((iTemp1159)?((int((fTemp178 >= 19)))?2950:2675):((iTemp1158)?2400:((iTemp1157)?600:350))):((iTemp1156)?((int((fTemp178 >= 14)))?2900:2600):((iTemp1155)?2400:((iTemp1154)?750:400)))):((iTemp1153)?((iTemp1152)?((int((fTemp178 >= 9)))?3100:2800):((iTemp1151)?2400:((iTemp1150)?1620:400))):((iTemp1149)?((iTemp1148)?2750:2450):((iTemp1147)?2250:((iTemp1146)?1040:600)))))))));
			float fTemp1219 = (fConst43 * (fTemp253 + ((iSlow27 * ((fSlow20 * ((int((fRec123[0] <= fRec36[0])))?fRec36[0]:fRec123[0])) + (fSlow25 * fTemp1066))) + (iSlow24 * ((fSlow23 * ((int((fRec122[0] <= fRec36[0])))?fRec36[0]:fRec122[0])) + (fSlow21 * fTemp1066))))));
			float fTemp1220 = (fTemp1219 + (fRec120[1] * iTemp19));
			fRec120[0] = (fTemp1220 - floorf(fTemp1220));
			fRec125[0] = ((fSlow14 * fRec125[1]) + (fSlow15 * ((iTemp1065)?((iTemp1064)?((iTemp1063)?((iTemp1062)?((iTemp1061)?((iTemp1060)?200:180):((iTemp1059)?170:((iTemp1058)?60:50))):((iTemp1057)?120:((iTemp1056)?100:((iTemp1055)?80:40)))):((iTemp1054)?((iTemp1053)?((iTemp1052)?200:150):((iTemp1051)?120:((iTemp1050)?100:60))):((iTemp1049)?((iTemp1048)?140:130):((iTemp1047)?120:((iTemp1046)?90:80))))):((iTemp1045)?((iTemp1044)?((iTemp1043)?((iTemp1042)?200:180):((iTemp1041)?170:((iTemp1040)?60:50))):((iTemp1039)?((iTemp1038)?135:130):((iTemp1037)?100:((iTemp1036)?80:70)))):((iTemp1035)?((iTemp1034)?((iTemp1033)?200:150):((iTemp1032)?120:((iTemp1031)?80:60))):((iTemp1030)?((iTemp1029)?140:130):((iTemp1028)?120:((iTemp1027)?90:80)))))):((iTemp1026)?((iTemp1025)?((iTemp1024)?((iTemp1023)?120:((iTemp1022)?100:((iTemp1021)?60:40))):((iTemp1020)?((iTemp1019)?135:130):((iTemp1018)?100:((iTemp1017)?80:70)))):((iTemp1016)?((iTemp1015)?120:((iTemp1014)?100:((iTemp1013)?80:70))):((iTemp1012)?((iTemp1011)?140:130):((iTemp1010)?120:((iTemp1009)?90:80))))):((iTemp1008)?((iTemp1007)?((iTemp1006)?120:((iTemp1005)?100:((iTemp1004)?80:40))):((iTemp1003)?120:((iTemp1002)?100:((iTemp1001)?80:40)))):((iTemp1000)?((iTemp999)?120:((iTemp998)?100:((iTemp997)?80:40))):((iTemp996)?((iTemp995)?130:120):((iTemp994)?110:((iTemp993)?70:60)))))))));
			fRec126[0] = ((fSlow14 * fRec126[1]) + (fSlow15 * ((iTemp1145)?((iTemp1144)?((iTemp1143)?((iTemp1142)?((iTemp1141)?((iTemp1140)?200:180):((iTemp1139)?170:((iTemp1138)?60:50))):((iTemp1137)?120:((iTemp1135)?100:((iTemp1134)?80:40)))):((iTemp1133)?((iTemp1132)?((iTemp1131)?200:150):((iTemp1130)?120:((iTemp1129)?100:60))):((iTemp1128)?((iTemp1127)?140:130):((iTemp1126)?120:((iTemp1125)?90:80))))):((iTemp1124)?((iTemp1123)?((iTemp1122)?((iTemp1121)?200:180):((iTemp1120)?170:((iTemp1119)?60:50))):((iTemp1118)?((iTemp1117)?135:130):((iTemp1116)?100:((iTemp1115)?80:70)))):((iTemp1114)?((iTemp1113)?((iTemp1112)?200:150):((iTemp1111)?120:((iTemp1110)?80:60))):((iTemp1109)?((iTemp1108)?140:130):((iTemp1107)?120:((iTemp1106)?90:80)))))):((iTemp1105)?((iTemp1104)?((iTemp1103)?((iTemp1102)?120:((iTemp1100)?100:((iTemp1099)?60:40))):((iTemp1098)?((iTemp1097)?135:130):((iTemp1096)?100:((iTemp1095)?80:70)))):((iTemp1094)?((iTemp1093)?120:((iTemp1091)?100:((iTemp1090)?80:70))):((iTemp1089)?((iTemp1088)?140:130):((iTemp1087)?120:((iTemp1086)?90:80))))):((iTemp1085)?((iTemp1084)?((iTemp1083)?120:((iTemp1081)?100:((iTemp1080)?80:40))):((iTemp1079)?120:((iTemp1077)?100:((iTemp1076)?80:40)))):((iTemp1075)?((iTemp1074)?120:((iTemp1072)?100:((iTemp1071)?80:40))):((iTemp1070)?((iTemp1069)?130:120):((iTemp1068)?110:((iTemp1067)?70:60)))))))));
			fRec127[0] = ((fSlow14 * fRec127[1]) + (fSlow15 * ((iTemp1218)?((iTemp1217)?((iTemp1216)?((iTemp1215)?((iTemp1214)?((iTemp1213)?200:180):((iTemp1212)?170:((iTemp1211)?60:50))):((iTemp1210)?120:((iTemp1209)?100:((iTemp1208)?80:40)))):((iTemp1207)?((iTemp1206)?((iTemp1205)?200:150):((iTemp1204)?120:((iTemp1203)?100:60))):((iTemp1202)?((iTemp1201)?140:130):((iTemp1200)?120:((iTemp1199)?90:80))))):((iTemp1198)?((iTemp1197)?((iTemp1196)?((iTemp1195)?200:180):((iTemp1194)?170:((iTemp1193)?60:50))):((iTemp1192)?((iTemp1191)?135:130):((iTemp1190)?100:((iTemp1189)?80:70)))):((iTemp1188)?((iTemp1187)?((iTemp1186)?200:150):((iTemp1185)?120:((iTemp1184)?80:60))):((iTemp1183)?((iTemp1182)?140:130):((iTemp1181)?120:((iTemp1180)?90:80)))))):((iTemp1179)?((iTemp1178)?((iTemp1177)?((iTemp1176)?120:((iTemp1175)?100:((iTemp1174)?60:40))):((iTemp1173)?((iTemp1172)?135:130):((iTemp1171)?100:((iTemp1170)?80:70)))):((iTemp1169)?((iTemp1168)?120:((iTemp1167)?100:((iTemp1166)?80:70))):((iTemp1165)?((iTemp1164)?140:130):((iTemp1163)?120:((iTemp1162)?90:80))))):((iTemp1161)?((iTemp1160)?((iTemp1159)?120:((iTemp1158)?100:((iTemp1157)?80:40))):((iTemp1156)?120:((iTemp1155)?100:((iTemp1154)?80:40)))):((iTemp1153)?((iTemp1152)?120:((iTemp1151)?100:((iTemp1150)?80:40))):((iTemp1149)?((iTemp1148)?130:120):((iTemp1147)?110:((iTemp1146)?70:60)))))))));
			float fTemp1221 = (fTemp258 + ((iSlow27 * ((fSlow20 * fRec127[0]) + (fSlow25 * fRec125[0]))) + (iSlow24 * ((fSlow23 * fRec126[0]) + (fSlow21 * fRec125[0])))));
			float fTemp1222 = (fTemp1221 * iTemp256);
			fRec124[0] = (fTemp1222 + (iTemp257 * fRec124[1]));
			float fTemp1223 = expf((fConst5 * (0 - fRec124[0])));
			fRec128[0] = ((fRec128[1] * iTemp257) + (fTemp1222 * fTemp265));
			float fTemp1224 = expf((fConst5 * (0 - fRec128[0])));
			fRec129[0] = (iVec2[2] - ((fRec129[1] * (0 - (fTemp1224 + fTemp1223))) + ((fRec129[2] * fTemp1224) * fTemp1223)));
			float fTemp1225 = ((fRec130[1] * iTemp268) + fTemp1219);
			fRec130[0] = (fTemp1225 - floorf(fTemp1225));
			float fTemp1226 = (iTemp270 * fTemp1221);
			fRec131[0] = (fTemp1226 + (iTemp271 * fRec131[1]));
			float fTemp1227 = expf((fConst5 * (0 - fRec131[0])));
			fRec132[0] = ((fRec132[1] * iTemp271) + (fTemp1226 * fTemp265));
			float fTemp1228 = expf((fConst5 * (0 - fRec132[0])));
			fRec133[0] = (iVec3[2] - ((fRec133[1] * (0 - (fTemp1228 + fTemp1227))) + ((fRec133[2] * fTemp1228) * fTemp1227)));
			fRec134[0] = ((fSlow14 * fRec134[1]) + (fSlow15 * powf(10,(0.05f * ((iTemp1145)?((iTemp1144)?((iTemp1143)?((iTemp1142)?((iTemp1141)?((iTemp1140)?-60:-40):((iTemp1139)?-35:((iTemp1138)?-16:0))):((iTemp1137)?((iTemp1136)?-50:-22):((iTemp1135)?-22:((iTemp1134)?-11:0)))):((iTemp1133)?((iTemp1132)?((iTemp1131)?-56:-40):((iTemp1130)?-15:((iTemp1129)?-20:0))):((iTemp1128)?((iTemp1127)?-50:-20):((iTemp1126)?-32:((iTemp1125)?-6:0))))):((iTemp1124)?((iTemp1123)?((iTemp1122)?((iTemp1121)?-64:-40):((iTemp1120)?-30:((iTemp1119)?-12:0))):((iTemp1118)?((iTemp1117)?-55:-28):((iTemp1116)?-16:((iTemp1115)?-9:0)))):((iTemp1114)?((iTemp1113)?((iTemp1112)?-60:-35):((iTemp1111)?-30:((iTemp1110)?-24:0))):((iTemp1109)?((iTemp1108)?-60:-36):((iTemp1107)?-20:((iTemp1106)?-4:0)))))):((iTemp1105)?((iTemp1104)?((iTemp1103)?((iTemp1102)?((iTemp1101)?-26:-14):((iTemp1100)?-17:((iTemp1099)?-20:0))):((iTemp1098)?((iTemp1097)?-26:-12):((iTemp1096)?-12:((iTemp1095)?-10:0)))):((iTemp1094)?((iTemp1093)?((iTemp1092)?-20:-14):((iTemp1091)?-12:((iTemp1090)?-14:0))):((iTemp1089)?((iTemp1088)?-22:-8):((iTemp1087)?-7:((iTemp1086)?-6:0))))):((iTemp1085)?((iTemp1084)?((iTemp1083)?((iTemp1082)?-36:-28):((iTemp1081)?-32:((iTemp1080)?-20:0))):((iTemp1079)?((iTemp1078)?-40:-20):((iTemp1077)?-21:((iTemp1076)?-11:0)))):((iTemp1075)?((iTemp1074)?((iTemp1073)?-18:-12):((iTemp1072)?-9:((iTemp1071)?-12:0))):((iTemp1070)?((iTemp1069)?-20:-9):((iTemp1068)?-9:((iTemp1067)?-7:0)))))))))));
			fRec135[0] = (fSlow50 + (fSlow14 * fRec135[1]));
			iRec136[0] = (iSlow2 & (iRec136[1] | (fRec137[1] >= 1)));
			int iTemp1229 = (iSlow3 & (fRec137[1] > 0));
			fRec137[0] = (((iTemp1229 == 0) | (fRec137[1] >= 1e-06f)) * ((fSlow52 * (((iRec136[1] == 0) & iSlow2) & (fRec137[1] < 1))) - (fRec137[1] * (((fSlow7 * iTemp1229) + (fSlow51 * (iRec136[1] & (fRec137[1] > fSlow4)))) + -1))));
			output0[i] = (FAUSTFLOAT)(fSlow53 * ((fRec137[0] * fRec135[0]) * ((fRec134[0] * (((fRec133[0] * (1 - (fTemp1227 + (fTemp1228 * (1 - fTemp1227))))) * ftbl0[int((65536 * fRec130[0]))]) + ((fRec129[0] * (1 - (fTemp1223 + (fTemp1224 * (1 - fTemp1223))))) * ftbl0[int((65536 * fRec120[0]))]))) + (((iSlow35)?((0.0036666666f * (400 - fRec36[0])) + 3):((0.00084f * (1000 - fRec36[0])) + 0.8f)) * ((((fRec119[0] * (((fRec118[0] * (1 - (fTemp991 + (fTemp992 * (1 - fTemp991))))) * ftbl0[int((65536 * fRec115[0]))]) + ((fRec114[0] * (1 - (fTemp987 + (fTemp988 * (1 - fTemp987))))) * ftbl0[int((65536 * fRec105[0]))]))) + (fRec104[0] * (((fRec103[0] * (1 - (fTemp749 + (fTemp750 * (1 - fTemp749))))) * ftbl0[int((65536 * fRec100[0]))]) + ((fRec99[0] * (1 - (fTemp745 + (fTemp746 * (1 - fTemp745))))) * ftbl0[int((65536 * fRec90[0]))])))) + (fRec89[0] * (((fRec88[0] * (1 - (fTemp511 + (fTemp512 * (1 - fTemp511))))) * ftbl0[int((65536 * fRec85[0]))]) + ((fRec84[0] * (1 - (fTemp507 + (fTemp508 * (1 - fTemp507))))) * ftbl0[int((65536 * fRec75[0]))])))) + (fRec74[0] * (((fRec73[0] * (1 - (fTemp273 + (fTemp274 * (1 - fTemp273))))) * ftbl0[int((65536 * fRec70[0]))]) + ((fRec69[0] * (1 - (fTemp261 + (fTemp266 * (1 - fTemp261))))) * ftbl0[int((65536 * fRec1[0]))]))))))));
			// post processing
			fRec137[1] = fRec137[0];
			iRec136[1] = iRec136[0];
			fRec135[1] = fRec135[0];
			fRec134[1] = fRec134[0];
			fRec133[2] = fRec133[1]; fRec133[1] = fRec133[0];
			fRec132[1] = fRec132[0];
			fRec131[1] = fRec131[0];
			fRec130[1] = fRec130[0];
			fRec129[2] = fRec129[1]; fRec129[1] = fRec129[0];
			fRec128[1] = fRec128[0];
			fRec124[1] = fRec124[0];
			fRec127[1] = fRec127[0];
			fRec126[1] = fRec126[0];
			fRec125[1] = fRec125[0];
			fRec120[1] = fRec120[0];
			fRec123[1] = fRec123[0];
			fRec122[1] = fRec122[0];
			fRec121[1] = fRec121[0];
			fRec119[1] = fRec119[0];
			fRec118[2] = fRec118[1]; fRec118[1] = fRec118[0];
			fRec117[1] = fRec117[0];
			fRec116[1] = fRec116[0];
			fRec115[1] = fRec115[0];
			fRec114[2] = fRec114[1]; fRec114[1] = fRec114[0];
			fRec113[1] = fRec113[0];
			fRec109[1] = fRec109[0];
			fRec112[1] = fRec112[0];
			fRec111[1] = fRec111[0];
			fRec110[1] = fRec110[0];
			fRec105[1] = fRec105[0];
			fRec108[1] = fRec108[0];
			fRec107[1] = fRec107[0];
			fRec106[1] = fRec106[0];
			fRec104[1] = fRec104[0];
			fRec103[2] = fRec103[1]; fRec103[1] = fRec103[0];
			fRec102[1] = fRec102[0];
			fRec101[1] = fRec101[0];
			fRec100[1] = fRec100[0];
			fRec99[2] = fRec99[1]; fRec99[1] = fRec99[0];
			fRec98[1] = fRec98[0];
			fRec94[1] = fRec94[0];
			fRec97[1] = fRec97[0];
			fRec96[1] = fRec96[0];
			fRec95[1] = fRec95[0];
			fRec90[1] = fRec90[0];
			fRec93[1] = fRec93[0];
			fRec92[1] = fRec92[0];
			fRec91[1] = fRec91[0];
			fRec89[1] = fRec89[0];
			fRec88[2] = fRec88[1]; fRec88[1] = fRec88[0];
			fRec87[1] = fRec87[0];
			fRec86[1] = fRec86[0];
			fRec85[1] = fRec85[0];
			fRec84[2] = fRec84[1]; fRec84[1] = fRec84[0];
			fRec83[1] = fRec83[0];
			fRec79[1] = fRec79[0];
			fRec82[1] = fRec82[0];
			fRec81[1] = fRec81[0];
			fRec80[1] = fRec80[0];
			fRec75[1] = fRec75[0];
			fRec78[1] = fRec78[0];
			fRec77[1] = fRec77[0];
			fRec76[1] = fRec76[0];
			fRec74[1] = fRec74[0];
			fRec73[2] = fRec73[1]; fRec73[1] = fRec73[0];
			fRec72[1] = fRec72[0];
			fRec71[1] = fRec71[0];
			fRec70[1] = fRec70[0];
			iVec3[2] = iVec3[1]; iVec3[1] = iVec3[0];
			fRec69[2] = fRec69[1]; fRec69[1] = fRec69[0];
			fRec68[1] = fRec68[0];
			fRec64[1] = fRec64[0];
			fRec67[1] = fRec67[0];
			fRec66[1] = fRec66[0];
			fRec65[1] = fRec65[0];
			fRec1[1] = fRec1[0];
			fRec63[1] = fRec63[0];
			fRec62[1] = fRec62[0];
			fRec61[1] = fRec61[0];
			iVec2[2] = iVec2[1]; iVec2[1] = iVec2[0];
			iRec2[1] = iRec2[0];
			iVec1[1] = iVec1[0];
			fRec3[1] = fRec3[0];
			fRec53[1] = fRec53[0];
			fRec54[1] = fRec54[0];
			fRec55[1] = fRec55[0];
			fRec56[1] = fRec56[0];
			fRec57[1] = fRec57[0];
			fRec58[1] = fRec58[0];
			fRec60[1] = fRec60[0];
			fRec59[1] = fRec59[0];
			fRec45[1] = fRec45[0];
			fRec46[1] = fRec46[0];
			fRec47[1] = fRec47[0];
			fRec48[1] = fRec48[0];
			fRec49[1] = fRec49[0];
			fRec50[1] = fRec50[0];
			fRec52[1] = fRec52[0];
			fRec51[1] = fRec51[0];
			fRec37[1] = fRec37[0];
			fRec38[1] = fRec38[0];
			fRec39[1] = fRec39[0];
			fRec40[1] = fRec40[0];
			fRec41[1] = fRec41[0];
			fRec42[1] = fRec42[0];
			fRec44[1] = fRec44[0];
			fRec43[1] = fRec43[0];
			fRec36[1] = fRec36[0];
			fRec30[1] = fRec30[0];
			fRec31[1] = fRec31[0];
			fRec32[1] = fRec32[0];
			fRec33[1] = fRec33[0];
			fRec34[1] = fRec34[0];
			fRec35[1] = fRec35[0];
			fRec22[1] = fRec22[0];
			fRec23[1] = fRec23[0];
			fRec24[1] = fRec24[0];
			fRec25[1] = fRec25[0];
			fRec26[1] = fRec26[0];
			fRec27[1] = fRec27[0];
			fRec29[1] = fRec29[0];
			fRec28[1] = fRec28[0];
			fRec21[1] = fRec21[0];
			iRec20[1] = iRec20[0];
			fRec5[1] = fRec5[0];
			fRec4[1] = fRec4[0];
			fRec14[1] = fRec14[0];
			fRec15[1] = fRec15[0];
			fRec16[1] = fRec16[0];
			fRec17[1] = fRec17[0];
			fRec18[1] = fRec18[0];
			fRec19[1] = fRec19[0];
			fRec6[1] = fRec6[0];
			fRec7[1] = fRec7[0];
			fRec8[1] = fRec8[0];
			fRec9[1] = fRec9[0];
			fRec10[1] = fRec10[0];
			fRec11[1] = fRec11[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			iVec0[1] = iVec0[0];
		}
	}
};


float 	LaDiDaFaust::ftbl0[65536];
