/*
  ==============================================================================

    AltLookAndFeel.cpp
    Created: 29 Nov 2014 5:19:37am
    Author:  Mike Olsen

  ==============================================================================
*/

//#include "../JuceLibraryCode/JuceHeader.h"
#include "AltLookAndFeel.h"

//==============================================================================
AltLookAndFeel::AltLookAndFeel()
{
}

AltLookAndFeel::~AltLookAndFeel()
{
}

void AltLookAndFeel::drawRotarySlider(Graphics& g, int x, int y, int width, int height,
	float sliderPos, float rotaryStartAngle, float rotaryEndAngle,
	Slider& slider)
{
	// this code is a modified version of the code in juce_LookAndFeed_V3 function of the same name

	// variables to help determine how to draw the rotary slider
	const float radius = jmin(width / 2, height / 2) - 2.0f;
	const float centreX = x + width * 0.5f;
	const float centreY = y + height * 0.5f;
	const float rx = centreX - radius;
	const float ry = centreY - radius;
	const float rw = radius * 2.0f;
	const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
	const bool isMouseOver = slider.isMouseOverOrDragging() && slider.isEnabled();

	// get the rotary fill color using the knob color function
	Colour knobColour(slider.findColour(Slider::rotarySliderFillColourId));
	
    const Colour gradCol3(knobColour.overlaidWith(Colour(slider.isEnabled() ? 0xCC000000 : 0xA0000000)));
	const Colour gradCol4(knobColour.overlaidWith(Colour(0x20FFFFFF)));

	// set the gradient color mix for the fill of the inner slider track
	g.setGradientFill(ColourGradient(gradCol4, centreX, centreY, gradCol3, centreX, centreY - radius, true));
	// determine outline thickness for rotary knob
	const float outlineThickness = slider.isEnabled() ? 0.8f : 0.03f;
	// draw glass sphere with gradient
	if (slider.getName().equalsIgnoreCase("vowelMix"))
	    drawGlassSphere_mod(g, rx, ry, radius * 2.0f, knobColour, outlineThickness, 0.4f);
	else
	    drawGlassSphere(g, rx, ry, radius * 2.0f, knobColour, outlineThickness);

	// get color for outline of rotary knob
	Colour rotaryFillColour(slider.findColour(Slider::rotarySliderFillColourId));
	g.setColour(rotaryFillColour);
	Path p;

	// create a circle and stroke it with outline
	p.addEllipse(-0.5f * rw, -0.5f * rw, rw , rw );
	PathStrokeType(rw * 0.025f).createStrokedPath(p, p);

	// add a small little rectangle near top of rotary knob -- it will point at current value
	// to give user orientation of knob
	p.addLineSegment(Line<float>(0.0f, -0.6f * radius, 0.0f, -0.85f * radius), rw * 0.025f);

	// fill the entire path
	g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
}

// overridden to make vowel box drop-down custom color
void AltLookAndFeel::drawPopupMenuBackground (Graphics& g, int width, int height)
{
    g.fillAll (findColour(altBackColour));
    ignoreUnused (width, height);

   #if ! JUCE_MAC
    g.setColour (findColour(altTextColour).withAlpha (0.6f));
    g.drawRect (0, 0, width, height);
   #endif
}

// overridden to make vowel box selections custom color and alpha value
void AltLookAndFeel::drawComboBox (Graphics& g, int width, int height, const bool /*isButtonDown*/,
                                   int buttonX, int buttonY, int buttonW, int buttonH, ComboBox& box)
{
    g.fillAll (findColour(altBackColour).withAlpha(0.5f));

    if (box.isEnabled() && box.hasKeyboardFocus (false))
    {
        g.setColour (findColour(altTextColour));
        g.drawRect (0, 0, width, height, 2);
    }
    else
    {
        g.setColour (findColour(altTextColour));
        g.drawRect (0, 0, width, height);
    }

    const float arrowX = 0.3f;
    const float arrowH = 0.2f;

    Path p;
    p.addTriangle (buttonX + buttonW * 0.5f,            buttonY + buttonH * (0.45f - arrowH),
                   buttonX + buttonW * (1.0f - arrowX), buttonY + buttonH * 0.45f,
                   buttonX + buttonW * arrowX,          buttonY + buttonH * 0.45f);

    p.addTriangle (buttonX + buttonW * 0.5f,            buttonY + buttonH * (0.55f + arrowH),
                   buttonX + buttonW * (1.0f - arrowX), buttonY + buttonH * 0.55f,
                   buttonX + buttonW * arrowX,          buttonY + buttonH * 0.55f);

    g.setColour (findColour(altTextColour).withMultipliedAlpha (box.isEnabled() ? 1.0f : 0.3f));
    g.fillPath (p);

}

void AltLookAndFeel::drawGlassSphere_mod (Graphics& g, const float x, const float y,
                                          const float diameter, const Colour& colour,
                                          const float outlineThickness, const float alpha) noexcept
{
    if (diameter <= outlineThickness)
        return;

    Path p;
    p.addEllipse (x, y, diameter, diameter);

    {
        ColourGradient cg (Colours::white.withAlpha(alpha).overlaidWith (colour.withMultipliedAlpha (0.3f)), 0, y,
                           Colours::white.withAlpha(alpha).overlaidWith (colour.withMultipliedAlpha (0.3f)), 0, y + diameter, false);

        cg.addColour (0.4, Colours::white.withAlpha(alpha).overlaidWith (colour));

        g.setGradientFill (cg);
        g.fillPath (p);
    }

    g.setGradientFill (ColourGradient (Colours::white.withAlpha(alpha), 0, y + diameter * 0.06f,
                                       Colours::transparentWhite, 0, y + diameter * 0.3f, false));
    g.fillEllipse (x + diameter * 0.2f, y + diameter * 0.05f, diameter * 0.6f, diameter * 0.4f);

    ColourGradient cg (Colours::transparentBlack,
                       x + diameter * 0.5f, y + diameter * 0.5f,
                       Colours::black.withAlpha (0.75f * outlineThickness * colour.getFloatAlpha()),
                       x, y + diameter * 0.5f, true);

    cg.addColour (0.7, Colours::transparentBlack);
    cg.addColour (0.8, Colours::black.withAlpha (0.1f * outlineThickness));

    g.setGradientFill (cg);
    g.fillPath (p);

    g.setColour (Colours::black.withAlpha (0.75f * colour.getFloatAlpha()));
    g.drawEllipse (x, y, diameter, diameter, outlineThickness);
}