/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/**
*/
class LaDiDaAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    LaDiDaAudioProcessor();
    ~LaDiDaAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    // commented out so can compile on Windows
/*
   #ifndef JucePlugin_PreferredChannelConfigurations
    bool setPreferredBusArrangement (bool isInput, int bus, const AudioChannelSet& preferredSet) override;
   #endif
*/
    void processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages) override
    {
        jassert (!isUsingDoublePrecision());
        process (buffer, midiMessages);
    };

    void processBlock (AudioBuffer<double>& buffer, MidiBuffer& midiMessages) override
    {
        jassert (isUsingDoublePrecision());
        process (buffer, midiMessages);
    };

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //==============================================================================
    void setParameterValues (String /*paraName*/);
    
    MidiKeyboardState keyboardState;
    
    int lastUIWidth, lastUIHeight, vowelNumItems;
    
    // ADSR parameters
    AudioParameterFloat* attackParam;
    AudioParameterFloat* decayParam;
    AudioParameterFloat* sustainParam;
    AudioParameterFloat* releaseParam;
    
    // other parameters
    AudioParameterFloat* gainParam;
    AudioParameterInt*   genderParam;
    AudioParameterInt*   vowel1Param;
    AudioParameterInt*   vowel2Param;
    AudioParameterInt*   vowel3Param;
    AudioParameterFloat* vowelMixParam;
    AudioParameterFloat* vibratoDepthParam;
    AudioParameterFloat* vibratoRateParam;
    
private:
    //==============================================================================
    template <typename FloatType>
    void process (AudioBuffer<FloatType>& buffer, MidiBuffer& midiMessages);
    
    Synthesiser synth;
    
    void initialiseSynth();
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (LaDiDaAudioProcessor)
};


#endif  // PLUGINPROCESSOR_H_INCLUDED
