/*
  ==============================================================================

    AltLookAndFeel.h
    Created: 29 Nov 2014 5:19:37am
    Author:  Mike Olsen

  ==============================================================================
*/

#ifndef ALTLOOKANDFEEL_H_INCLUDED
#define ALTLOOKANDFEEL_H_INCLUDED

#include "JuceHeader.h"

enum Pallette
{
	altForeColour = 1200,
	altBackColour,
	altTitleColour,
	altTextColour,
	altAccentColour
};

//==============================================================================
class AltLookAndFeel : public LookAndFeel_V3
{
public:
	AltLookAndFeel();
	~AltLookAndFeel();

    void drawPopupMenuBackground (Graphics&, int width, int height) override;
    void drawComboBox (Graphics&, int width, int height, bool isButtonDown,
                       int buttonX, int buttonY, int buttonW, int buttonH, ComboBox& box) override;

	void drawRotarySlider(Graphics&, int x, int y, int width, int height,
		float sliderPos, float rotaryStartAngle, float rotaryEndAngle,
		Slider&) override;
    
	void drawGlassSphere_mod (Graphics&, const float x, const float y,
                              const float diameter, const Colour& colour,
                              const float outlineThickness, const float alpha) noexcept;
private:
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AltLookAndFeel)
};

#endif  // ALTLOOKANDFEEL_H_INCLUDED