/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "AltLookAndFeel.h"

//==============================================================================
/**
*/
class LaDiDaAudioProcessorEditor  : public AudioProcessorEditor,
                                    public ComboBoxListener
{
public:
    LaDiDaAudioProcessorEditor (LaDiDaAudioProcessor&);
    ~LaDiDaAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    void passParameterToProcessor (String /*paraName*/);
    float getParameterValueFromProcessor (String /*paraName*/);
    
private:
    class ParameterSlider;
    class ParameterComboBox;
    AltLookAndFeel altLookAndFeel;
    
    Label attackLabel, decayLabel, sustainLabel, releaseLabel, adsrLabel,
          gainLabel, genderLabel, maleLabel, femaleLabel, vowelLabel, 
          vibratoDepthLabel, vibratoRateLabel, vibratoLabel;//, titleLabel;
    ScopedPointer<ParameterSlider> attackSlider, decaySlider, sustainSlider, releaseSlider, 
                                   gainSlider, genderSlider,vibratoDepthSlider, 
                                   vibratoRateSlider, vowelMixSlider;
    ScopedPointer<ParameterComboBox> vowel1ComboBox, vowel2ComboBox,vowel3ComboBox;
    
    Font titleFont, labelFont, unitsFont, genderFont;
    File imageAFile, imageEFile, imageOFile, imageUFile;
    Image imageA, imageE, imageO, imageU;
    
    LaDiDaAudioProcessor& getProcessor() const {
        return static_cast<LaDiDaAudioProcessor&> (processor);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (LaDiDaAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
