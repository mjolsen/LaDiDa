/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
// This is a handy slider subclass that controls an AudioProcessorParameter
// (may move this class into the library itself at some point in the future..)
//
// ***Taken from /JUCE/examples/audio plugin demo
class LaDiDaAudioProcessorEditor::ParameterSlider  : public Slider,
                                                     private Timer
{
public:
    ParameterSlider (AudioProcessorParameter& pIn, LaDiDaAudioProcessorEditor& eIn)
        : Slider (pIn.getName (256)), p (pIn), e (eIn)
    {
        setRange (0.0, 1.0, 0.0);
        startTimerHz (30);
        updateSliderPos();
    }

    void valueChanged() override
    {
        if (isMouseButtonDown())
            p.setValueNotifyingHost ((float) Slider::getValue());
        else
            p.setValue ((float) Slider::getValue());
        
        e.passParameterToProcessor (p.getName(256));
    }

    void timerCallback() override       { updateSliderPos(); }

    void startedDragging() override     { p.beginChangeGesture(); }
    void stoppedDragging() override     { p.endChangeGesture();   }

    double getValueFromText (const String& text) override   
    { 
        return p.getValueForText (text); 
    }
    
    // custom method to get slide value with the desired number of
    // decimal places and the text value suffix
    String getTextFromValue (double value) override         
    {
        String pString;
        String pName = p.getName(256);

        if (!(pName == "gender" || pName == "vowel1" || 
              pName == "vowel2" || pName == "vowel3"))
        {
            if (pName == "vibDepth")
                pString = String(100 * e.getParameterValueFromProcessor (pName),3);
            else
                pString = p.getText((float)value,1024);
        }
        else
            pString = p.getText((float)value,1024);
                      
        pString += Slider::getTextValueSuffix();

        return pString;
    }

    // get textbox width/height
    int getTextBoxWidth() const noexcept                    
    { 
        return Slider::getTextBoxWidth();
    }
    
    int getTextBoxHeight() const noexcept                    
    {
        return Slider::getTextBoxHeight();
    }
    
    void updateSliderPos()
    {
        const float newVal = p.getValue();

        if (newVal != (float) Slider::getValue() && ! isMouseButtonDown())
            Slider::setValue (newVal);
    }

    AudioProcessorParameter& p;
    LaDiDaAudioProcessorEditor& e;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ParameterSlider)
};

class LaDiDaAudioProcessorEditor::ParameterComboBox  : public ComboBox,
                                                       private Timer
{
public:
    ParameterComboBox (AudioProcessorParameter& pIn, LaDiDaAudioProcessorEditor& eIn)
        : ComboBox (pIn.getName (256)), p (pIn), e (eIn)
    {
        startTimerHz (30);
    }

    void valueChanged() //override
    {
        float tmpVal = (float) (ComboBox::getSelectedId()-1)/
                       (float) (ComboBox::getNumItems()-1);
        if (isMouseButtonDown())
            p.setValueNotifyingHost (tmpVal);
        else
            p.setValue (tmpVal);
        
        e.passParameterToProcessor (p.getName(256));
    }

    void timerCallback() override       { updateSelection(); }

    void updateSelection()
    {
        const float newVal = p.getValue();
        const int newValInt = (newVal < 0.25) ? 1 : 
                                (newVal >= 0.25 && newVal < 0.5) ? 2 : 
                                (newVal >= 0.5 && newVal < 0.75) ? 3 : 4;
        
        if (newValInt != ComboBox::getSelectedId() && ! isMouseButtonDown())
            ComboBox::setSelectedId(newValInt);
    }
    
    AudioProcessorParameter& p;
    LaDiDaAudioProcessorEditor& e;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ParameterComboBox)
};

//==============================================================================
LaDiDaAudioProcessorEditor::LaDiDaAudioProcessorEditor (LaDiDaAudioProcessor& p)
    : AudioProcessorEditor (&p), attackLabel (String(), "Attack"), 
      decayLabel (String(), "Decay"), sustainLabel (String(), "Sustain"), 
      releaseLabel (String(), "Release"), adsrLabel (String(), "ADSR Controls"), 
      vowelLabel (String(), "Vowel Control"), genderLabel (String(), "Gender"), 
      maleLabel (String(), "Male"), femaleLabel (String(), "Female"),
      vibratoLabel (String(), "Vibrato Controls")//, titleLabel (String(), "La-Di-Da")
{	
    // use different fonts on Windows
    #if JUCE_WINDOWS
        titleFont = Font ("Comic Sans MS", 55.0f, Font::plain);
        labelFont = Font ("Corbel", 16.0f, Font::plain);
        unitsFont = Font ("Corbel", 13.5f, Font::plain);
        genderFont = Font ("Corbel", 15.5f, Font::plain);
    #else
        titleFont = Font ("Comfortaa", 55.0f, Font::plain);
        labelFont = Font ("Comfortaa", 16.0f, Font::plain);
        unitsFont = Font ("Cantarell", 13.5f, Font::plain);
        genderFont = Font ("Cantarell", 15.5f, Font::plain);
    #endif
    
    // load image from binary resource
    imageA = ImageFileFormat::loadFrom(BinaryData::Face_A_png, (size_t) BinaryData::Face_A_png);
    
    // add our sliders
    addAndMakeVisible (attackSlider = new ParameterSlider (*p.attackParam, *this));
    attackSlider->setSliderStyle (Slider::LinearVertical);
    attackSlider->setTextBoxStyle (Slider::TextBoxBelow, false, attackSlider->getTextBoxWidth(), 
                                   attackSlider->getTextBoxHeight());
    attackSlider->setTextValueSuffix (" s");
    attackSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    attackSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    attackSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    attackSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));
    
    addAndMakeVisible (decaySlider = new ParameterSlider (*p.decayParam, *this));
    decaySlider->setSliderStyle (Slider::LinearVertical);
    decaySlider->setTextBoxStyle (Slider::TextBoxBelow, false, (int)((float)(decaySlider->getTextBoxWidth())*0.75f), 
                                  decaySlider->getTextBoxHeight());
    decaySlider->setTextValueSuffix (" s");
    decaySlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    decaySlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    decaySlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));
    
    addAndMakeVisible (sustainSlider = new ParameterSlider (*p.sustainParam, *this));
    sustainSlider->setSliderStyle (Slider::LinearVertical);
    sustainSlider->setTextBoxStyle (Slider::TextBoxBelow, false, (int)((float)(sustainSlider->getTextBoxWidth())*0.75f),
                                    sustainSlider->getTextBoxHeight());
    sustainSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    sustainSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    sustainSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (releaseSlider = new ParameterSlider (*p.releaseParam, *this));
    releaseSlider->setSliderStyle (Slider::LinearVertical);
    releaseSlider->setTextBoxStyle (Slider::TextBoxBelow, false, (int)((float)(releaseSlider->getTextBoxWidth())*0.75f),
                                    releaseSlider->getTextBoxHeight());
    releaseSlider->setTextValueSuffix (" s");
    releaseSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    releaseSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    releaseSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (gainSlider = new ParameterSlider (*p.gainParam, *this));
    gainSlider->setSliderStyle (Slider::Rotary);
    gainSlider->setTextBoxStyle (Slider::NoTextBox, true, 0, 0);
    gainSlider->setTextValueSuffix (" dB");
    gainSlider->setSkewFactor (0.75,true);
    gainSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    gainSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    gainSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    gainSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    gainSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xa4363434));
    gainSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    gainSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (vowelMixSlider = new ParameterSlider (*p.vowelMixParam, *this));
    vowelMixSlider->setSliderStyle (Slider::Rotary);
    vowelMixSlider->setTextBoxStyle (Slider::NoTextBox, false, (int)((float)(vowelMixSlider->getTextBoxWidth())*0.75f),
                                     vowelMixSlider->getTextBoxHeight());
    vowelMixSlider->setSkewFactor (0.75,true);
    vowelMixSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vowelMixSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vowelMixSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vowelMixSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vowelMixSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xa4363434));
    vowelMixSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    vowelMixSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (genderSlider = new ParameterSlider (*p.genderParam, *this));
    genderSlider->setSliderStyle (Slider::LinearHorizontal);
    genderSlider->setRange (0, 1, 1);
    genderSlider->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    genderSlider->setSliderSnapsToMousePosition(true);
    genderSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    genderSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    genderSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    genderSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    genderSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    genderSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (vibratoDepthSlider = new ParameterSlider (*p.vibratoDepthParam, *this));
    vibratoDepthSlider->setSliderStyle (Slider::Rotary);
    vibratoDepthSlider->setTextBoxStyle (Slider::TextBoxBelow, false, 
										 (int)((float)(vibratoDepthSlider->getTextBoxWidth())*0.75f),
                                         vibratoDepthSlider->getTextBoxHeight());
    vibratoDepthSlider->setTextValueSuffix ("%");
    vibratoDepthSlider->setSkewFactor (0.75,true);
    vibratoDepthSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vibratoDepthSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vibratoDepthSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vibratoDepthSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vibratoDepthSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xa4363434));
    vibratoDepthSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    vibratoDepthSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));

    addAndMakeVisible (vibratoRateSlider = new ParameterSlider (*p.vibratoRateParam, *this));
    vibratoRateSlider->setSliderStyle (Slider::Rotary);
    vibratoRateSlider->setTextBoxStyle (Slider::TextBoxBelow, false, 
										(int)((float)(vibratoRateSlider->getTextBoxWidth())*0.75f),
                                        vibratoRateSlider->getTextBoxHeight());
    vibratoRateSlider->setTextValueSuffix (" Hz");
    vibratoRateSlider->setSkewFactor (0.75,true);
    vibratoRateSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vibratoRateSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vibratoRateSlider->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    vibratoRateSlider->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    vibratoRateSlider->setColour (Slider::rotarySliderFillColourId, Colour (0xa4363434));
    vibratoRateSlider->setColour (Slider::thumbColourId, Colour (0xff565760));
    vibratoRateSlider->setColour (Slider::trackColourId, Colour (0x47d7daf3));
    
    // add our combo boxes
    addAndMakeVisible (vowel1ComboBox = new ParameterComboBox (*p.vowel1Param, *this));
    vowel1ComboBox->setEditableText (false);
    vowel1ComboBox->setJustificationType (Justification::centred);
    vowel1ComboBox->setTextWhenNothingSelected (String());
    vowel1ComboBox->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    vowel1ComboBox->addItem (TRANS("A"), 1);
    vowel1ComboBox->addItem (TRANS("E"), 2);
    vowel1ComboBox->addItem (TRANS("O"), 3);
    vowel1ComboBox->addItem (TRANS("U"), 4);
    vowel1ComboBox->addListener (this);
    vowel1ComboBox->setSelectedId(p.vowel1Param->get()+1);
    
    addAndMakeVisible (vowel2ComboBox = new ParameterComboBox (*p.vowel2Param, *this));
    vowel2ComboBox->setEditableText (false);
    vowel2ComboBox->setJustificationType (Justification::centred);
    vowel2ComboBox->setTextWhenNothingSelected (String());
    vowel2ComboBox->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    vowel2ComboBox->addItem (TRANS("A"), 1);
    vowel2ComboBox->addItem (TRANS("E"), 2);
    vowel2ComboBox->addItem (TRANS("O"), 3);
    vowel2ComboBox->addItem (TRANS("U"), 4);
    vowel2ComboBox->addListener (this);
    vowel2ComboBox->setSelectedId(p.vowel2Param->get()+1);
    
    addAndMakeVisible (vowel3ComboBox = new ParameterComboBox (*p.vowel3Param, *this));
    vowel3ComboBox->setEditableText (false);
    vowel3ComboBox->setJustificationType (Justification::centred);
    vowel3ComboBox->setTextWhenNothingSelected (String());
    vowel3ComboBox->setTextWhenNoChoicesAvailable (TRANS("(no choices)"));
    vowel3ComboBox->addItem (TRANS("A"), 1);
    vowel3ComboBox->addItem (TRANS("E"), 2);
    vowel3ComboBox->addItem (TRANS("O"), 3);
    vowel3ComboBox->addItem (TRANS("U"), 4);
    vowel3ComboBox->addListener (this);
    vowel3ComboBox->setSelectedId(p.vowel3Param->get()+1);
    
    // add the labels
    addAndMakeVisible (adsrLabel);
    adsrLabel.setFont (labelFont);
    adsrLabel.setJustificationType (Justification::centred);
    
    attackLabel.attachToComponent (attackSlider, false);
    attackLabel.setFont (labelFont);
    attackLabel.setJustificationType (Justification::centred);

    decayLabel.attachToComponent (decaySlider, false);
    decayLabel.setFont (labelFont);
    decayLabel.setJustificationType (Justification::centred);
    
    sustainLabel.attachToComponent (sustainSlider, false);
    sustainLabel.setFont (labelFont);
    sustainLabel.setJustificationType (Justification::centred);
    
    releaseLabel.attachToComponent (releaseSlider, false);
    releaseLabel.setFont (labelFont);
    releaseLabel.setJustificationType (Justification::centred);

    addAndMakeVisible (genderLabel);
    genderLabel.setFont (labelFont);
    genderLabel.setJustificationType (Justification::centred);
    
    addAndMakeVisible (maleLabel);
    maleLabel.setFont (genderFont);
    maleLabel.setJustificationType (Justification::right);

    addAndMakeVisible (femaleLabel);
    femaleLabel.setFont (genderFont);
    femaleLabel.setJustificationType (Justification::left);
    
    addAndMakeVisible (vowelLabel);
    vowelLabel.setFont (labelFont);
    vowelLabel.setJustificationType (Justification::centred);

    addAndMakeVisible (vibratoLabel);
    vibratoLabel.setFont (labelFont);
    vibratoLabel.setJustificationType (Justification::centred);
    
    setResizeLimits (650, 300, 800, 300);
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (p.lastUIWidth, p.lastUIHeight);
    
    setLookAndFeel (&altLookAndFeel);

    // set the colors that I want to use
	Colour backColour(0xff0847ab);
	setColour(altBackColour, backColour);
	altLookAndFeel.setColour(altBackColour, backColour);

	Colour textColour(0xff050f23);
	setColour(altTextColour, textColour);
	altLookAndFeel.setColour(altTextColour, textColour);
	
	Colour titleColour(0xff050f23);
	setColour(altTitleColour, titleColour);
	altLookAndFeel.setColour(altTitleColour, titleColour);
}

LaDiDaAudioProcessorEditor::~LaDiDaAudioProcessorEditor()
{
}

//==============================================================================
void LaDiDaAudioProcessorEditor::paint(Graphics& g)
{
    Colour ringColour = Colour(0xff1f2634);
    Path p;
        
    g.fillAll(findColour(altBackColour));

    g.setOpacity (1.0f);
    g.drawImageWithin (imageA,vowel1ComboBox->getX()-5,vowel2ComboBox->getY()-5,
                vowel3ComboBox->getX()-vowel1ComboBox->getX()+vowel2ComboBox->getWidth()+10, 
                vowel1ComboBox->getY()-vowel2ComboBox->getY()+vowel2ComboBox->getHeight()+10, 
                RectanglePlacement::stretchToFit,false);
 
    g.setFont(titleFont);
	g.setColour(findColour(altTitleColour));
	Rectangle<int> r(getLocalBounds().reduced (8).removeFromRight(getLocalBounds().reduced(12).getWidth()));
	g.drawText(TRANS("La-Di-Da"), r.getX(), r.getY()+10, r.getWidth(), 50, Justification::left, true);

	// variables to store attributes
	int cLeft, cTop, cHeight, cWidth, tWidth, tHeight;

	// figure out where the gain control is
	tWidth = 57;
	tHeight = 20;
	cLeft = gainSlider->getX()+(int)(((float)(gainSlider->getWidth()-tWidth))/2.0f);
	cTop = gainSlider->getY();
	cHeight = gainSlider->getHeight()-gainSlider->getTextBoxHeight();
	cWidth = tWidth;

	float radiusX = 0.5f * (float)cHeight;
	float radiusY = 0.5f * (float)cWidth;
	float centreY = (float)cTop + radiusY;
	float centreX = (float)cLeft + radiusX;
	float offset = 4.0f;

	// get the color of the gain control
	g.setColour(ringColour);

	// draw arcs around the outer edges of the gain control
	p.addCentredArc(centreX, centreY, radiusX + offset, radiusY + offset, 3.14159f, 0.885398f, 2.87979f, true);
	p.addCentredArc(centreX, centreY, radiusX + offset, radiusY + offset, 0.0f, 0.261799f, 2.25619f, true);
	PathStrokeType(2.0f).createStrokedPath(p, p);
	g.fillPath(p);

	// draw three small circles for dots
	p.clear();
	p.addEllipse(centreX - 2.0f, centreY - radiusY - 4.5f, 4.0f, 4.0f);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX - radiusX + offset + 2.0f, centreY + radiusY - offset - 2.0f, 4.0f, 4.0f);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX + radiusX - offset - 6.0f, centreY + radiusY - offset - 2.0f, 4.0f, 4.0f);
	g.fillPath(p);

	// add in the controls name, min, max and zero labels
	g.setColour(findColour(altTextColour));
	g.setFont(labelFont);
	g.drawText("Gain", cLeft, cTop - 37, tWidth, tHeight, Justification::centred, true);
	g.setFont(unitsFont);
	g.drawText("0 dB", cLeft, cTop - 14, cWidth, 10, Justification(36), 0);
	g.drawText("-30 dB", (int)(centreX - radiusX) - 10, (int)(centreY + radiusY) + 2, 35, 10, Justification(36), 0);
	g.drawText("+30 dB", (int)centreX + 2, (int)(centreY + radiusY)  + 2, 35, 10, Justification(36), 0);

	// get the vibrato depth control coordinates
	cLeft = vibratoDepthSlider->getX()+(int)((vibratoDepthSlider->getWidth()-tWidth)/2);
	cTop = vibratoDepthSlider->getY();
	cHeight = vibratoDepthSlider->getHeight()-vibratoDepthSlider->getTextBoxHeight();
	cWidth = tWidth;

	radiusX = 0.5f * (float)cHeight;
	radiusY = 0.5f * (float)cWidth;
	centreY = (float)cTop + radiusY;
	centreX = (float)cLeft + radiusX;
	offset = 4.0f;

	// get the color of the vibrato depth control
	g.setColour(ringColour);

	// draw arc around the outer edge of the vibrato depth control
	p.clear();
	p.addCentredArc(centreX, centreY, radiusX + offset, radiusY + offset, 3.14159f, 0.885398f, 5.397787f, true);
	PathStrokeType(2.0f).createStrokedPath(p, p);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX - radiusX + offset + 2.0f, centreY + radiusY - offset - 2.0f, 4.0f, 4.0f);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX + radiusX - offset - 6.0f, centreY + radiusY - offset - 2.0f, 4.0f, 4.0f);
	g.fillPath(p);
	
	// add in the controls name, min, max and zero labels
	g.setColour(findColour(altTextColour));
	g.setFont(labelFont);
	g.drawText("Depth", cLeft, cTop - 37, tWidth, tHeight, Justification::centred, true);
	g.setFont(unitsFont);

	// get the vibrato rate control coordinates
	cLeft = vibratoRateSlider->getX()+(int)((vibratoRateSlider->getWidth()-tWidth)/2);
	cTop = vibratoRateSlider->getY();
	cHeight = vibratoRateSlider->getHeight()-vibratoRateSlider->getTextBoxHeight();
	cWidth = vibratoRateSlider->getWidth()-(vibratoRateSlider->getWidth()-tWidth);

	radiusX = 0.5f * (float)cHeight;
	radiusY = 0.5f * (float)cWidth;
	centreY = (float)cTop + radiusY;
	centreX = (float)cLeft + radiusX;
	offset = 4.0f;

	// get the color of the vibrato rate control
	g.setColour(ringColour);

	// draw arc around the outer edge of the vibrato rate control
	p.clear();
	p.addCentredArc(centreX, centreY, radiusX + offset, radiusY + offset, 3.14159f, 0.885398f, 5.397787f, true);
	PathStrokeType(2.0f).createStrokedPath(p, p);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX - radiusX + offset + 2.0f, centreY + radiusY - offset - 2.0f, 4.0f, 4.0f);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX + radiusX - offset - 6.0f, centreY + radiusY - offset - 2.0f, 4.0f, 4.0f);
	g.fillPath(p);
	
	// add in the controls name, min, max and zero labels
	g.setColour(findColour(altTextColour));
	g.setFont(labelFont);
	g.drawText("Rate", cLeft, cTop - 37, tWidth, tHeight, Justification::centred, true);
	g.setFont(unitsFont);

	// get the vowel mix control coordinates
	cTop = vowelMixSlider->getY();
	cHeight = vowelMixSlider->getHeight();
	cWidth = cHeight;
	cLeft = vowelMixSlider->getX()+(int)((vowelMixSlider->getWidth()-cWidth)/2);
    
	radiusX = 0.5f * (float)cHeight;
	radiusY = 0.5f * (float)cWidth;
	centreY = (float)cTop + radiusY;
	centreX = (float)cLeft + radiusX;
    offset = 5.0f;
    
	// get the color of the vibrato depth control
	g.setColour(findColour(altTextColour));

	p.clear();
	
	// draw arcs around the outer edges of the vowel mix control
	p.addCentredArc(centreX, centreY, radiusX + offset, radiusY + offset, 3.14159f, 0.885398f, 2.87979f, true);
	p.addCentredArc(centreX, centreY, radiusX + offset, radiusY + offset, 0.0f, 0.261799f, 2.25619f, true);
	PathStrokeType(2.5f).createStrokedPath(p, p);
	g.fillPath(p);

	// draw three small circles for dots
	p.clear();
	p.addEllipse(centreX - 2.0f, centreY - radiusY - 7.0f, 6.0f, 6.0f);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX - radiusX + offset + 4.0f, centreY + radiusY - offset - 4.0f, 6.0f, 6.0f);
	g.fillPath(p);

	p.clear();
	p.addEllipse(centreX + radiusX - offset - 8.0f, centreY + radiusY - offset - 4.0f, 6.0f, 6.0f);
	g.fillPath(p);

}

void LaDiDaAudioProcessorEditor::resized()
{    
    // store current component size in rectangle
    Rectangle<int> r(getLocalBounds().reduced (8));
    //titleLabel.setBounds(r.removeFromTop(40).removeFromLeft(r.getWidth() * 0.4));
    r.removeFromTop(70);

    int defSliderWidth = 150;
    int defSliderHeight = 78;
    
    Rectangle<int> controlsArea(r.removeFromTop(r.getHeight()));
    Rectangle<int> vowelControlArea(controlsArea.removeFromRight((int)(controlsArea.getWidth() * 0.25f)));

    Rectangle<int> nonAdsrControlArea(controlsArea.removeFromRight((int)(controlsArea.getWidth() * 0.35f)));
    Rectangle<int> genderSliderArea(nonAdsrControlArea.removeFromBottom((int)(nonAdsrControlArea.getHeight() * 0.35f)));
    Rectangle<int> gainSliderArea(controlsArea.removeFromLeft((int)(controlsArea.getWidth() * 0.30f)));
    Rectangle<int> adsrArea(controlsArea.removeFromBottom(controlsArea.getHeight()));

    vowelLabel.setBounds(vowelControlArea.removeFromTop(15));
    int vowelComboBoxWidth = 40;
    int vowelComboBoxHeight = 20;
    int vowelMixSliderWidth = defSliderWidth;
    int vowelMixSliderHeight = defSliderHeight;
    
    if (vowelControlArea.getWidth() < defSliderWidth)
        vowelMixSliderWidth = vowelControlArea.getWidth();
    if (vowelControlArea.getHeight() < defSliderHeight)
        vowelMixSliderWidth = vowelControlArea.getHeight();
        
    vowelMixSlider->setBounds(vowelControlArea.getX()+(int)(0.5f*(float)(vowelControlArea.getWidth()-vowelMixSliderWidth)),
                              vowelControlArea.getY()+(int)(0.5f*(float)(vowelControlArea.getHeight()-vowelMixSliderHeight)),
                              vowelMixSliderWidth,vowelMixSliderHeight);
    vowel2ComboBox->setBounds(vowelMixSlider->getX()+(int)(0.5f*(float)(vowelMixSliderWidth-vowelComboBoxWidth)),
                              vowelMixSlider->getY()-(vowelComboBoxHeight+10),
                              vowelComboBoxWidth,vowelComboBoxHeight);
    vowel1ComboBox->setBounds(vowelMixSlider->getX()+(int)(0.25*(float)(vowelMixSliderWidth)-0.5f*(float)(vowelComboBoxWidth)),
                              vowelMixSlider->getY()+vowelMixSliderHeight+3,
                              vowelComboBoxWidth,vowelComboBoxHeight);
    vowel3ComboBox->setBounds(vowelMixSlider->getX()+(int)(0.75f*(float)(vowelMixSliderWidth)-0.5f*(float)(vowelComboBoxWidth)),
                              vowelMixSlider->getY()+vowelMixSliderHeight+3,
                              vowelComboBoxWidth,vowelComboBoxHeight);

    int genderSliderWidth = 50;
    
	genderSlider->setBounds(genderSliderArea.getX() + (int)((float)(genderSliderArea.getWidth())/2.0f - (float)(genderSliderWidth)/2.0f),
                            genderSliderArea.getY() + genderSliderArea.getHeight()-30, 
                            genderSliderWidth, 30);
    genderLabel.setBounds(genderSliderArea.getX(), genderSlider->getY() - 20,genderSliderArea.getWidth(),20);
    maleLabel.setBounds(genderSlider->getX() - 30, genderSlider->getY() + (int)((float)(genderSlider->getHeight())/4.0f), 30, 20);
    femaleLabel.setBounds(genderSlider->getX() + genderSlider->getWidth(), genderSlider->getY() + (int)((float)(genderSlider->getHeight())/4.0f), 70, 20);

    int gainSliderHeight = 57;

    Rectangle<int> temp = gainSliderArea.removeFromTop(15);
    gainSlider->setBounds(gainSliderArea.getX() + (int)(0.5f*(float)(gainSliderArea.getWidth()-gainSliderHeight)), 
                          gainSliderArea.getY() + 55,
                          gainSliderHeight, gainSliderHeight);
    
    int vibratoSliderWidth = 78;
    vibratoLabel.setBounds(nonAdsrControlArea.removeFromTop(15));
    vibratoDepthSlider->setBounds(nonAdsrControlArea.getX() + (int)(0.25f*(float)nonAdsrControlArea.getWidth()-0.5f*(float)vibratoSliderWidth),
                                  nonAdsrControlArea.getY() + 55, vibratoSliderWidth, vibratoSliderWidth);
    
    vibratoRateSlider->setBounds(nonAdsrControlArea.getX() + (int)(0.75f*(float)nonAdsrControlArea.getWidth() - 0.5f*(float)vibratoSliderWidth),
                                 nonAdsrControlArea.getY() + 55, vibratoSliderWidth, vibratoSliderWidth);

    adsrLabel.setBounds(adsrArea.removeFromTop(15));
    attackSlider->setBounds (adsrArea.removeFromLeft ((int)((float)adsrArea.getWidth()/4.0f)).removeFromBottom(adsrArea.getY() + (int)((float)adsrArea.getHeight()/3.0f)));
    decaySlider->setBounds (adsrArea.removeFromLeft ((int)((float)adsrArea.getWidth()/3.0f)).removeFromBottom(adsrArea.getY() + (int)((float)adsrArea.getHeight()/3.0f)));
    sustainSlider->setBounds (adsrArea.removeFromLeft ((int)((float)adsrArea.getWidth()/2.0f)).removeFromBottom(adsrArea.getY() + (int)((float)adsrArea.getHeight()/3.0f)));
    releaseSlider->setBounds (adsrArea.removeFromLeft (adsrArea.getWidth() ).removeFromBottom(adsrArea.getY() + (int)(float)(adsrArea.getHeight()/3.0f)));

    getProcessor().lastUIWidth = getWidth();
    getProcessor().lastUIHeight = getHeight();
}


void LaDiDaAudioProcessorEditor::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    //[UsercomboBoxChanged_Pre]
    //[/UsercomboBoxChanged_Pre]

    if (comboBoxThatHasChanged == vowel1ComboBox)
        vowel1ComboBox->valueChanged();
    else if (comboBoxThatHasChanged == vowel2ComboBox)
        vowel2ComboBox->valueChanged();
    else if (comboBoxThatHasChanged == vowel3ComboBox)
        vowel3ComboBox->valueChanged();

    //[UsercomboBoxChanged_Post]
    //[/UsercomboBoxChanged_Post]
}

void LaDiDaAudioProcessorEditor::passParameterToProcessor(String paraName)
{
    getProcessor().setParameterValues(paraName);
}

float LaDiDaAudioProcessorEditor::getParameterValueFromProcessor (String paraName)
{
    if (paraName == "vowelMix")
        return getProcessor().vowelMixParam->get();
    else if (paraName == "vibDepth")
        return getProcessor().vibratoDepthParam->get();
}
