#include <iostream>
#include <chrono>
#include <random>

// a random distribution object
std::uniform_real_distribution<float> distribution (0.0,1.0);

// different random seeds
std::random_device rd1;
std::random_device rd2;
std::random_device rd3;
std::random_device rd4;
std::random_device rd5;

// uncorrelated random number generators
std::mt19937 generator1 (rd1());
std::mt19937 generator2 (rd2());
std::mt19937 generator3 (rd3());
std::mt19937 generator4 (rd4());
std::mt19937 generator5 (rd5());

// functions to obtain random numbers from the different
// generators (values in [-1.0,1.0])
float getRandomNumber1(){
    return distribution (generator1) * 2.0f - 1.0f;
}

float getRandomNumber2(){
    return distribution (generator2) * 2.0f - 1.0f;
}

float getRandomNumber3(){
    return distribution (generator3) * 2.0f - 1.0f;
}

float getRandomNumber4(){
    return distribution (generator4) * 2.0f - 1.0f;
}

float getRandomNumber5(){
    return distribution (generator5) * 2.0f - 1.0f;
}
